<!DOCTYPE html>
<html lang="en">
    
<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>Home | Any Time Money</title>
    <meta name="description" content="yaaaro" />
    <meta name="keywords" content="website desin and development company in mumbai, web development, web design, seo" />
    <!-- Favicon -->
    <link href="{{url('css/painting/uploads/img/favicon.ico')}}" rel="icon">
    <!-- Google Web Fonts -->
    <!-- <link rel="preconnect" href=""> -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <!-- Libraries Stylesheet -->
    <link href="{{url('css/painting/lib/animate/animate.min.css')}}" rel="stylesheet">
    <link href="{{url('css/painting/lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{url('css/painting/css/style.css')}}" rel="stylesheet">
    <style>
        html,
        body {
            height: 100%;
        }
    </style>
</head>
<body>
    <!-- Topbar Start  -->
    <div class="col-lg-0 text-center text-lg-right b-block d-md-none" style="background-color: black; color: white;">
        <div class="d-inline-flex align-items-right">
            <!-- <a href="http://127.0.0.1/any-time-money/" class="btn px-0" style="color: white;">Home</a>
                <a href="http://127.0.0.1/any-time-money/" class="btn px-0" style="color: white;">Register</a>
                <a href="http://127.0.0.1/any-time-money/" class="btn px-0" style="color: white;">Login</a> -->
        </div>
    </div>
    <!-- Topbar End -->
    @include('navbar')
    <!-- Carousel Start -->
    <div class=" mb-1 ">
        <div class="row_container px-xl-12" style="background-color:#3D464D">
            <div class="col-lg-12">
                <div id="header-carousel" class="carousel slide carousel-fade mb-0 mb-lg-0" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#header-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#header-carousel" data-slide-to="1"></li>
                        <li data-target="#header-carousel" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item position-relative active" style="height: 430px;">
                            <img class="position-absolute w-100 h-100" src="{{url('css/painting/uploads/banner/1690075543_carousel-3.jpg')}}" style="object-fit: cover;">
                            <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">

                            </div>
                        </div>
                        <div class="carousel-item position-relative " style="height: 430px;">
                            <img class="position-absolute w-100 h-100" src="{{url('css/painting/uploads/banner/1690075518_carousel-2.jpg')}}" style="object-fit: cover;">
                            <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">

                            </div>
                        </div>
                        <div class="carousel-item position-relative " style="height: 430px;">
                            <img class="position-absolute w-100 h-100" src="{{url('css/painting/uploads/banner/1690084592_carousel-1.jpg')}}" style="object-fit: cover;">
                            <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <ul class="nav nav-tabs bg-dark" id="myTab" role="tablist">
                <li class="nav-item" onclick="selectCategory('1')">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="category11" aria-selected="true">category1</a>
                </li>
                <li class="nav-item" onclick="selectCategory('2')">
                    <a class="nav-link " id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="category2" aria-selected="true">category2</a>
                </li>
                <li class="nav-item" onclick="selectCategory('3')">
                    <a class="nav-link " id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="category3" aria-selected="true">category3</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="category_1" role="tabpanel" aria-labelledby="home-tab">
                    <!-- Start -->
                    <div class="container-fluid pt-5 pb-3">
                        <div class="row px-xl-5">
                            <input type="hidden" readonly id="categorys_1" value="6">
                            <div class="col-lg-2 col-md-4 col-sm-6 pb-1">
                                <div class="product-item bg-light mb-4" style="height:480px">
                                    <div class="product-img position-relative overflow-hidden" style="height:302px">
                                        <img class="img-fluid w-100" src="{{url('css/painting/uploads/img/game-6.jpg')}}" alt="">
                                    </div>
                                    <div class="text-center py-4">
                                        <a class="h6 text-decoration-none text-truncate" href="{{url('/shop_detail')}}">Product 6</a>
                                        <div class="d-flex align-items-center justify-content-center mt-2">
                                            <h5><i class="fas fa-rupee-sign ml-1"></i>
                                                100000 </h5>
                                            <h6 class="text-muted ml-2"><i class="fas fa-thin fa-gem ml-1 text-primary"></i>
                                                10 </h6>
                                        </div>
                                        <div class=" mb-4 pt-2">
                                            <button class="btn btn-primary px-3 disabled" data-toggle="modal" data-target="#myModal">Buy
                                                Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-6 pb-1">
                                <div class="product-item bg-light mb-4" style="height:480px">
                                    <div class="product-img position-relative overflow-hidden" style="height:302px">
                                        <img class="img-fluid w-100" src="{{url('css/painting/uploads/img/product-1.jpg')}}" alt="">
                                    </div>
                                    <div class="text-center py-4">
                                        <a class="h6 text-decoration-none text-truncate" href="{{url('/shop_detail')}}">Product 5</a>
                                        <div class="d-flex align-items-center justify-content-center mt-2">
                                            <h5><i class="fas fa-rupee-sign ml-1"></i>
                                                100000 </h5>
                                            <h6 class="text-muted ml-2"><i class="fas fa-thin fa-gem ml-1 text-primary"></i>
                                                30 </h6>
                                        </div>
                                        <div class=" mb-4 pt-2">
                                            <button class="btn btn-primary px-3 disabled" data-toggle="modal" data-target="#myModal">Buy
                                                Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-6 pb-1">
                                <div class="product-item bg-light mb-4" style="height:480px">
                                    <div class="product-img position-relative overflow-hidden" style="height:302px">
                                        <img class="img-fluid w-100" src="{{url('css/painting/uploads/img/game-5.jpg')}}" alt="">
                                    </div>
                                    <div class="text-center py-4">
                                        <a class="h6 text-decoration-none text-truncate" href="{{url('/shop_detail')}}">Product 4</a>
                                        <div class="d-flex align-items-center justify-content-center mt-2">
                                            <h5><i class="fas fa-rupee-sign ml-1"></i>
                                                100000 </h5>
                                            <h6 class="text-muted ml-2"><i class="fas fa-thin fa-gem ml-1 text-primary"></i>
                                                60 </h6>
                                        </div>
                                        <div class=" mb-4 pt-2">
                                            <button class="btn btn-primary px-3 disabled" data-toggle="modal" data-target="#myModal">Buy
                                                Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-6 pb-1">
                                <div class="product-item bg-light mb-4" style="height:480px">
                                    <div class="product-img position-relative overflow-hidden" style="height:302px">
                                        <img class="img-fluid w-100" src="{{url('css/painting/uploads/img/game-3.jpg')}}" alt="">
                                    </div>
                                    <div class="text-center py-4">
                                        <a class="h6 text-decoration-none text-truncate" href="{{url('/shop_detail')}}">Product 3</a>
                                        <div class="d-flex align-items-center justify-content-center mt-2">
                                            <h5><i class="fas fa-rupee-sign ml-1"></i>
                                                10000 </h5>
                                            <h6 class="text-muted ml-2"><i class="fas fa-thin fa-gem ml-1 text-primary"></i>
                                                40 </h6>
                                        </div>
                                        <div class=" mb-4 pt-2">
                                            <button class="btn btn-primary px-3 disabled" data-toggle="modal" data-target="#myModal">Buy
                                                Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-6 pb-1">
                                <div class="product-item bg-light mb-4" style="height:480px">
                                    <div class="product-img position-relative overflow-hidden" style="height:302px">
                                        <img class="img-fluid w-100" src="{{url('css/painting/uploads/img/game-2.jpg')}}" alt="">
                                    </div>
                                    <div class="text-center py-4">
                                        <a class="h6 text-decoration-none text-truncate" href="{{url('/shop_detail')}}">Product 2</a>
                                        <div class="d-flex align-items-center justify-content-center mt-2">
                                            <h5><i class="fas fa-rupee-sign ml-1"></i>
                                                10000 </h5>
                                            <h6 class="text-muted ml-2"><i class="fas fa-thin fa-gem ml-1 text-primary"></i>
                                                100 </h6>
                                        </div>
                                        <div class=" mb-4 pt-2">
                                            <button class="btn btn-primary px-3 disabled" data-toggle="modal" data-target="#myModal">Buy
                                                Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-6 pb-1">
                                <div class="product-item bg-light mb-4" style="height:480px">
                                    <div class="product-img position-relative overflow-hidden" style="height:302px">
                                        <img class="img-fluid w-100" src="{{url('css/painting/uploads/img/game-1.jpg')}}" alt="">
                                    </div>
                                    <div class="text-center py-4">
                                        <a class="h6 text-decoration-none text-truncate" href="{{url('/shop_detail')}}">Product 1</a>
                                        <div class="d-flex align-items-center justify-content-center mt-2">
                                            <h5><i class="fas fa-rupee-sign ml-1"></i>
                                                500 </h5>
                                            <h6 class="text-muted ml-2"><i class="fas fa-thin fa-gem ml-1 text-primary"></i>
                                                10 </h6>
                                        </div>
                                        <div class=" mb-4 pt-2">
                                            <button class="btn btn-primary px-3 disabled" data-toggle="modal" data-target="#myModal">Buy
                                                Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="{{url('/shop')}}" id="form_category" method="get">
                        <input type="hidden" id="category_filter" readonly value="1" name="category_id[]">
                        <center><button type="submit" class="btn btn-primary" style="margin-bottom: 15px;">More</button></a>
                        </center>
                    </form>
                </div>
                <!-- end -->
                <div class="tab-pane fade" id="category_2" role="tabpanel" aria-labelledby="profile-tab">
                    <!-- Start -->
                    <div class="container-fluid pt-5 pb-3">
                        <div class="row px-xl-5">
                            <input type="hidden" readonly id="categorys_2" value="2">
                            <div class="col-lg-2 col-md-4 col-sm-6 pb-1">
                                <div class="product-item bg-light mb-4" style="height:480px">
                                    <div class="product-img position-relative overflow-hidden" style="height:302px">
                                        <img class="img-fluid w-100" src="{{url('css/painting/uploads/img/game-5.jpg')}}" alt="">
                                    </div>
                                    <div class="text-center py-4">
                                        <a class="h6 text-decoration-none text-truncate" href="{{url('/shop_detail')}}">Product 8</a>
                                        <div class="d-flex align-items-center justify-content-center mt-2">
                                            <h5>
                                                100000<i class="fas fa-rupee-sign ml-1"></i>
                                            </h5>
                                            <h6 class="text-muted ml-2">
                                                2<i class="fas fa-thin fa-gem ml-1 text-primary"></i>
                                            </h6>
                                        </div>
                                        <div class=" mb-4 pt-2">
                                            <button class="btn btn-primary px-3 disabled" data-toggle="modal" data-target="#myModal">Buy
                                                Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-6 pb-1">
                                <div class="product-item bg-light mb-4" style="height:480px">
                                    <div class="product-img position-relative overflow-hidden" style="height:302px">
                                        <img class="img-fluid w-100" src="{{url('css/painting/uploads/img/game-6.jpg')}}" alt="">
                                    </div>
                                    <div class="text-center py-4">
                                        <a class="h6 text-decoration-none text-truncate" href="{{url('/shop_detail')}}">Product 7</a>
                                        <div class="d-flex align-items-center justify-content-center mt-2">
                                            <h5>
                                                100000<i class="fas fa-rupee-sign ml-1"></i>
                                            </h5>
                                            <h6 class="text-muted ml-2">
                                                20<i class="fas fa-thin fa-gem ml-1 text-primary"></i>
                                            </h6>
                                        </div>
                                        <div class=" mb-4 pt-2">
                                            <button class="btn btn-primary px-3 disabled" data-toggle="modal" data-target="#myModal">Buy
                                                Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="http://127.0.0.1/any-time-money/shop" id="form_categoryg" method="get">
                        <input type="hidden" id="category_filter" readonly value="1" name="category_id[]">
                        <center><button type="submit" class="btn btn-primary" style="margin-bottom: 15px;">More</button></a>
                        </center>
                    </form>
                    <!-- end -->
                </div>
                <div class="tab-pane fade" id="category_3" role="tabpanel" aria-labelledby="contact-tab">
                    <div class="container-fluid pt-5 pb-3">
                        <div class="row px-xl-5">
                            <input type="hidden" readonly id="categorys_3" value="0">
                            <div class="col-lg-12 col-md-4 col-sm-6 pb-1">
                                <div class="cat-item font-weight-bold text-center p-3 mb-4">No price available</div>
                            </div>
                        </div>
                    </div>
                    <form action="{{url('/shop')}}" id="form_categorygg" method="get">
                        <input type="hidden" id="category_filter" readonly value="1" name="category_id[]">
                        <center><button type="submit" class="btn btn-primary" style="margin-bottom: 15px;">More</button></a>
                        </center>
                    </form>
                </div>
            </div>
        </div>
        <!-- Carousel End -->
        <!-- Featured End -->
        <!-- Categories Start -->
    </div>
    <div class="container-fluid pt-3">
        <!-- <h2 class="mx-xl-4 mb-4"><span class="bg-secondary pr-3">Categories</span></h2> -->
        <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span class="bg-secondary pr-3">Game
                Price</span></h2>
        <div class="row px-xl-0 pb-0" style="padding: 0%;">
            <div class="col-lg-4 col-md-4 col-sm-6 pb-0">
                <div class="cat-item d-flex align-items-center mb-4" style="background: linear-gradient(90deg, rgba(255,0,0,1) 0%, rgba(254,48,28) 76%, rgb(224, 48, 48) 100%);">
                    <h4 style="color: white;
                  text-align: center;
                  WIDTH: 100%;
                  MARGIN-TOP: 5PX;
                  FONT-FAMILY: math; ">Below-20000</h4>
                </div>
                <div>
                    <a class="text-decoration-none" href="{{url('/game_detail')}}">
                        <div class="cat-item d-flex align-items-center mb-4">
                            <div class="overflow-hidden" style="width: 100px; height: 100px;">
                                <img class="img-fluid" src="{{url('css/painting/uploads/img/product-5.jpg')}}" alt="">
                            </div>
                            <div class="flex-fill pl-3">
                                <h6>
                                    game-5 </h6>
                                <small class="text-body">
                                    3424 <i class="fas fa-thin fa-star ml-1 text-primary"></i>
                                </small>
                            </div>
                        </div>
                    </a>
                    <a class="text-decoration-none" href="{{url('/game_detail')}}">
                        <div class="cat-item d-flex align-items-center mb-4">
                            <div class="overflow-hidden" style="width: 100px; height: 100px;">
                                <img class="img-fluid" src="{{url('css/painting/uploads/img/product-1.jpg')}}" alt="">
                            </div>
                            <div class="flex-fill pl-3">
                                <h6>
                                    game-1 </h6>
                                <small class="text-body">
                                    10 <i class="fas fa-thin fa-star ml-1 text-primary"></i>
                                </small>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 pb-1">
                <div class="cat-item d-flex align-items-center mb-4" style="background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(9,9,121,1) 35%, rgba(0,109,131,1) 100%);">
                    <h4 style="color: white;
                  text-align: center;
                  WIDTH: 100%;
                  MARGIN-TOP: 5PX;
                  FONT-FAMILY: math; ">20000-40000</h4>
                </div>
                <div>
                    <a class="text-decoration-none" href="{{url('/game_detail')}}">
                        <div class="cat-item d-flex align-items-center mb-4">
                            <div class="overflow-hidden" style="width: 100px; height: 100px;">
                                <img class="img-fluid" src="{{url('css/painting/uploads/img/product-4.jpg')}}" alt="">
                            </div>
                            <div class="flex-fill pl-3">
                                <h6>
                                    game-4 </h6>
                                <small class="text-body">
                                    200 <i class="fas fa-thin fa-star ml-1 text-primary"></i>
                                </small>
                            </div>
                        </div>
                    </a>
                    <a class="text-decoration-none" href="{{url('/game_detail')}}">
                        <div class="cat-item d-flex align-items-center mb-4">
                            <div class="overflow-hidden" style="width: 100px; height: 100px;">
                                <img class="img-fluid" src="{{url('css/painting/uploads/img/product-3.jpg')}}" alt="">
                            </div>
                            <div class="flex-fill pl-3">
                                <h6>
                                    game-3 </h6>
                                <small class="text-body">
                                    200 <i class="fas fa-thin fa-star ml-1 text-primary"></i>
                                </small>
                            </div>
                        </div>
                    </a>
                    <a class="text-decoration-none" href="{{url('/game_detail')}}">
                        <div class="cat-item d-flex align-items-center mb-4">
                            <div class="overflow-hidden" style="width: 100px; height: 100px;">
                                <img class="img-fluid" src="{{url('css/painting/uploads/img/product-2.jpg')}}" alt="">
                            </div>
                            <div class="flex-fill pl-3">
                                <h6>
                                    game-2 </h6>
                                <small class="text-body">
                                    20 <i class="fas fa-thin fa-star ml-1 text-primary"></i>
                                </small>
                            </div>
                        </div>
                    </a>
                    <form action="{{url('/games')}}" method="get">
                        <input type="hidden" id="category_filter" readonly value="40000" name="game_id">
                        <center><button type="submit" class="btn btn-primary" style="margin-bottom: 15px;">More</button>
                        </center>
                    </form>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6 pb-1">
                <div class="cat-item d-flex align-items-center mb-4" style="background: linear-gradient(90deg, rgb(214, 205, 82) 0%, rgb(218, 203, 75) 83%, rgb(201, 187, 34) 100%);">
                    <h4 style="color: white;
                  text-align: center;
                  WIDTH: 100%;
                  MARGIN-TOP: 5PX;
                  FONT-FAMILY: math; ">40000-Above</h4>
                </div>
                <div>
                    <div class="cat-item font-weight-bold text-center p-3 mb-4">No price available</div>
                </div>
            </div>
        </div>

        <!-- Products Start -->

        <!-- Products End -->

        <!-- Offer Start  -->
        <div class="container-fluid pt-5 pb-3">
            <div class="row px-xl-5">
                <div class="col-md-6">
                    <div class="product-offer mb-30" style="height: 300px;">
                        <img class="img-fluid" src="{{url('css/painting/uploads/img/game_template.png')}}" alt="">
                        <div class="offer-text">
                            <h6 class="text-white text-uppercase">Live Game</h6>
                            <h3 class="text-white mb-3">Rolling Dice</h3>
                            <a href="{{url('/game')}}" class="btn btn-primary">Play Now</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="product-offer mb-30" style="height: 300px;">
                        <img class="img-fluid" src="{{url('css/painting/uploads/img/game-5.jpg')}}" alt="">
                        <div class="offer-text">
                            <h6 class="text-white text-uppercase">Save 20%</h6>
                            <h3 class="text-white mb-3">Special Offer</h3>
                            <a href="" class="btn btn-primary">Shop Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Offer End-->

        <!-- Products Start -->

        <!-- Products End -->
        <!-- Vendor Start -->
        <div class="container-fluid py-5">
            <div class="row px-xl-5">
                <div class="col">
                    <div class="owl-carousel vendor-carousel">
                        <div class="bg-light p-4">
                            <img src="{{url('css/painting/uploads/img/vendor-1.jpg')}}" alt="">
                        </div>
                        <div class="bg-light p-4">
                            <img src="{{url('css/painting/uploads/img/vendor-2.jpg')}}" alt="">
                        </div>
                        <div class="bg-light p-4">
                            <img src="{{url('css/painting/uploads/img/vendor-3.jpg')}}" alt="">
                        </div>
                        <div class="bg-light p-4">
                            <img src="{{url('css/painting/uploads/img/vendor-4.jpg')}}" alt="">
                        </div>
                        <div class="bg-light p-4">
                            <img src="{{url('css/painting/uploads/img/vendor-5.jpg')}}" alt="">
                        </div>
                        <div class="bg-light p-4">
                            <img src="{{url('css/painting/uploads/img/vendor-6.jpg')}}" alt="">
                        </div>
                        <div class="bg-light p-4">
                            <img src="{{url('css/painting/uploads/img/vendor-7.jpg')}}" alt="">
                        </div>
                        <div class="bg-light p-4">
                            <img src="{{url('css/painting/uploads/img/vendor-8.jpg')}}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Vendor End -->
        <script>
            function selectCategory(id) {
                if (id == 1) {
                    $("#category_2").removeClass('show active');
                    $("#category_3").removeClass('show active');
                    $("#category_1").addClass('show active');
                    $("#category_filter").val(id);
                    var count = $("#categorys_1").val();
                    if (count > 5) {
                        $("#form_category").show();
                    } else {
                        $("#form_category").hide();
                    }
                }

                if (id == 2) {
                    $("#category_1").removeClass('show active');
                    $("#category_3").removeClass('show active');
                    $("#category_2").addClass('show active');
                    $("#category_filter").val(id);
                    var count = $("#categorys_2").val();
                    if (count > 5) {
                        $("#form_categoryg").show();
                    } else {
                        $("#form_categoryg").hide();
                    }
                }

                if (id == 3) {
                    $("#category_1").removeClass('show active');
                    $("#category_2").removeClass('show active');
                    $("#category_3").addClass('show active');
                    $("#category_filter").val(id);
                    var count = $("#categorys_3").val();
                    if (count > 5) {
                        $("#form_categorygg").show();
                    } else {
                        $("#form_categorygg").hide();
                    }
                }
            }
        </script><!-- Modal Login-->
        <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <h6>Please Login for futher process</h6>
                    </div>
                    <div class="modal-footer">
                        <a href="http://127.0.0.1/any-time-money/login"><button type="button" class="btn btn-warning">Login</button></a>
                    </div>
                </div>

            </div>
        </div>
        <!-- Modal Confirmation-->
        <div class="modal fade" id="myModalPrice" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="modal-body">
                        <h6>Are you sure, you want to claim the price</h6>
                    </div>
                    <div class="modal-footer">
                        <form method="post" action="http://127.0.0.1/any-time-money/Site/price_add">
                            <input type="hidden" name="csrf_test_name" value="4b1987be178bd0ea32e9af8cb3ee59e9">
                            <input type="hidden" name="game_id" id="product_pop_id" readonly>
                            <button type="submit" class="btn btn-outline-success">Yes</button>
                        </form>
                        <button type="button" data-dismiss="modal" class="btn btn-outline-danger">No</button>
                    </div>
                </div>
            </div>
        </div>
        @include('footer')
</body>

</html>