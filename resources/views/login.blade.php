<!DOCTYPE html>
<html lang="en">

@include('head')

<body>
    <!-- Topbar Start  -->

    <div class="col-lg-0 text-center text-lg-right b-block d-md-none" style="background-color: black; color: white;">
        <div class="d-inline-flex align-items-right">

            <!-- <a href="http://127.0.0.1/any-time-money/" class="btn px-0" style="color: white;">Home</a>
                <a href="http://127.0.0.1/any-time-money/" class="btn px-0" style="color: white;">Register</a>
                <a href="http://127.0.0.1/any-time-money/" class="btn px-0" style="color: white;">Login</a> -->
        </div>
    </div>

    <!-- Topbar End -->
    @include('navbar')
    <!-- Breadcrumb Start -->
    <div class="container-fluid mt-4">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-decoration-none text-dark" href="{{url('/')}}">Home</a>
                    <span class="breadcrumb-item active">Login</span>
                </nav>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Checkout Start -->
    <div class="container-fluid">
        <div class="row  px-xl-5 d-flex align-items-center justify-content-center">
            <div class="col-lg-12">
                <h5 class="section-title position-relative text-uppercase mb-3"><span class="bg-secondary pr-3">Login</span>
                </h5>
            </div>
            <div class="col-lg-8">
                <div class="bg-light p-30 mb-5">
                    <form action="http://127.0.0.1/any-time-money/Login/user_login" method="post">
                        <input type="hidden" name="csrf_test_name" value="b48f62c116d9af91a71b46e210fb4fae">
                        <div class="bg-light p-30">
                            <div class="row">
                                <div class="col-md-12 form-group">
                                    <label>E-mail *</label>
                                    <input class="form-control" name="email" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Enter E-mail" required>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label>Password *</label>
                                    <input required class="form-control" type="password" name="password" placeholder="Enter Password" />
                                </div>
                                <div class="col-md-12">
                                    <a class="text-info text-decoration-none" href="http://127.0.0.1/any-time-money/forget">forget Password ?</a>
                                </div>
                                <div class="col-md-12 mt-2">
                                    <button type="submit" class="btn  btn-primary font-weight-bold px-4 py-2">Login</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Checkout End -->
    </div>
    <!-- Modal Login-->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h6>Please Login for futher process</h6>
                </div>
                <div class="modal-footer">
                    <a href="http://127.0.0.1/any-time-money/login"><button type="button" class="btn btn-warning">Login</button></a>
                </div>
            </div>

        </div>
    </div>
    <!-- Modal Confirmation-->
    <div class="modal fade" id="myModalPrice" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h6>Are you sure, you want to claim the price</h6>
                </div>
                <div class="modal-footer">
                    <form method="post" action="http://127.0.0.1/any-time-money/Site/price_add">
                        <input type="hidden" name="csrf_test_name" value="b48f62c116d9af91a71b46e210fb4fae">
                        <input type="hidden" name="game_id" id="product_pop_id" readonly>
                        <button type="submit" class="btn btn-outline-success">Yes</button>
                    </form>
                    <button type="button" data-dismiss="modal" class="btn btn-outline-danger">No</button>
                </div>
            </div>
        </div>
    </div>
    @include('footer')
</body>
</html>