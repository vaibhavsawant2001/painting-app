
    <!-- Navbar Start -->
    <div class="container-fluid bg-dark mb-0">
        <div class="row px-xl-0">
            <div class="col-lg-3 d-none d-lg-block">
                <img src="http://127.0.0.1/any-time-money/uploads/img/logo_W.png" alt="" style="width: 200px;margin-top: 5px;">
            </div>
            <div class="col-lg-9">
                <nav class="navbar navbar-expand-lg bg-dark navbar-dark py-2 py-lg-0 px-0">
                    <a href="" class="text-decoration-none d-block d-lg-none">
                        <img src="http://127.0.0.1/any-time-money/uploads/img/logo_W.png" alt="" style="width: 200px;">

                    </a>
                    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                        <div class="navbar-nav mr-auto py-0">
                            <a href="{{url('/')}}" class="nav-item nav-link text-warning">Home</a>
                            <a href="{{url('/shop')}}" class="nav-item nav-link ">Shop</a>
                            <a href="{{url('/games')}}" class="nav-item nav-link ">Game</a>
                            <a href="{{url('/contact-us')}}" class="nav-item nav-link ">Contact</a>
                            <a href="{{url('/register')}}" class="nav-item nav-link " style="color: white;">Register</a>
                            <a href="{{url('/login')}}" class="nav-item nav-link " style="color: white;">Login</a>
                        </div>


                        <div class="navbar-nav ml-auto py-0 d-none d-lg-block">
                            <a href="javascript:void(0);" class="btn px-0" style="color: white;">Jems</a>
                            <a href="javascript:void(0);" class="btn px-0">
                                <i class="fas fa-thin fa-gem text-primary"></i>
                                <span class="badge text-secondary border border-secondary rounded-circle" style="padding-bottom: 2px;">0</span>
                            </a>
                            <a href="javascript:void(0);" class="btn px-0" style="color: white;">Point</a>
                            <a href="javascript:void(0);" class="btn px-0">
                                <i class="fas fa-thin fa-star text-primary"></i>
                                <span class="badge text-secondary border border-secondary rounded-circle" style="padding-bottom: 2px;">0</span>
                            </a>
                            <a href="{{url('/favorite')}}" class="btn px-0 " style="color: white;">Favourite</a>
                            <a href="{{url('/favorite')}}" class="btn px-0">
                                <i class="fas fa-heart text-primary"></i>
                                <span class="badge text-secondary  border border-secondary rounded-circle" style="padding-bottom: 2px;">0</span>
                            </a>
                            <a href="{{url('/my_shopping')}}" class="btn px-0 " style="color: white;">My Shopping</a>
                            <a href="{{url('/my_shopping')}}" class="btn px-0">
                                <i class="fas fa-shopping-cart text-primary"></i>
                                <span class="badge text-secondary  border border-secondary rounded-circle" style="padding-bottom: 2px;">0</span>
                            </a>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <!-- Navbar End -->