<!DOCTYPE html>
<html lang="en">
@include('head')
<body>
    <!-- Topbar Start  -->
    <div class="col-lg-0 text-center text-lg-right b-block d-md-none" style="background-color: black; color: white;">
        <div class="d-inline-flex align-items-right">
            <!-- <a href="http://127.0.0.1/any-time-money/" class="btn px-0" style="color: white;">Home</a>
                <a href="http://127.0.0.1/any-time-money/" class="btn px-0" style="color: white;">Register</a>
                <a href="http://127.0.0.1/any-time-money/" class="btn px-0" style="color: white;">Login</a> -->
        </div>
    </div>
    <!-- Topbar End -->
@include('navbar')
    <!-- Shop Start -->
    <!-- Breadcrumb Start -->
    <div class="container-fluid mt-4">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-decoration-none text-dark" href="http://127.0.0.1/any-time-money/">Home</a>
                    <span class="breadcrumb-item active">Shop</span>
                </nav>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->
    <div class="container-fluid">
        <h5 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span class="bg-secondary pr-3">Shop</span>
        </h5>
        <div class="row px-xl-5">
            <!-- Shop Sidebar Start -->
            <div class="col-lg-3 col-md-4">
                <!-- Price Start -->

                <div class="bg-light p-4 mb-30">
                    <form action="http://127.0.0.1/any-time-money/shop" method="get">
                        <label>All Category</label>
                        <div class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                            <input type="checkbox" class="custom-control-input checkboxs" onclick="checkout_val()" name="category_id[]" value="1" id="1">
                            <label class="custom-control-label" for="1">category11</label>
                        </div>

                        <div class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                            <input type="checkbox" class="custom-control-input checkboxs" onclick="checkout_val()" name="category_id[]" value="2" id="2">
                            <label class="custom-control-label" for="2">category2</label>
                        </div>

                        <div class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                            <input type="checkbox" class="custom-control-input checkboxs" onclick="checkout_val()" name="category_id[]" value="3" id="3">
                            <label class="custom-control-label" for="3">category3</label>
                        </div>

                        <button type="submit" id="filters" name="filter" value="filter" class="btn btn-disable btn-primary font-weight-bold px-3 py-1">Filter</button>
                    </form>
                </div>
                <!-- Price End -->
            </div>
            <!-- Shop Sidebar End -->

            <!-- Shop Product Start -->
            <div class="col-lg-9 col-md-8">
                <div class="row pb-3">
                    <div class="col-lg-4 col-md-6 col-sm-6 pb-1">
                        <div class="product-item bg-light mb-4" style="height:620px">
                            <div class="product-img position-relative  overflow-hidden" style="height:452px">
                                <img class="img-fluid w-100" src="http://127.0.0.1/any-time-money/uploads/img/game-5.jpg" alt="">
                            </div>
                            <div class="text-center py-4">
                                <a class="h6 text-decoration-none text-truncate" href="http://127.0.0.1/any-time-money/shop-detail/9">Product 8</a>
                                <div class="d-flex align-items-center justify-content-center mt-2">
                                    <h5>100000<i class="fas fa-rupee-sign ml-1"></i></h5>
                                    <h6 class="text-muted ml-2">2<i class="fas fa-thin fa-gem ml-1 text-primary"></i></h6>
                                </div>
                                <div class=" mb-4 pt-2">
                                    <button class="btn btn-primary px-3 disabled" data-toggle="modal" data-target="#myModal">Buy
                                        Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-12">
                    <nav>
                        <ul class="pagination justify-content-center">
                            <li class="page-item disabled"><a class="page-link" href="#">Previous</span></a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>
                </div> -->
                    <div class="col-lg-4 col-md-6 col-sm-6 pb-1">
                        <div class="product-item bg-light mb-4" style="height:620px">
                            <div class="product-img position-relative  overflow-hidden" style="height:452px">
                                <img class="img-fluid w-100" src="http://127.0.0.1/any-time-money/uploads/img/game-6.jpg" alt="">
                            </div>
                            <div class="text-center py-4">
                                <a class="h6 text-decoration-none text-truncate" href="http://127.0.0.1/any-time-money/shop-detail/8">Product 7</a>
                                <div class="d-flex align-items-center justify-content-center mt-2">
                                    <h5>100000<i class="fas fa-rupee-sign ml-1"></i></h5>
                                    <h6 class="text-muted ml-2">20<i class="fas fa-thin fa-gem ml-1 text-primary"></i></h6>
                                </div>
                                <div class=" mb-4 pt-2">
                                    <button class="btn btn-primary px-3 disabled" data-toggle="modal" data-target="#myModal">Buy
                                        Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-12">
                    <nav>
                        <ul class="pagination justify-content-center">
                            <li class="page-item disabled"><a class="page-link" href="#">Previous</span></a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>
                </div> -->
                    <div class="col-lg-4 col-md-6 col-sm-6 pb-1">
                        <div class="product-item bg-light mb-4" style="height:620px">
                            <div class="product-img position-relative  overflow-hidden" style="height:452px">
                                <img class="img-fluid w-100" src="http://127.0.0.1/any-time-money/uploads/img/game-6.jpg" alt="">
                            </div>
                            <div class="text-center py-4">
                                <a class="h6 text-decoration-none text-truncate" href="http://127.0.0.1/any-time-money/shop-detail/7">Product 6</a>
                                <div class="d-flex align-items-center justify-content-center mt-2">
                                    <h5>100000<i class="fas fa-rupee-sign ml-1"></i></h5>
                                    <h6 class="text-muted ml-2">10<i class="fas fa-thin fa-gem ml-1 text-primary"></i></h6>
                                </div>
                                <div class=" mb-4 pt-2">
                                    <button class="btn btn-primary px-3 disabled" data-toggle="modal" data-target="#myModal">Buy
                                        Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-12">
                    <nav>
                        <ul class="pagination justify-content-center">
                            <li class="page-item disabled"><a class="page-link" href="#">Previous</span></a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>
                </div> -->
                    <div class="col-lg-4 col-md-6 col-sm-6 pb-1">
                        <div class="product-item bg-light mb-4" style="height:620px">
                            <div class="product-img position-relative  overflow-hidden" style="height:452px">
                                <img class="img-fluid w-100" src="http://127.0.0.1/any-time-money/uploads/img/product-1.jpg" alt="">
                            </div>
                            <div class="text-center py-4">
                                <a class="h6 text-decoration-none text-truncate" href="http://127.0.0.1/any-time-money/shop-detail/6">Product 5</a>
                                <div class="d-flex align-items-center justify-content-center mt-2">
                                    <h5>100000<i class="fas fa-rupee-sign ml-1"></i></h5>
                                    <h6 class="text-muted ml-2">30<i class="fas fa-thin fa-gem ml-1 text-primary"></i></h6>
                                </div>
                                <div class=" mb-4 pt-2">
                                    <button class="btn btn-primary px-3 disabled" data-toggle="modal" data-target="#myModal">Buy
                                        Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-12">
                    <nav>
                        <ul class="pagination justify-content-center">
                            <li class="page-item disabled"><a class="page-link" href="#">Previous</span></a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>
                </div> -->
                    <div class="col-lg-4 col-md-6 col-sm-6 pb-1">
                        <div class="product-item bg-light mb-4" style="height:620px">
                            <div class="product-img position-relative  overflow-hidden" style="height:452px">
                                <img class="img-fluid w-100" src="http://127.0.0.1/any-time-money/uploads/img/game-5.jpg" alt="">
                            </div>
                            <div class="text-center py-4">
                                <a class="h6 text-decoration-none text-truncate" href="http://127.0.0.1/any-time-money/shop-detail/5">Product 4</a>
                                <div class="d-flex align-items-center justify-content-center mt-2">
                                    <h5>100000<i class="fas fa-rupee-sign ml-1"></i></h5>
                                    <h6 class="text-muted ml-2">60<i class="fas fa-thin fa-gem ml-1 text-primary"></i></h6>
                                </div>
                                <div class=" mb-4 pt-2">
                                    <button class="btn btn-primary px-3 disabled" data-toggle="modal" data-target="#myModal">Buy
                                        Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-12">
                    <nav>
                        <ul class="pagination justify-content-center">
                            <li class="page-item disabled"><a class="page-link" href="#">Previous</span></a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>
                </div> -->
                    <div class="col-lg-4 col-md-6 col-sm-6 pb-1">
                        <div class="product-item bg-light mb-4" style="height:620px">
                            <div class="product-img position-relative  overflow-hidden" style="height:452px">
                                <img class="img-fluid w-100" src="http://127.0.0.1/any-time-money/uploads/img/game-3.jpg" alt="">
                            </div>
                            <div class="text-center py-4">
                                <a class="h6 text-decoration-none text-truncate" href="http://127.0.0.1/any-time-money/shop-detail/3">Product 3</a>
                                <div class="d-flex align-items-center justify-content-center mt-2">
                                    <h5>10000<i class="fas fa-rupee-sign ml-1"></i></h5>
                                    <h6 class="text-muted ml-2">40<i class="fas fa-thin fa-gem ml-1 text-primary"></i></h6>
                                </div>
                                <div class=" mb-4 pt-2">
                                    <button class="btn btn-primary px-3 disabled" data-toggle="modal" data-target="#myModal">Buy
                                        Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-12">
                    <nav>
                        <ul class="pagination justify-content-center">
                            <li class="page-item disabled"><a class="page-link" href="#">Previous</span></a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>
                </div> -->
                    <div class="col-lg-4 col-md-6 col-sm-6 pb-1">
                        <div class="product-item bg-light mb-4" style="height:620px">
                            <div class="product-img position-relative  overflow-hidden" style="height:452px">
                                <img class="img-fluid w-100" src="http://127.0.0.1/any-time-money/uploads/img/game-2.jpg" alt="">
                            </div>
                            <div class="text-center py-4">
                                <a class="h6 text-decoration-none text-truncate" href="http://127.0.0.1/any-time-money/shop-detail/2">Product 2</a>
                                <div class="d-flex align-items-center justify-content-center mt-2">
                                    <h5>10000<i class="fas fa-rupee-sign ml-1"></i></h5>
                                    <h6 class="text-muted ml-2">100<i class="fas fa-thin fa-gem ml-1 text-primary"></i></h6>
                                </div>
                                <div class=" mb-4 pt-2">
                                    <button class="btn btn-primary px-3 disabled" data-toggle="modal" data-target="#myModal">Buy
                                        Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-12">
                    <nav>
                        <ul class="pagination justify-content-center">
                            <li class="page-item disabled"><a class="page-link" href="#">Previous</span></a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>
                </div> -->
                    <div class="col-lg-4 col-md-6 col-sm-6 pb-1">
                        <div class="product-item bg-light mb-4" style="height:620px">
                            <div class="product-img position-relative  overflow-hidden" style="height:452px">
                                <img class="img-fluid w-100" src="http://127.0.0.1/any-time-money/uploads/img/game-1.jpg" alt="">
                            </div>
                            <div class="text-center py-4">
                                <a class="h6 text-decoration-none text-truncate" href="http://127.0.0.1/any-time-money/shop-detail/1">Product 1</a>
                                <div class="d-flex align-items-center justify-content-center mt-2">
                                    <h5>500<i class="fas fa-rupee-sign ml-1"></i></h5>
                                    <h6 class="text-muted ml-2">10<i class="fas fa-thin fa-gem ml-1 text-primary"></i></h6>
                                </div>
                                <div class=" mb-4 pt-2">
                                    <button class="btn btn-primary px-3 disabled" data-toggle="modal" data-target="#myModal">Buy
                                        Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-12">
                    <nav>
                        <ul class="pagination justify-content-center">
                            <li class="page-item disabled"><a class="page-link" href="#">Previous</span></a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>
                </div> -->
                </div>
            </div>
            <!-- Shop Product End -->
        </div>
    </div>
    <!-- Shop End -->
    <script>
        function checkout_val() {
            var checkBox = $('input[type=checkbox]').is(':checked');

            if (checkBox == true) {
                $("#filters").removeClass('btn-disable');
            } else {
                $("#filters").addClass('btn-disable');
            }
        };
    </script><!-- Modal Login-->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h6>Please Login for futher process</h6>
                </div>
                <div class="modal-footer">
                    <a href="http://127.0.0.1/any-time-money/login"><button type="button" class="btn btn-warning">Login</button></a>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal Confirmation-->
    <div class="modal fade" id="myModalPrice" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <h6>Are you sure, you want to claim the price</h6>
                </div>
                <div class="modal-footer">
                    <form method="post" action="http://127.0.0.1/any-time-money/Site/price_add">
                        <input type="hidden" name="csrf_test_name" value="b48f62c116d9af91a71b46e210fb4fae">
                        <input type="hidden" name="game_id" id="product_pop_id" readonly>
                        <button type="submit" class="btn btn-outline-success">Yes</button>
                    </form>
                    <button type="button" data-dismiss="modal" class="btn btn-outline-danger">No</button>
                </div>
            </div>
        </div>
    </div>
@include('footer')
</body>

</html>