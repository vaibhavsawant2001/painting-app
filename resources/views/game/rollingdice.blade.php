<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script src="https://kit.fontawesome.com/2949d68d8a.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{ asset('css/game/style.css') }}">
    <script src="{{ asset('css/game/js/script.js') }}"></script>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@900&family=Signika:wght@600&display=swap"
        rel="stylesheet">

    <title>Responsive Buttons</title>
</head>
<body>
    <div class="container">
        <div id="vidbtnyourbetcontainer">
            <div class="liveVideoContainer">
                <div class="video-container">
                    <video width="1200" style="border-radius:10px; margin-top:20px;" id="videoHere" autoplay="1"
                        muted loop src="css/game/dice.mp4"></video>
                </div>
                <div class="lobby">
                    <a class="primary" onclick="window.dialog.showModal();" id="exitLink"><i
                            class="fa-solid fa-arrow-right-from-bracket"></i><span title="Lobby">Lobby</span></a>
                </div>
                <!-- Modal HTML structure -->
                <div id="exitModal" style="display:none;">
                    <div class="lobbymodalcontent">
                        <p>Are you sure you want to exit?</p>
                        <div class="yesnobtn">
                            <button id="exitYes">Yes</button>
                            <button id="exitNo">No</button>
                        </div>
                    </div>
                </div>
                <div class="icons" >
                    <div class="prevdiceicon">
                        <button><i id="diceIcon" class="fa-solid fa-dice"></i></button>
                    </div>
                    <div class="chaticon">
                        <button id="openChatBtn"><i id="chaticon" class="fa-solid fa-comment"></i></button>
                    </div>
                    
                </div>
            </div>
            <div class="button-container">
                <div class="button1x3">
                    <button class="bet-button" data-value="1" onclick="openModal(1)">1</button>
                    <button class="bet-button" data-value="2" onclick="openModal(2)">2</button>
                    <button class="bet-button" data-value="3" onclick="openModal(3)">3</button>
                </div>
                <div class="button2x3">
                    <button class="bet-button" data-value="4" onclick="openModal(4)">4</button>
                    <button class="bet-button" data-value="5" onclick="openModal(5)">5</button>
                    <button class="bet-button" data-value="6" onclick="openModal(6)">6</button>
                </div>
            </div>
            <div class="YourBetnBalance">
                <div class="yourbet">
                    <div class="yourbetchild1">
                        <h5>Game ID: <span>9307</span></h5>
                        <h4>Your Bet<span>₹1000</span></h4>
                    </div>
                </div>
                <div class="balance">
                    <div class="balanceRupees">₹23,900 <br><span>Balance</span></div>
                    <div class="totalBet">₹1,500 <br><span>Total Bet</span></div>
                </div>
            </div>
        </div>

        <!-- Modal -->
        <div id="myModal" class="modal">
            <div class="modal-content">
                <span class="close" onclick="closeModal()"><i class="fa-sharp fa-solid fa-circle-xmark"></i></span>
                <p id="modalContent"></p>
                <div class="btn-container">
                    <button id="button10" class="modal-button" onclick="selectValue('button10')">10</button>
                    <button id="button50" class="modal-button" onclick="selectValue('button50')">50</button>
                    <button id="button100" class="modal-button" onclick="selectValue('button100')">100</button>
                    <button id="button500" class="modal-button" onclick="selectValue('button500')">500</button>
                    <button id="button1000" class="modal-button" onclick="selectValue('button1000')">1000</button>
                </div>
                {{-- <!-- Range input below buttons -->
                <div class="range-container">
                    <input type="range" id="betRange" min="0" max="1000" step="10" value="10"
                        oninput="updateRangeValue()">
                    <span id="rangeValue">₹0</span>
                </div> --}}

                <div class="placebet-container">
                    <button class="placebet-button" onclick="submitValues()">Place Bet</button>
                </div>
            </div>
        </div>
        <div class="Last6RollContainer">
            <div id="PreviousDiceNumberContainer">
                <button id="closePrevDiceContainer"><i class="fa-solid fa-xmark"></i></button>
                <h3>Last 6 Rolls</h3>
                <div class="row">
                    <span>9881</span>
                    <button class="button">5</button>
                    <button class="button">3</button>
                    <button class="button">4</button>
                </div>
                <div class="row">
                    <span>9882</span>
                    <button class="button">6</button>
                    <button class="button">4</button>
                    <button class="button">2</button>
                </div>
                <div class="row">
                    <span>9883</span>
                    <button class="button">5</button>
                    <button class="button">3</button>
                    <button class="button">3</button>
                </div>
                <div class="row">
                    <span>9884</span>
                    <button class="button">2</button>
                    <button class="button">4</button>
                    <button class="button">6</button>
                </div>
                <div class="row">
                    <span>9885</span>
                    <button class="button">5</button>
                    <button class="button">4</button>
                    <button class="button">5</button>
                </div>
                <div class="row">
                    <span>9886</span>
                    <button class="button">3</button>
                    <button class="button">2</button>
                    <button class="button">1</button>
                </div>
            </div>
        </div>


        <!-- ChatBox Modal -->
        <div id="ChatBox">
            <div class="chathead">
                <h2><span>Chat</span></h2>
            </div>
            <div class="roll-hr">
                <label for="">Roll : 2542</label>
                <div class="hr"></div>
            </div>
            <button id="closeChatBtn"><i class="fa-solid fa-xmark"></i></button>
        </div>
    </div>

</body>

</html>

<script>
    //Opening Chatbox when click on chaticon 
    const openChatBtn = document.getElementById('openChatBtn');
    const chatModal = document.getElementById('ChatBox');
    const closeChatBtn = document.getElementById('closeChatBtn');

    // Open chat modal
    openChatBtn.addEventListener('click', () => {
        chatModal.style.display = 'block';
    });

    // Close chat modal
    closeChatBtn.addEventListener('click', () => {
        chatModal.style.display = 'none';
    });
</script>

<script>
    function submitValues() {
        if (selectedValues.length > 0) {
            closeModal();
            const chatMessagesContainer = document.getElementById('ChatBox');
            const message = "₹" + selectedValues.map(value => value.replace('button', '')).join(', ') +
                " - guest292585";
            const pElement = document.createElement('p');
            pElement.textContent = message;
            chatMessagesContainer.appendChild(pElement);
        } else {
            alert("Please select values first");
        }
    }
</script>

<script>
    // Get elements
    const diceIcon = document.getElementById('diceIcon');
    const previousDiceContainer = document.getElementById('PreviousDiceNumberContainer');

    // Show/hide previous dice container when dice icon is clicked
    diceIcon.addEventListener('click', () => {
        if (previousDiceContainer.style.display === 'none' || previousDiceContainer.style.display === '') {
            previousDiceContainer.style.display = 'block';
        } else {
            previousDiceContainer.style.display = 'none';
        }
    });
</script>

<script>
    // Function to show the modal
    function showModal() {
        document.getElementById('exitModal').style.display = 'block';
    }

    // Function to hide the modal
    function hideModal() {
        document.getElementById('exitModal').style.display = 'none';
    }

    // Function to handle the "Yes" button click
    function exitToLobby() {
        window.location.href = "{{ url('/') }}"; // Replace with your lobby URL
    }

    // Attach event listeners
    document.getElementById('exitLink').addEventListener('click', function(e) {
        e.preventDefault();
        showModal();
    });

    document.getElementById('exitYes').addEventListener('click', function() {
        exitToLobby();
    });

    document.getElementById('exitNo').addEventListener('click', function() {
        hideModal();
    });

    // Close the modal if the user clicks outside of it
    window.addEventListener('click', function(e) {
        if (e.target === document.getElementById('exitModal')) {
            hideModal();
        }
    });
</script>

<script>
    //Close the PreviousDiceNumberContainer when click on x button
    document.addEventListener('DOMContentLoaded', function() {
        // Get the close button element
        var closeButton = document.getElementById('closePrevDiceContainer');

        // Get the container element
        var container = document.getElementById('PreviousDiceNumberContainer');

        // Add a click event listener to the close button
        closeButton.addEventListener('click', function() {
            // Hide the container when the close button is clicked
            container.style.display = 'none';
        });
    });
</script>
