
<!DOCTYPE html>
<html lang="en">

@include('head')

<body>
    <!-- Topbar Start  -->

    <div class="col-lg-0 text-center text-lg-right b-block d-md-none" style="background-color: black; color: white;">
        <div class="d-inline-flex align-items-right">
                    </div>
    </div>
    <!-- Topbar End -->
@include('navbar')
<!-- Shop Start -->
<!-- Breadcrumb Start -->
<div class="container-fluid mt-4">
    <div class="row px-xl-5">
        <div class="col-12">
            <nav class="breadcrumb bg-light mb-30">
                <a class="breadcrumb-item text-decoration-none text-dark" href="http://127.0.0.1/any-time-money/">Home</a>
                <a class="breadcrumb-item text-decoration-none text-dark" href="http://127.0.0.1/any-time-money/games">Game</a>
                <span class="breadcrumb-item active">Game Details</span>
            </nav>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->
<!-- Shop Detail Start -->
<div class="container-fluid pb-5">
    <div class="row px-xl-5">
        <div class="col-lg-3 mb-30">
            <div id="product-carousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner bg-light">
                    <div class="carousel-item active">
                        <img class="w-100 h-50" src="http://127.0.0.1/any-time-money/uploads/img/product-5.jpg"
                            alt="Image">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-9 h-auto mb-30">
            <div class="h-100 bg-light p-30">
                
                                <h3>
                    game-5                </h3>
                <h3 class="font-weight-semi-bold mb-4">
                    <i class="fas fa-rupee-sign ml-1"></i>
                    4000                    <i class="fas fa-thin fa-star ml-1 text-primary"></i>
                    3424                </h3>
                                <p class="mb-4">short desc</p>
                                                <div class="d-flex align-items-center mb-4 pt-2">
                    <button class="btn btn-primary px-3 disabled" data-toggle="modal"
                        data-target="#myModal">Claim</button>
                </div>
                            </div>
        </div>
    </div>
        <div class="row px-xl-5">
        <div class="col">
            <div class="bg-light p-30">
                <div class="nav nav-tabs mb-4">
                    <a class="nav-item nav-link text-dark active" data-toggle="tab" href="#tab-pane-1">Description</a>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="tab-pane-1">
                        <h4 class="mb-3">Product Description</h4>
                        long desc                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
<!-- Shop Detail End -->

<!-- Products Start -->
<div class="container-fluid py-5">
    <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span class="bg-secondary pr-3">You May Also
            Like</span></h2>
    <div class="row px-xl-5">
        <div class="col">
            <div class="owl-carousel related-carousel">
                                <div class="product-item bg-light mb-4" style="height:610px">
                    <div class="product-img position-relative overflow-hidden" style="height:452px">
                        <img class="img-fluid w-100" src="http://127.0.0.1/any-time-money/uploads/img/product-4.jpg" alt="">
                    </div>
                    <div class="text-center py-4">
                        <a class="h6 text-decoration-none text-truncate"
                            href="http://127.0.0.1/any-time-money/game-detail/4">game-4</a>
                        <div class="d-flex align-items-center justify-content-center mt-2">
                            <h5>24000<i class="fas fa-rupee-sign ml-1"></i></h5>
                            <h6 class="text-muted ml-2">200<i
                                    class="fas fa-thin fa-star ml-1 text-primary"></i></h6>
                        </div>
                        <!--                         <div class=" mb-4 pt-2">
                            <button class="btn btn-primary px-3 disabled" data-toggle="modal"
                                data-target="#myModal">Claim</button>
                        </div>
                         -->
                    </div>
                </div>
                                <div class="product-item bg-light mb-4" style="height:610px">
                    <div class="product-img position-relative overflow-hidden" style="height:452px">
                        <img class="img-fluid w-100" src="http://127.0.0.1/any-time-money/uploads/img/product-3.jpg" alt="">
                    </div>
                    <div class="text-center py-4">
                        <a class="h6 text-decoration-none text-truncate"
                            href="http://127.0.0.1/any-time-money/game-detail/3">game-3</a>
                        <div class="d-flex align-items-center justify-content-center mt-2">
                            <h5>30000<i class="fas fa-rupee-sign ml-1"></i></h5>
                            <h6 class="text-muted ml-2">200<i
                                    class="fas fa-thin fa-star ml-1 text-primary"></i></h6>
                        </div>
                        <!--                         <div class=" mb-4 pt-2">
                            <button class="btn btn-primary px-3 disabled" data-toggle="modal"
                                data-target="#myModal">Claim</button>
                        </div>
                         -->
                    </div>
                </div>
                                <div class="product-item bg-light mb-4" style="height:610px">
                    <div class="product-img position-relative overflow-hidden" style="height:452px">
                        <img class="img-fluid w-100" src="http://127.0.0.1/any-time-money/uploads/img/product-2.jpg" alt="">
                    </div>
                    <div class="text-center py-4">
                        <a class="h6 text-decoration-none text-truncate"
                            href="http://127.0.0.1/any-time-money/game-detail/2">game-2</a>
                        <div class="d-flex align-items-center justify-content-center mt-2">
                            <h5>30002<i class="fas fa-rupee-sign ml-1"></i></h5>
                            <h6 class="text-muted ml-2">20<i
                                    class="fas fa-thin fa-star ml-1 text-primary"></i></h6>
                        </div>
                        <!--                         <div class=" mb-4 pt-2">
                            <button class="btn btn-primary px-3 disabled" data-toggle="modal"
                                data-target="#myModal">Claim</button>
                        </div>
                         -->
                    </div>
                </div>
                                <div class="product-item bg-light mb-4" style="height:610px">
                    <div class="product-img position-relative overflow-hidden" style="height:452px">
                        <img class="img-fluid w-100" src="http://127.0.0.1/any-time-money/uploads/img/product-1.jpg" alt="">
                    </div>
                    <div class="text-center py-4">
                        <a class="h6 text-decoration-none text-truncate"
                            href="http://127.0.0.1/any-time-money/game-detail/1">game-1</a>
                        <div class="d-flex align-items-center justify-content-center mt-2">
                            <h5>15<i class="fas fa-rupee-sign ml-1"></i></h5>
                            <h6 class="text-muted ml-2">10<i
                                    class="fas fa-thin fa-star ml-1 text-primary"></i></h6>
                        </div>
                        <!--                         <div class=" mb-4 pt-2">
                            <button class="btn btn-primary px-3 disabled" data-toggle="modal"
                                data-target="#myModal">Claim</button>
                        </div>
                         -->
                    </div>
                </div>
                            </div>
        </div>
    </div>
</div>
<!-- Products End --><!-- Modal Login-->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h6>Please Login for futher process</h6>
            </div>
            <div class="modal-footer">
                <a href="http://127.0.0.1/any-time-money/login"><button type="button" class="btn btn-warning">Login</button></a>
            </div>
        </div>

    </div>
</div>

<!-- Modal Confirmation-->
<div class="modal fade" id="myModalPrice" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h6>Are you sure, you want to claim the price</h6>
            </div>
            <div class="modal-footer">
                <form method="post" action="http://127.0.0.1/any-time-money/Site/price_add">
                    <input type="hidden" name="csrf_test_name"
                        value="b48f62c116d9af91a71b46e210fb4fae">
                    <input type="hidden" name="game_id" id="product_pop_id" readonly>
                    <button type="submit" class="btn btn-outline-success">Yes</button>
                </form>
                <button type="button" data-dismiss="modal" class="btn btn-outline-danger">No</button>
            </div>
        </div>
    </div>
</div>
@include('footer')
</body>

</html>