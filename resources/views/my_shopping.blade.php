
<!DOCTYPE html>
<html lang="en">
@include('head')
<body>
    <!-- Topbar Start  -->

    <div class="col-lg-0 text-center text-lg-right b-block d-md-none" style="background-color: black; color: white;">
        <div class="d-inline-flex align-items-right">

                            <a href="javascript:void(0);" class="btn px-0" style="color: white;"></a>
                <a href="javascript:void(0);" class="btn px-0">
                    <span class="badge text-secondary border border-secondary rounded-circle"
                        style="padding-bottom: 2px;">0</span>
                    <i class="fas fa-thin fa-gem text-primary"></i>
                </a>
                <a href="javascript:void(0);" class="btn px-0" style="color: white;"></a>
                <a href="javascript:void(0);" class="btn px-0">
                    <span class="badge text-secondary border border-secondary rounded-circle" style="padding-bottom: 2px;">
                        0                    </span>
                    <i class="fas fa-thin fa-star text-primary"></i>

                </a>
                <a href="http://127.0.0.1/any-time-money/favorite"
                    class="btn px-0 "
                    style="color: white;"></a>
                <a href="http://127.0.0.1/any-time-money/favorite" class="btn px-0">
                    <span class="badge text-secondary  border border-secondary rounded-circle" style="padding-bottom: 2px;">
                        0                    </span>
                    <i class="fas fa-heart text-primary"></i>
                </a>
                <a href="http://127.0.0.1/any-time-money/my_shopping"
                    class="btn px-0 text-warning"
                    style="color: white;">
                </a>
                <a href="http://127.0.0.1/any-time-money/my_shopping" class="btn px-0">
                    <span class="badge text-secondary  border border-secondary rounded-circle" style="padding-bottom: 2px;">
                        0                    </span>
                    <i class="fas fa-shopping-cart text-primary"></i>
                </a>
                    </div>
    </div>

    <!-- Topbar End -->
@include('navbar')
<!-- Shop Start -->
<!-- Breadcrumb Start -->
<div class="container-fluid mt-4">
    <div class="row px-xl-5">
        <div class="col-12">
            <nav class="breadcrumb bg-light mb-30">
                <a class="breadcrumb-item text-decoration-none text-dark" href="http://127.0.0.1/any-time-money/">Home</a>
                <span class="breadcrumb-item active">My Shopping</span>
            </nav>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->
<div class="container-fluid">
    <h5 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span class="bg-secondary pr-3">My Shopping</span>
    </h5>
    <div class="row px-xl-5">
        <!-- Shop Product Start -->
        <div class="col-lg-12 col-md-8">
            <div class="row pb-3">
                <div class="col-lg-12 col-md-6 col-sm-6 pb-1">
                    <div class="text-center product-item bg-light p-4 mb-4">Not Available</div>
                </div>            </div>
        </div>
        <!-- Shop Product End -->
    </div>
</div>
<!-- Shop End --><!-- Modal Login-->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h6>Please Login for futher process</h6>
            </div>
            <div class="modal-footer">
                <a href="http://127.0.0.1/any-time-money/login"><button type="button" class="btn btn-warning">Login</button></a>
            </div>
        </div>

    </div>
</div>

<!-- Modal Confirmation-->
<div class="modal fade" id="myModalPrice" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h6>Are you sure, you want to claim the price</h6>
            </div>
            <div class="modal-footer">
                <form method="post" action="http://127.0.0.1/any-time-money/Site/price_add">
                    <input type="hidden" name="csrf_test_name"
                        value="3e54aa43ed54dce6561ed6e204fee9d9">
                    <input type="hidden" name="game_id" id="product_pop_id" readonly>
                    <button type="submit" class="btn btn-outline-success">Yes</button>
                </form>
                <button type="button" data-dismiss="modal" class="btn btn-outline-danger">No</button>
            </div>
        </div>
    </div>
</div>
@include('footer')
</body>
</html>