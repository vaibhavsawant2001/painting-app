<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>Home | Any Time Money</title>
    <meta name="description" content="yaaaro" />
    <meta name="keywords" content="website desin and development company in mumbai, web development, web design, seo" />
    <!-- Favicon -->
    <link href="{{url('css/painting/uploads/img/favicon.ico')}}" rel="icon">
    <!-- Google Web Fonts -->
    <!-- <link rel="preconnect" href=""> -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <!-- Libraries Stylesheet -->
    <link href="{{url('css/painting/lib/animate/animate.min.css')}}" rel="stylesheet">
    <link href="{{url('css/painting/lib/owlcarousel/assets/owl.carousel.min.css')}}" rel="stylesheet">
    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{url('css/painting/css/style.css')}}" rel="stylesheet">
    <style>
    html,
    body {
        height: 100%;
    }
    </style>
</head>