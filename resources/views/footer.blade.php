    <!-- Footer Start -->
    <div class="container-fluid bg-dark mb-0 position-sticky" style="top:100vh">
            <center><img src="{{url('css/painting/uploads/img/logo_W.png')}}" alt=""
                    style="width: 30%; margin-top: 15px; margin-bottom: 15px "></center>
        </div>
        <!-- Footer End -->

        <!-- Back to Top -->
        <a href="#" class="btn btn-primary back-to-top"><i class="fa fa-angle-double-up"></i></a>

        <!-- JavaScript Libraries -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
        <script src="{{url('css/painting/lib/easing/easing.min.js')}}"></script>
        <script src="{{url('css/painting/lib/owlcarousel/owl.carousel.min.js')}}"></script>

        <!-- Contact Javascript File -->
        <!-- <script src="<? //= base_url() ?>mail/jqBootstrapValidation.min.js"></script> -->
        <!-- <script src="<? //= base_url() ?>mail/contact.js"></script> -->

        <!-- Template Javascript -->
        <script src="{{url('css/painting/js/main.js')}}"></script>
        <script>
        function pop_up_price(product_id) {
            $("#product_pop_id").val(product_id);
        }
        </script>