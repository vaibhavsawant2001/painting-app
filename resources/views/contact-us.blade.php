
<!DOCTYPE html>
<html lang="en">

@include('head')

<body>
    <!-- Topbar Start  -->

    <div class="col-lg-0 text-center text-lg-right b-block d-md-none" style="background-color: black; color: white;">
        <div class="d-inline-flex align-items-right">

                            <!-- <a href="http://127.0.0.1/any-time-money/" class="btn px-0" style="color: white;">Home</a>
                <a href="http://127.0.0.1/any-time-money/" class="btn px-0" style="color: white;">Register</a>
                <a href="http://127.0.0.1/any-time-money/" class="btn px-0" style="color: white;">Login</a> -->
                    </div>
    </div>

    <!-- Topbar End -->
@include('navbar')
<!-- Breadcrumb Start -->
<div class="container-fluid mt-4">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-decoration-none text-dark" href="http://127.0.0.1/any-time-money/">Home</a>
				<span class="breadcrumb-item active">Register</span>
			</nav>
		</div>
	</div>
</div>
<!-- Breadcrumb End -->

<!-- Contact Start -->
<div class="container-fluid" style="margin-top:15px ;">
	<h5 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span class="bg-secondary pr-3">Contact
			Us</span></h5>
	<div class="row px-xl-5">
		<div class="col-lg-7 mb-5">
			<div class="contact-form bg-light p-30">
												<form name="sentMessage" action="http://127.0.0.1/any-time-money/Site/contactdb" method="post" id="contactForm" >
					<input type="hidden" name="csrf_test_name"
						value="b48f62c116d9af91a71b46e210fb4fae">
					<div class="control-group mb-3">
						<input type="text" class="form-control" name="name" id="name" placeholder="Your Name" required="required"
							placeholder="Please enter your name" />
					</div>
					<div class="control-group mb-3">
						<input type="email" name="email" class="form-control"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" id="email" placeholder="Your Email" required="required"
							placeholder="Please enter your email" />
					</div>
					<div class="control-group mb-3">
						<textarea class="form-control" name="message" rows="8" id="message" placeholder="Message" required="required"
							placeholder="Please enter your message"></textarea>
					</div>
					<div>
						<button class="btn btn-primary py-2 px-4" type="submit" id="sendMessageButton">Send
							Message</button>
					</div>
				</form>
			</div>
		</div>
		<div class="col-lg-5 mb-5">

			<div class="bg-light p-30 mb-3" style="margin-top:0px;">

				<p class="mb-2"><i class="fa fa-envelope text-primary mr-3"></i>anytimemoney@gmail.com</p>
				<p class="mb-2"><i class="fa fa-phone-alt text-primary mr-3"></i>+91 987654321</p>
			</div>
		</div>
	</div>
</div>
<!-- Contact End --><!-- Modal Login-->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h6>Please Login for futher process</h6>
            </div>
            <div class="modal-footer">
                <a href="http://127.0.0.1/any-time-money/login"><button type="button" class="btn btn-warning">Login</button></a>
            </div>
        </div>

    </div>
</div>

<!-- Modal Confirmation-->
<div class="modal fade" id="myModalPrice" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h6>Are you sure, you want to claim the price</h6>
            </div>
            <div class="modal-footer">
                <form method="post" action="http://127.0.0.1/any-time-money/Site/price_add">
                    <input type="hidden" name="csrf_test_name"
                        value="b48f62c116d9af91a71b46e210fb4fae">
                    <input type="hidden" name="game_id" id="product_pop_id" readonly>
                    <button type="submit" class="btn btn-outline-success">Yes</button>
                </form>
                <button type="button" data-dismiss="modal" class="btn btn-outline-danger">No</button>
            </div>
        </div>
    </div>
</div>
@include('footer')
</body>

</html>