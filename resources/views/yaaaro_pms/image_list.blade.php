@include('yaaaro_pms/head')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Gallery
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Gallery</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center">
            <a href="{{ url('yaaaro_pms/add_image') }}" class="btn btn-primary">Add Image</a>
          </div>
          <div class="box-body table-responsive no-padding">
            <table id="gallery_table" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th hidden>ID</th>
                  <th>Image</th>
                  <th>Title</th>
                  <th>Main Category</th>
                  <th>Status</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                @foreach($imagelist as $image)
                <tr>
                  <td hidden>{{$image->id}}</td>
                  <td><img width="100" height="100" src="{{ url('public/' . $image->image) }}"></td>
                  <td>{{$image->image_title}}</td>
                  <td>{{$image->image_category}}</td>
                  <td>
                    <form method="POST" action="{{ route('gallery.status', ['id' => $image->id]) }}">
                      @csrf
                      @method('POST')

                      @if($image->status == 0)
                      <button type="submit" class="label label-danger">Denied</button>
                      @elseif($image->status == 1)
                      <button type="submit" class="label label-success">Approved</button>
                      @endif
                    </form>
                  </td>
                  <td>
                    <a href="{{ route('gallery.edit', $image->id) }}" class="label label-warning"><span class="label label-warning">Edit</span></a>
                  </td>
                  <td>
                    <form action="{{ route('gallery.destroy', $image->id) }}" method="post">
                      @csrf
                      @method('delete')
                      <button type="submit" class="btn btn-danger">Delete</button>
                    </form>
                  </td>

                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@include('yaaaro_pms/footer')