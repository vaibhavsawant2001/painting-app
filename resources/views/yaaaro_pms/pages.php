<?php
include_once('header.php');
    // Change Status
if ( isset($_REQUEST['page_id']) && isset($_REQUEST['page_status']) ) {
    $where_array = array( 'id' => strip_tags($_REQUEST['page_id']) );
    $update_array = array(  'status' => strip_tags($_REQUEST['page_status']) );    

    if($model->update("pages", $update_array, $where_array)){
        $succ = 'Status Update';
    }
}


?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    Web Pages
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Web Pages</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center"> 
                <!-- <a href="volunteer_add.php" class="btn btn-primary">Add foster</a> -->
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
                <thead>
              <tr>
                    <th> Page Name </th>
                    <th> Status </th>
              </tr>
              </thead>
              <tbody>
             <?php  
                if($datas = $model->singleselect("pages")){ 
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $page_name = $data['page_name'];                    
                        $status = $data['status'];                    
             ?>
                <tr>
                  <!-- <td> <?= $id; ?> </td>  -->
                  <td> <?= $page_name; ?> </td> 
                  <td>     
                    <?php if($status == '0') { ?>

                    <a  href="pages.php?page_status=1&page_id=<?= $id; ?> " class="label label-danger"> Denied </a>  

                    <?php } else { ?>

                    <a  href="pages.php?page_status=0&page_id=<?= $id; ?>" class="label label-success">Approved</a>  
                    <?php } ?>
                  </td>
               </tr>
            <?php } } ?>
            </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  
     

        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>