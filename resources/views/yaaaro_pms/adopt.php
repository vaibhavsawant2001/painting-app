<?php
include_once('header.php');
    
// Change Status
if ( isset($_REQUEST['service_id']) && isset($_REQUEST['service_status']) ) {
    $where_array = array( 'id' => strip_tags($_REQUEST['service_id']) );
    $update_array = array(  'status' => strip_tags($_REQUEST['service_status']) );    

    if($model->update("services", $update_array, $where_array)){
        $succ = 'Status Update';
    }
}
    // delete
elseif (isset($_REQUEST['service_del_id'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['service_del_id'])  );
    $stmt_del = $model->select('services',$where_array);
    foreach($stmt_del as $delete_image){
      if(!empty($delete_image['image'])){
            $deleteimage = '../'.$delete_image['image'];
            unlink($deleteimage);
      }
    }
  
    if($model->delete("services", $where_array)){
        $succ = 'Delete';
    }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    Adopt
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Adopt</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
            <thead>
              <tr>
                    <th>Category Name</th>
                    <th> </th>      
              </tr>
              </thead>
              <tbody>
                <tr>
                  <td> Cats </td>     
                  <td> <a href="adopt-animal.php?animal=Cats"> <span class="label label-warning"> View/Add Cats </span> </a> </td> 
               </tr>
                <tr>
                  <td> Dogs </td>     
                  <td> <a href="adopt-animal.php?animal=Dogs"> <span class="label label-warning"> View/Add Dogs </span> </a> </td> 
               </tr>
                <tr>
                  <td> Others </td>     
                  <td> <a href="adopt-animal.php?animal=Others"> <span class="label label-warning"> View/Add Others </span> </a> </td> 
               </tr>
            </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  
        <!-- <script type="text/javascript">
        var count= <?php //echo $num; ?>
            for(var i=1 ;i<=count; i++;){
            var longText = $('#discription'+i);
            longText.text(longText.text().substr(0, 500));
            }
        </script>   -->

        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>