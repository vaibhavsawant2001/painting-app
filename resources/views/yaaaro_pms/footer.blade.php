<footer class="main-footer text-center">
    <p class="footerCopyright">
        Designed By <a href="http://bwebdesignmumbai.in" target="_blank">Web Design Mumbai</a>
        <a title="Privacy Policy" href="http://yaaaro.com/" target="_blank">yaaaro</a>
        Powered By <a href="http://bluesuninfo.com/" target="_blank">Blue sun info</a>
        <a href="http://placementmumbai.com/" target="_blank">PM </a>© {{ date('Y') }}.
    </p>
</footer>
</div>
<script src="{{ asset('css/plugins/jQuery/jquery-2.2.3.min.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{ asset('css/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('css/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('css/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
    $(function () {
        $('#client_master').DataTable()
    });
    $(function () {
        $('#datatable').DataTable()
    });
</script>
<script src="{{ asset('css/bower_components/select2/dist/js/select2.full.min.js') }}"></script>
<script src="{{ asset('css/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('css/dist/js/adminlte.min.js') }}"></script>
<script src="{{ asset('css/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('css/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ asset('css/dist/js/demo.js') }}"></script>
<script>
    CKEDITOR.replace('editor', {
        height: 300,
        filebrowserUploadUrl: 'upload.php'
    });
</script>
<script>
    $('.select2').select2()
    $(document).ready(function () {
        $('.sidebar-menu').tree()
    })
    window.setTimeout(function () {
        $(".alert").fadeTo(500, 0).slideUp(500, function () {
            $(this).remove();
        });
    }, 4000);
</script>
</body>
</html>
