<?php

// ob_start();
// error_reporting(E_ALL);

include_once('header.php');
error_reporting(E_ERROR | E_PARSE);

// for edit
if(isset($_REQUEST['policy_id'])){    
  $edit_id = strip_tags($_REQUEST['policy_id']);
  $where = array( 'id' => $edit_id );
  if($others = $model->select('policy',$where)){
      foreach($others as $other){   
          $id = $other['id'];
          $title = $other['title'];
          $content = $other['content'];
      }
  }
}

if(isset($_POST['policy_edit'])){
  $policy_edit1 = 'policy_edit'; 
  $edit_id = $_POST['id'];

  $where_other = array( 
    'id' => $edit_id
  );

  $update_array = array(
          'title' => addslashes(strip_tags(htmlentities($_POST['title']))),
          'content' => $_POST['content'],
          'date' => $todayDate
  );
  if($model->update("policy", $update_array, $where_other)){
    move_uploaded_file($temp_file, $file1);
    $model->url('policy.php?succ=Update&policy_id='.$edit_id);
  }else{
      $model->url('policy_add.php?msg&policy_id='.$edit_id);
  }
}

// for insert
if(isset($_POST['submit'])){

      
      $insert_array = array(
          'title' => addslashes(strip_tags(htmlentities($_POST['title']))),
          'content' => $_POST['content'],
          'date' => $todayDate,
          'status' => '1'
      );
      if($model->insert("policy",$insert_array)){
            $model->url('policy.php?succ');
      }
      else
          $msg="faild";
    }  

?>

<script type="text/javascript" src="ckeditor/ckeditor.js">
</script>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add policy
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="policy.php">policy</a></li>
      <li class="active">Add Service</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">

        <div class="box box-primary">

          <div class="box-header with-border">
            <?php if (isset($_REQUEST['fail'])) {
              echo '<div class="alert alert-danger alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              Something Went Wrong....
              </div>';
            } ?>

          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form action="policy_add.php" enctype="multipart/form-data" method="POST">
            <div class="box-body">
               <?php if(isset($_REQUEST['policy_id'])){ ?>
                    <input type="hidden" class="form-control" name="id" value="<?= $edit_id; ?>" >
                 <?php } ?>
       
              
                <div class="form-group">
                  <label for="Service Name"> Service Name : </label>
                  <input type="text" class="form-control" name="title" value="<?php if(isset($_REQUEST['policy_id'])) { echo $title; } elseif(isset($_POST['title'])) { echo $_POST['title']; } else{ echo ''; } ?>">
                </div> 


              <div class="form-group">
                <label for="Contant"> Content : </label>
                <textarea class="form-control" rows="4" name="content" id="editor"><?php if(isset($_REQUEST['policy_id'])) { echo $content; } elseif(isset($_POST['content'])) { echo $_POST['content']; } else{ echo ''; } ?></textarea>
                <!-- <script>CKEDITOR.replace( 'content' );</script> -->
              </div>

           
            </div>


             <!-- /.box-body -->

             <div class="box-footer" align="center">
              <button type="submit" name="<?php if(isset($_REQUEST['policy_id'])) { echo 'policy_edit'; } elseif(isset($policy_edit1) == 'policy_edit') { echo 'policy_id'; } else{ echo 'submit'; } ?>" value="submit" class="btn btn-primary ">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>

<?php include('footer.php'); ?>