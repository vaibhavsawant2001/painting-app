<?php
    include('includes/conn.php');
    session_start();
    if(isset($_POST['submit'])){

        function test_input($data) {
            $data = trim($data);
            $data = strip_tags($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }

        $old_username = test_input($_POST['old_username']);
        $old_password = test_input($_POST['old_password']);
        $new_username = test_input($_POST['new_username']);
        $new_password = test_input($_POST['new_password']);
        $new_re_password = test_input($_POST['new_re_password']);
        $date = date('M d, Y h:i A');
        $status = '1';

    if(!empty($old_username) && !empty($old_password) && !empty($new_username) && !empty($new_password) && !empty($new_re_password) ) {
        $query = "select * from user where username = '$old_username' and password = '$old_password'";
        $run = mysqli_query($conn, $query);
        $result = mysqli_fetch_assoc($run);
        $id = $result['id'];
        $old_username1 = $result['username'];
        $old_password1 = $result['password'];
     
        if( $old_username1 ===  $old_username &&  $old_password1 === $old_password ){
            if($new_password === $new_re_password){
                $query = "UPDATE `user` SET `username`='$new_username',`password`='$new_password', `date`='$date',`status`='$status' WHERE `id`= '$id' "; 
                // echo $query;
                // exit();
                if(mysqli_query($conn, $query)){ 
                    header('location: index.php?succ'); 
                }  else{
                    echo '<script>alert("Something went wrong!")</script>';
                }
            }
            else{
                echo '<script>alert("password and re-password must be same")</script>';
            }    
        }
        else{
            echo '<script>alert("Old username and password is incorrect")</script>';
        }
    }
    echo '<script>alert("All Fields Required.")</script>';
}
// if(isset($_SESSION['user_crm'] ) && isset($_SESSION['pass_crm']) ){
//     session_destroy();
//     session_abort();
//     unset($_SESSION['user_crm']);
//     unset($_SESSION['pass_crm']);
//     header('location: index.php');
// }
?>


<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Yaaaro PMS | Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
<style>
    .has-error{
        color:red;
    }
</style>  
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#">
      <b><img src="uploads/logo.png"></b><br>
      <p>Change Password</p>
    </a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <!-- <p class="login-box-msg"> in to start your session</p> -->

    <form action="change_password.php" method="post">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="old_username" placeholder="old Username">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="old_password" placeholder="Old Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="new_username" placeholder="New Username">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="new_password" placeholder="New Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" name="new_re_password" placeholder="New Re-Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div> -->
        <!-- /.col -->
        <div class="col-lg-12 text-center">
          <a href="admin.php" class="btn btn-primary">Cancel</a>
          <button type="submit" name="submit" class="btn btn-primary">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <!-- <a href="#">I forgot my password</a><br> -->
    <!-- <a href="register.php" class="text-center">Register a new membership</a> -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>
