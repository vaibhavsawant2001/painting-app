@include('yaaaro_pms/head')

<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Company
    </h1>
    <ol class="breadcrumb">
      <li><a href="{{ url('admin') }}"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Company</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center">
            <a href="{{ url('yaaaro_pms/company_add') }}" class="btn btn-primary">Add Company</a>
          </div>
          <div class="box-body table-responsive no-padding">
            <table id="company_table" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Image</th>
                  <th>Title</th>
                  <th>Edit</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
                @foreach($company as $company)
                <tr>
                  <td>{{$company->id}}</td>
                  <td><img src="{{ url('public/' . $company->company_logo) }}" height="100" width="100"></td>
                  <td>{{$company->company_title}}</td>
                  <td><a href="{{ route('company.edit', $company->id) }}" class="label label-warning">Edit</a></td>
                  <td>
    <form action="{{ route('company.destroy', $company->id) }}" method="POST" onsubmit="return confirm('Are you sure you want to delete this company?')">
        @csrf
        @method('DELETE')
        <button type="submit" class="btn btn-danger">Delete</button>
    </form>
</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

@include('yaaaro_pms/footer')