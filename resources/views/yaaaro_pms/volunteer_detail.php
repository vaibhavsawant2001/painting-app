<?php
include_once('header.php');

if (isset($_REQUEST['v_id'])) {
    $where = array(
        'id' => $_REQUEST['v_id'],
        'user_type' => 'Volunteer'
    );
    if($datas = $model->select("volunteer",$where)){ 
        foreach($datas as $data){ 
            $id = $data['id'];
            $image = $data['image'];
            $name = $data['name'];
            $email = $data['email'];
            $phone = $data['phone'];
            $address = $data['address'];
            $city = $data['city'];
            $state = $data['state'];
            $country = $data['country'];
            $zip_code = $data['zip_code'];

            $dob = $data['dob'];
            $age = $data['age'];
            $type = $data['type'];
            $hobbies = $data['hobbies'];
            $occupation = $data['occupation'];
            $occupation_text = $data['occupation_text']; 
            $trained_volunteer = $data['trained_volunteer']; 
            $trained_text = $data['trained_text']; 
            $experience = $data['experience']; 
            $availability = $data['availability']; 
            $about_you = $data['about_you']; 
            $about_us = $data['about_us']; 

            $about = $data['about']; 
            $image = $data['image']; 
            $tc = $data['tc']; 
            $facebook = $data['facebook']; 
            $twitter = $data['twitter']; 
            $instagram = $data['instagram']; 
            $linkedin = $data['linkedin']; 
            $travel_in = $data['travel_in']; 
            $travel_out = $data['travel_out']; 
            $date = $data['date']; 
        }
    }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
             <?= $name; ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="volunteer.php"><i class="fa fa-dashboard"></i> Volunteer</a></li>
            <li class="active"> <?= $name; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- End Main Content -->
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box" style="padding: 15px;">
                    <!-- /.box-header -->
                    <div class="box-body rc_detail table-responsive no-padding">
                      
                            <div class="col-md-3 col-lg-3 col-xs-12">
                                <img class="rc_detail_img" src="<?= (!empty($image)) ? '../'.$image : '../assests/img/user.png'; ?>" alt="<?= $name; ?>" width="100%" />
                            </div>
                            <div class="col-md-6">
                                <label>Name:</label>
                                <span><?= $name; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>Email Address:</label>
                                <span><?= $email; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>Telephone / Mobile Number: </label>
                                <span><?= $phone; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>Date of Birth:</label>
                                <span><?= $dob; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>Address :</label>
                                <span><?= $address; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>City :</label>
                                <span><?= $city; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>State :</label>
                                <span><?= $state; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>Country :</label>
                                <span><?= $country; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>Zip/Postal Code :</label>
                                <span><?= $zip_code; ?></span>
                            </div>
                            <div class="col-md-12">
                                <hr>
                                <h2>Other Information</h2>
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <label>Q. Are You Over 18 :</label>
                                <span><?= $age; ?></span>
                            </div>      
                            <?php if(!empty($occupation)){ ?> 
                            <div class="col-md-12">
                                <label>Q. Are you student or working? :</label>
                                <span><?= $occupation; ?></span>, <span><?= $occupation_text; ?></span>
                            </div>  
                            <?php } if(!empty($trained_volunteer)){ ?>    
                            <div class="col-md-12">
                                <label>Q. Are you trained on any aspect of dealing with animals?  :</label>
                                <span><?= $trained_volunteer; ?></span>, <span><?= $trained_text; ?></span>
                            </div>
                            <?php } if(!empty($travel_in)){ ?> 
                            <div class="col-md-12">
                                <label>Q. Can you travel to different states within the country? :</label>
                                <span><?= $travel_in; ?></span>
                            </div>
                            <?php } if(!empty($travel_out)){ ?> 
                            <div class="col-md-12">
                                <label>Q. Can you travel outside of the country?   :</label>
                                <span><?= $travel_out; ?></span>
                            </div>
                            <?php } if(!empty($type)){ ?> 
                            <div class="col-md-12">
                                <label for="">Q. In which of the following areas are you interested in volunteering? :</label><br>
                                <span><?= $type; ?></span>
                            </div>
                            <?php } if(!empty($hobbies)){ ?> 
                            <div class="col-md-12">
                                <label>Q. What is your passion/hobbies?:</label>
                                <span><?= $hobbies; ?></span>
                            </div>       
                            <?php } if(!empty($experience)){ ?> 
                            <div class="col-md-12">
                                <label>Q. What is your experience/exposure in past as a volunteer?  :</label>
                                <span><?= $experience; ?></span>
                            </div>
                            <?php } if(!empty($availability)){ ?> 
                            <div class="col-md-12">
                                <label>Q. What is your availability? :</label>
                                <span><?= $availability; ?></span>
                            </div>
                            <?php } if(!empty($about_you)){ ?> 
                            <div class="col-md-12">
                                <label>Q. Is there anything else you like us to know about you? :</label>
                                <span><?= $about_you; ?></span>
                            </div>
                            <?php } if(!empty($about_us)){ ?> 
                            <div class="col-md-12">
                                <label>Q. How did you hear about us? :</label>
                                <span><?= $about_us; ?></span>
                            </div>
                            <?php } ?> 
                   
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.content -->
</div>


<?php include('footer.php'); ?>