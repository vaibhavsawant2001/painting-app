<?php

// ob_start();
// error_reporting(E_ALL);
// error_reporting(-1);

include_once('header.php');


// for edit
if(isset($_REQUEST['scat_id'])){    
  $edit_id = strip_tags($_REQUEST['scat_id']);
  $where = array( 'id' => $edit_id );
  if($others = $model->select('gallery_sub',$where)){
      foreach($others as $other){   
          $id = $other['id'];
          $image = $other['image'];
          $title = $other['title'];
          // $subject = $other['subject'];
          // $content = $other['content'];
          $blogs_cat_id = $other['gallery_id'];
      }
  }
}

if(isset($_POST['scat_edit'])){
  $blog_edit1 = 'scat_edit'; 
  $edit_id = $_POST['id'];

  $image1 = strip_tags($_POST['image1']);
  $image2 = $_FILES['image']['name'];
    if (empty($image2)) {
      $file = $image1;
    }
    else{  
      // for image replace
      $where = array( 'id' => $edit_id );
      $stmt_del = $model->select('gallery_sub',$where);
      foreach($stmt_del as $delete_image){
        $deleteimage = '../'.$delete_image['image'];
        unlink($deleteimage);
      }

    $about_file = $_FILES['image']['name'];
    $target_dir1 = '../uploads/program/';
    $target_dir = 'uploads/program/';
    $newfilename = date('dmYHis').str_replace(" ", "", basename($about_file));
    $file1 = $target_dir1 . basename($newfilename);
    $file = $target_dir . basename($newfilename);
    $uploadOk = 1;
    $temp_file = $_FILES["image"]["tmp_name"];
  }
//file

  // $filee = strip_tags($_POST['file1']);
  // $file2 = $_FILES['file']['name'];
  //   if (empty($file2)) {
  //     $fileed = $filee;
  //   }
  //   else{  
  //     // for image replace
  //     $where = array( 'id' => $edit_id );
  //     $stmt_del = $model->select('program_detail',$where);
  //     foreach($stmt_del as $delete_image){
  //       $deletefile = '../'.$delete_image['file'];
  //       unlink($deletefile);
  //     }

  //   $ed_file = $_FILES['file']['name'];
  //   $target_dir3 = '../uploads/program/';
  //   $target_dir2 = 'uploads/program/';
  //   $edfilename = date('dmYHis').str_replace(" ", "", basename($ed_file));
  //   $file3 = $target_dir3 . basename($edfilename);
  //   $fileed = $target_dir2 . basename($edfilename);
  //   $temped_file = $_FILES["file"]["tmp_name"];
  // }

  $where_other = array( 
    'id' => $edit_id
  );

    $image = $file;
    $title = addslashes(strip_tags(htmlentities($_POST['title'])));
    $blogs_cat_id = addslashes(strip_tags(htmlentities($_POST['gallery_id'])));
    // $subject = addslashes(strip_tags(htmlentities($_POST['subject'])));
    // $content = addslashes($_POST['content']);
    // $date = $todayDate;

  $update_array = array(
    'image' => $file,
    // 'file' => $fileed,
    'title' => $title,
    'gallery_id' => $blogs_cat_id,
    // 'content' => $content,
    // 'date' => $todayDate,
  );
  if($model->update("gallery_sub", $update_array, $where_other)){
    move_uploaded_file($temp_file, $file1);
    // move_uploaded_file($temped_file, $file3);
    $model->url('gallery_sub.php?succ=Update');
  }else{
      $model->url('gallery_add_sub.php?scat_id='.$edit_id);
  }
}

// for insert
if(isset($_POST['submit'])){

    $about_file = $_FILES['image']['name'];
    if(!empty($about_file)){
      $target_dir1 = '../uploads/program/';
      $target_dir = 'uploads/program/';
      $newfilename = date('dmYHis').str_replace(" ", "", basename($about_file));
      $file1 = $target_dir1 . basename($newfilename);
      $file = $target_dir . basename($newfilename);
      $uploadOk = 1;
      $temp_file = $_FILES["image"]["tmp_name"];
    }else{
      $file1 = '';
      $file ='';
    }
  
    $image = $file;
    $title = addslashes(strip_tags(htmlentities($_POST['title'])));
    // $subject = addslashes(strip_tags(htmlentities($_POST['subject'])));    
    $gallery_id = addslashes(strip_tags(htmlentities($_POST['gallery_id'])));
    // $content = addslashes($_POST['content']);
    // $date = $todayDate;
    $status = '1';

  $insert_array = array(
      'image' => $file,
      // 'subject' => $subject,
      'title' => $title,
      'gallery_id' => $gallery_id,
      // 'content' => $content,
      // 'date' => $todayDate,
      'status' => '1'
  );
  if($model->insert("gallery_sub",$insert_array)){
        move_uploaded_file($temp_file, $file1);
        $model->url('gallery_sub.php?succ');
  }
  else
      $msg="faild";
}  

?>

<script type="text/javascript" src="ckeditor/ckeditor.js">
</script>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add Subcategory
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="blogs.php">Subcategory</a></li>
      <li class="active">Add Subcategory</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">

        <div class="box box-primary">

          <div class="box-header with-border">
            <?php if (isset($_REQUEST['fail'])) {
              echo '<div class="alert alert-danger alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              Something Went Wrong....
              </div>';
            } ?>

          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form enctype="multipart/form-data" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">
            <div class="box-body">
               <?php if(isset($_REQUEST['scat_id'])){ ?>
                    <input type="hidden" class="form-control" name="id" value="<?= $edit_id; ?>" >
                 <?php } ?>
              
               <div class="row">
                <div class="form-group col-md-6">
                  <label for=" Name"> Name : </label>
                  <input type="text" class="form-control" name="title" required value="<?php if(isset($_REQUEST['scat_id'])) { echo $title; } elseif(isset($_POST['title'])) { echo $_POST['title']; } else{ echo ''; } ?>">
                </div>

                <div class="form-group col-md-6">
                  <label for=" Name">Category : </label>
                    <select class="form-control" name="gallery_id">
                    <?php
                    $where_array=array(
                    'status' => '1','type'=>'main'
                    );
                       if($datas = $model->select("gallery",$where_array)){ 
                        echo '<option value="">Select Category</option>';
                       foreach($datas as $data){ 
                        $id = $data['id'];
                        $title = $data['title'];
                    ?>
                   <option value="<?= $id; ?>" <?php if($gallery_id==$id) {
                    echo 'selected';
                    }
                    else 
                    {
                      echo "";
                    }
                  ?>>  <?= $title; ?> </option>
                   <?php  
                      }
                    }
                    ?>
                  </select>
                </div>
                <!-- <div class="form-group col-md-6">
                  <label for=" Name"> Subject : </label>
                  <input type="text" class="form-control" name="subject" required value="<?php if(isset($_REQUEST['scat_id'])) { echo $subject; } elseif(isset($_POST['subject'])) { echo $_POST['subject']; } else{ echo ''; } ?>">
                </div>
 -->
             <!--  <div class="form-group col-md-12">
                <label for="Contant"> Content : </label>
                <textarea class="form-control" rows="4" name="content" id="editor"><?php if(isset($_REQUEST['scat_id'])) { echo $content; } elseif(isset($_POST['content'])) { echo $_POST['content']; } else{ echo ''; } ?></textarea>
             </div>  -->

              <div class="form-group col-md-12">
                <label for="exampleInputFile">Image :- </label> <br />
                  
               <?php if(isset($_REQUEST['scat_id'])){ ?> 
                  <input type="hidden" name="image1" value="<?= $image; ?>">
                  
               <?php echo $image; }?>

                <input type="file" name="image" size="12"  data-toggle="tooltip"  data-placement="top" title="For Better Result Use Width and Height as Mention Above">  
              </div>
<!-- 
              <div class="form-group col-md-12">
                <label for="exampleInputFile">File :- </label> <br />
                  
               <?php if(isset($_REQUEST['scat_id'])){ ?> 
                  <input type="hidden" name="file1" value="<?= $file; ?>">
                  
               <?php echo $file; }?>

                <input type="file" name="file" size="12"  data-toggle="tooltip"  data-placement="top" title="For Better Result Use Width and Height as Mention Above">  
              </div>  
 -->
            </div>


             <!-- /.box-body -->

             <div class="box-footer" align="center">
              <button type="submit" name="<?php if(isset($_REQUEST['scat_id'])) { echo 'scat_edit'; } elseif(isset($scat_edit1) == 'scat_edit') { echo 'scat_id'; } else{ echo 'submit'; } ?>" value="submit" class="btn btn-primary ">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>

<?php include('footer.php'); ?>