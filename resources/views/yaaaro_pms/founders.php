<?php
include_once('header.php');
    
// Change Status
if ( isset($_REQUEST['founders_id']) && isset($_REQUEST['founders_status']) ) {
    $where_array = array( 'id' => strip_tags($_REQUEST['founders_id']) );
    $update_array = array(  'status' => strip_tags($_REQUEST['founders_status']) );    

    if($model->update("founders", $update_array, $where_array)){
        $succ = 'Status Update';
    }
}
    // delete
elseif (isset($_REQUEST['founders_del_id'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['founders_del_id'])  );
    $stmt_del = $model->select('founders',$where_array);
    foreach($stmt_del as $delete_image){
      if(!empty($delete_image['image'])){
        $deleteimage = '../'.$delete_image['image'];
        unlink($deleteimage);
      }
    }
    if($model->delete("founders", $where_array)){
        $succ = 'Delete';
    }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
        Our Founders
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Our Founders</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center"> 
                <a href="founders_add.php" class="btn btn-primary">Add Volunteer</a>
          </div>
          <?php if (isset($_REQUEST['succ'])) {
              echo '<div class="alert alert-success alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
             Founder Add Successfully...
              </div>';
            } ?>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
                <thead>
              <tr>
                    <th> Image </th>
                    <th> Name </th>
                    <th> Degree </th>    
                    <th> Email </th>
                    <th> Contact No. </th>
                    <th> Short Discription </th>
                    <th> Status </th>
                    <th> </th>
                    <th> </th>
              </tr>
              </thead>
              <tbody>
             <?php  
                if($datas = $model->singleselect("founders")){ 
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $image = $data['image'];
                        $name = $data['name'];
                        $email = $data['email'];
                        $phone = $data['phone'];
                        $degree = $data['degree'];
                        $heading = $data['heading'];
                        $status = $data['status'];                    
             ?>
                <tr>
                  <td> <img src="../<?= $image; ?>" height="100px" width="120px"> </td>  
                  <td> <?= $name; ?> </td> 
                  <td> <?= $degree; ?> </td>  
                  <td> <?= $email; ?> </td> 
                  <td> <?= $phone; ?> </td> 
                  <td> <?= $heading; ?> </td>  
                       
                                     
                  <td>     
                        <?php if($status == '0') { ?>

                        <a  href="founders.php?founders_status=1&founders_id=<?= $id; ?> " class="label label-danger"> Denied </a>  

                        <?php } else { ?>

                        <a  href="founders.php?founders_status=0&founders_id=<?= $id; ?>" class="label label-success">Approved</a>  
                        <?php } ?>
                  </td>
             
                  <td>
                      <a href="founders_add.php?founders_id=<?php echo $id; ?>"> <span class="label label-warning"> Edit </span> </a> 
                  </td>
                  <td>
                      <a href="founders.php?founders_del_id=<?php echo $id; ?>" onclick="return confirm('Are you sure you want to Remove?');"> <span class="label label-danger"> Delete </span> </a> 
                  </td> 
               </tr>
            <?php } } ?>
            </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  
     

        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>