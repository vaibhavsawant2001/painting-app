<?php

// ob_start();
// error_reporting(E_ALL);

include_once('header.php');
error_reporting(E_ERROR | E_PARSE);

// for edit
if(isset($_REQUEST['service_id'])){    
  $edit_id = strip_tags($_REQUEST['service_id']);
  $where = array( 'id' => $edit_id );
  if($others = $model->select('services',$where)){
      foreach($others as $other){   
          $id = $other['id'];
          $image = $other['image'];
          $image_title = $other['image_title'];
          $title = $other['title'];
          $subject = $other['subject'];
          $content = $other['content'];
          // $metatag = $other['metatag'];
      }
  }
}

if(isset($_POST['service_edit'])){
  $service_edit1 = 'service_edit'; 
  $edit_id = $_POST['id'];

  $image1 = strip_tags($_POST['image1']);
  $image2 = $_FILES['image']['name'];
    if (empty($image2)) {
      $file = $image1;
    }
    else{  
      // for image replace
      $stmt_del = $model->select('services',$where);
      foreach($stmt_del as $delete_image){
        $deleteimage = '../'.$delete_image['image'];
        unlink($deleteimage);
      }

    $about_file = $_FILES['image']['name'];
    $target_dir1 = '../uploads/events/';
    $target_dir = 'uploads/events/';
    $newfilename = date('dmYHis').str_replace(" ", "", basename($about_file));
    $file1 = $target_dir1 . basename($newfilename);
    $file = $target_dir . basename($newfilename);
    $uploadOk = 1;
    $temp_file = $_FILES["image"]["tmp_name"];
  }

  $where_other = array( 
    'id' => $edit_id
  );
  $main_id =  $_POST['cat_id'];
  $update_array = array(
          'image' => $file,
          'title' => addslashes(strip_tags(htmlentities($_POST['title']))),
          'edate' => addslashes(strip_tags(htmlentities($_POST['edate']))),
          'elocation' => addslashes(strip_tags(htmlentities($_POST['elocation']))),
          'subject' => addslashes(strip_tags(htmlentities($_POST['subject']))),
          'content' => $_POST['content'],
          'date' => $todayDate
  );
  if($model->update("services", $update_array, $where_other)){
    move_uploaded_file($temp_file, $file1);
    $model->url('services.php?main_id='.$main_id.'&service_id='.$service_id.'&Update');
  }else{
      $model->url('service_add.php?main_id='.$main_id.'&service_id='.$service_id.'&fail');
  }
}

// for insert
if(isset($_POST['submit'])){

   
        $about_file = $_FILES['image']['name'];
        if(!empty($about_file)){
        $target_dir1 = '../uploads/events/';
        $target_dir = 'uploads/events/';
        $newfilename = date('dmYHis').str_replace(" ", "", basename($about_file));
        $file1 = $target_dir1 . basename($newfilename);
        $file = $target_dir . basename($newfilename);
        $uploadOk = 1;
        $temp_file = $_FILES["image"]["tmp_name"];
        }else{
          $file = '';
          $file1 = '';
        }
      $main_id =  $_POST['cat_id'];
      $insert_array = array(
          'image' => $file,
          'cat_id' =>  $main_id,
          'type' => 'sub',
          'title' => addslashes(strip_tags(htmlentities($_POST['title']))),
          'edate' => addslashes(strip_tags(htmlentities($_POST['edate']))),
          'elocation' => addslashes(strip_tags(htmlentities($_POST['elocation']))),
          'subject' => addslashes(strip_tags(htmlentities($_POST['subject']))),
          'content' => $_POST['content'],
          'date' => $todayDate,
          'status' => '1'
      );
      if($model->insert("services",$insert_array)){
            move_uploaded_file($temp_file, $file1);
            $model->url('services.php?main_id='.$main_id.'&succ');
      }
      else
          $msg="faild";
    }  

?>

<script type="text/javascript" src="ckeditor/ckeditor.js">
</script>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add Events
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="services.php">Events</a></li>
      <li class="active">Add Events</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">

        <div class="box box-primary">

          <div class="box-header with-border">
            <?php if (isset($_REQUEST['fail'])) {
              echo '<div class="alert alert-danger alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              Something Went Wrong....
              </div>';
            } ?>

          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form enctype="multipart/form-data" method="POST">
            <div class="box-body">
               <?php if(isset($_REQUEST['service_id'])){ ?>
                    <input type="hidden" class="form-control" name="id" value="<?= $edit_id; ?>" >
                 <?php } ?>
               <?php if(isset($_REQUEST['main_id'])){ ?>
                    <input type="hidden" class="form-control" name="cat_id" value="<?= $_REQUEST['main_id']; ?>" >
                 <?php } ?>
              
                <div class="form-group">
                  <label for="Service Name">  Name : </label>
                  <input type="text" class="form-control" name="title" value="<?php if(isset($_REQUEST['service_id'])) { echo $title; } elseif(isset($_POST['title'])) { echo $_POST['title']; } else{ echo ''; } ?>">
                </div> 

                <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                          <label for="storie Name">Events Date : </label>
                          <input type="date" class="form-control" name="edate" value="<?php if(isset($_REQUEST['service_id'])) { echo $edate; } elseif(isset($_POST['edate'])) { echo $_POST['edate']; } else{ echo ''; } ?>">
                      </div> 
                  </div>
                  <div class="col-md-6">
                      <div class="form-group">
                        <label for="Service Name"> Events Location : </label>
                        <input type="text" class="form-control" name="elocation" value="<?php if(isset($_REQUEST['service_id'])) { echo $elocation; } elseif(isset($_POST['elocation'])) { echo $_POST['elocation']; } else{ echo ''; } ?>">
                      </div> 
                  </div>
              </div>                
              <div class="form-group">
                <label for="Contant">Short Content : </label>
                <textarea class="form-control" rows="4" name="subject"><?php if(isset($_REQUEST['service_id'])) { echo $subject; } elseif(isset($_POST['subject'])) { echo $_POST['subject']; } else{ echo ''; } ?></textarea>
              </div>
              <div class="form-group">
                <label for="Contant"> Content : </label>
                <textarea class="form-control" rows="4" name="content" id="editor"><?php if(isset($_REQUEST['service_id'])) { echo $content; } elseif(isset($_POST['content'])) { echo $_POST['content']; } else{ echo ''; } ?></textarea>
              </div>

              <div class="form-group">
                <label for="exampleInputFile">Image :- </label> <br />
                  
               <?php if(isset($_REQUEST['service_id'])){ ?> 
                  <input type="hidden" name="image1" value="<?= $image; ?>">
                   <img src="../<?= $image; ?>"  height="100" width="100px"/> <br /> <br />
               <?php }?>

                <input type="file" name="image" size="12"  data-toggle="tooltip"  data-placement="top" title="For Better Result Use Width and Height as Mention Above">   

              </div>  
            </div>


             <!-- /.box-body -->

             <div class="box-footer" align="center">
              <button type="submit" name="<?php if(isset($_REQUEST['service_id'])) { echo 'service_edit'; } elseif(isset($service_edit1) == 'service_edit') { echo 'service_id'; } else{ echo 'submit'; } ?>" value="submit" class="btn btn-primary ">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>

<?php include('footer.php'); ?>