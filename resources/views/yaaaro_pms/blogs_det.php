<?php
include_once('header.php');
      
// Change Status
if ( isset($_REQUEST['detl_id']) && isset($_REQUEST['detl_status']) ) {
    $where_array = array('id' => strip_tags($_REQUEST['detl_id']) );
    $update_array = array('status' => strip_tags($_REQUEST['detl_status']));    

    if($model->update("blog_detail", $update_array, $where_array)){
        $succ = 'Status Update';
    }
}
    // delete
elseif (isset($_REQUEST['det_del_id'])) {
  //main
    $where_array = array('id' => strip_tags($_REQUEST['det_del_id']) );
    $stmt_del = $model->select('program_detail',$where_array);
    foreach($stmt_del as $delete_image){
      if(!empty($delete_image['image'])){
            $deleteimage = '../'.$delete_image['image'];
            unlink($deleteimage);
      }
        if(!empty($delete_image['file'])){
            $deletefile = '../'.$delete_image['file'];
            unlink($deletefile);
      }
    }
   
    if($model->delete("program_detail", $where_array)){
        $succ = 'Delete';
    }
  }

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    Program Detail
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Program Detail</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center"> 
              <a href="prog_detail_add.php" class="btn btn-primary">Add Program Detail</a>                
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
                <thead>
              <tr>
                    <th hidden> ID </th> 
                    <th> Name </th>
                    <th> Category </th>
                    <th> Subcategory </th>
                    <th> Program Date </th>
                    <th> Program Location </th>
                    <th> Subject </th>
                    <th> Content </th>
                    <th> Image </th>
                    <th> File</th>
                    <th> Image Detail</th>
                    <th> Status</th>
                    <th> Edit</th>
                    <th> Delete</th>
              </tr>
              </thead>
              <tbody>
             <?php  
             if($datas = $model->singleselect("program_detail")){ 
                  $num = count($datas);
                  // echo $num; 
                  $i= '1';
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $prg_id=$data['program_cat_id'];
                        $p_sub_id=$data['program_sub_id'];
                        $title = $data['title'];
                        $date = $data['prg_date'];
                        $location = $data['prg_location'];
                        $subject = $data['subject'];
                        $content = $data['content'];
                        $image = $data['image'];
                        $file = $data['file']; 
                        $img = $data['img'];                   
                        $status = $data['status'];     
                                  
             ?>
                <tr>
                  <td hidden> <?= $id; ?> </td> 
                  <td><?= $title; ?></td>
                  <td><?php
                    $where_array=array('status' => '1','id'=>$prg_id);
                       if($datas = $model->select("program_category",$where_array)){ 
                       
                       foreach($datas as $data){ 
                        $Category = $data['title'];
                    ?>
                       <?= $Category; ?> 
                   <?php  
                      }
                    }
                    ?></td>
                    <td><?php
                    $where_array=array('status' => '1' ,'id'=>$p_sub_id);
                       if($datas = $model->select("program_subcategory",$where_array)){ 
                       
                       foreach($datas as $data){ 
                        $Subcategory = $data['title'];
                    ?>
                     <?= $Subcategory; ?> 
                   <?php  
                      }
                    }
                    ?></td>
                  <td><?= $date; ?></td>
                  <td><?= $location; ?></td>
                  <td><?= $subject; ?></td>
                  <td><?= $content; ?></td>
  
                  <td> <img src="../<?= $image; ?>" height="100px" width="120px"> </td>   
                  <td> <a href="../<?php echo $file ?>" class="label label-warning">Download</a></td> 
                  <td> <img src="../<?= $img; ?>" height="100px" width="120px"> </td>
                  <td>     
                        <?php if($status == '0') { ?>

                        <a  href="prog_detail.php?detl_status=1&detl_id=<?= $id; ?> " class="label label-danger"> Denied </a>  

                        <?php } else { ?>

                        <a  href="prog_detail.php?detl_status=0&detl_id=<?= $id; ?>" class="label label-success">Approved</a>  
                        <?php } ?>
                  </td>

             
                  <td>
                    <a href="prog_detail_add.php?detl_id=<?php echo $id; ?>"> <span class="label label-warning"> Edit </span> </a> 
                  </td>
                  <td>
                      <a href="prog_detail.php?det_del_id=<?php echo $id; ?>" onclick="return confirm('Are you sure you want to Remove?');"> <span class="label label-danger"> Delete </span> </a> 
                  </td> 
               </tr>
            <?php $i++;} } ?>
            </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  
        <!-- <script type="text/javascript">
        var count= <?php //echo $num; ?>
            for(var i=1 ;i<=count; i++;){
            var longText = $('#discription'+i);
            longText.text(longText.text().substr(0, 500));
            }
        </script>   -->

        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>