<?php
include_once('header.php');
// Change Status
if ( isset($_REQUEST['tag_id']) && isset($_REQUEST['tag_status']) ) {
    $where_array = array( 'id' => strip_tags($_REQUEST['tag_id']) );
    $update_array = array(  'status' => strip_tags($_REQUEST['tag_status']) );    

    if($model->update("hash_tag", $update_array, $where_array)){
        $succ = 'Status Update';
    }
}
    // delete
elseif (isset($_REQUEST['tag_del_id'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['tag_del_id'])  );
    $stmt_del = $model->select('hash_tag',$where_array);
    foreach($stmt_del as $delete_image){
      if(!empty($delete_image['image'])){
        $deleteimage = '../'.$delete_image['image'];
        unlink($deleteimage);
      }
    }
    if($model->delete("hash_tag", $where_array)){
        $succ = 'Delete';
    } 
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      #Tags
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">#Tags</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 


    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center"> 
                <a href="tag_add.php" class="btn btn-primary">Add #Tags</a>
                     
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
              <thead>
              <tr>

                <th > Sr. No. </th>
                <th> Title</th>    
               <!-- <th> Title-1</th>      
                <th> Content</th>       
                 <th> Title-3</th> -->
                <th> Status </th>      
                <th> Edit </th>
                <th> Delete </th>
              </tr>
              </thead>
              <tbody>
              <?php 
              $where_array = array('type'=> 'tag');
                if($datas = $model->select("hash_tag",$where_array)){ 
                        $i=1;  
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $title1 = $data['title'];
                        //$title2 = $data['title2'];
                        //$banner_type = $data['banner_type'];
                        //$image = $data['image'];
                        $status = $data['status'];                   
             ?>
                <tr>

                  <td> <?php echo $i; ?> </td> 
                  <!--<td> 
                  <?php if($banner_type=='image'){
                      echo '<img src="../'. $image.'" width="100px" height="100px" >' ; 
                  }if($banner_type=='video'){
                     $url = str_replace("watch?v=",'embed/',$image);    
                    echo '<iframe width="320"  allowfullscreen="allowfullscreen"  mozallowfullscreen="mozallowfullscreen"   msallowfullscreen="msallowfullscreen"   oallowfullscreen="oallowfullscreen"  webkitallowfullscreen="webkitallowfullscreen" src="'. $url .'"></iframe>';
                  }
                  ?>
                  </td>  -->  
                   <!--<td> <?= $title1; ?> </td>       
                  <td> <?= $title2; ?> </td>   -->     
                  <td> <?= $title1; ?> </td> 
                  <td>     
                    <?php if($status == '0') { ?>

                    <a  href="tag.php?tag_status=1&tag_id=<?= $id; ?> " class="label label-danger"> Denied </a>  

                    <?php } else { ?>

                    <a  href="tag.php?tag_status=0&tag_id=<?= $id; ?>" class="label label-success">Approved</a>  
                    <?php } ?>
                  </td>
                  <td> 
                    
                     
                  <a href="tag_add.php?tag_id=<?= $id; ?>"> <span class="label label-info"> Edit </span> </a>
                    
                  </td>
                  <td>
                    <a href="tag.php?tag_del_id=<?= $id; ?>" onclick="return confirm('Are you sure you want to delete ?')"> <span class="label label-danger"> Delete </span> </a> 
                  </td> 

                </tr>
            <?php $i++; }} ?>
            </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  


        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>