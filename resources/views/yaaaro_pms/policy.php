<?php
include_once('header.php');
    
// Change Status
if ( isset($_REQUEST['policy_id']) && isset($_REQUEST['policy_status']) ) {
    $where_array = array( 'id' => strip_tags($_REQUEST['policy_id']) );
    $update_array = array(  'status' => strip_tags($_REQUEST['policy_status']) );    

    if($model->update("policy", $update_array, $where_array)){
        $succ = 'Status Update';
    }
}
    // delete
elseif (isset($_REQUEST['policy_del_id'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['policy_del_id'])  );
  
    if($model->delete("policy", $where_array)){
        $succ = 'Delete';
    }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    Management Policy
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Management Policy</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center"> 
                <a href="policy_add.php" class="btn btn-primary">Add Policy</a>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
                <thead>
              <tr>
                    <th hidden> ID </th> 
                    <th> Name</th>
                    <th> Content</th>
                    <th> </th>      
                    <th> </th>
                    <th> </th>
              </tr>
              </thead>
              <tbody>
             <?php  
                if($datas = $model->singleselect("policy")){ 
                  $num = count($datas);
                  // echo $num; 
                  $i= '1';
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $title = $data['title'];
                        $content = $data['content'];
                        $status = $data['status'];                    
             ?>
                <tr>
                  <td hidden> <?= $id; ?> </td> 
                  <td> <?= $title; ?> </td> 
                  <td> <?= $content; ?> </td>        
                  <td>     
                        <?php if($status == '0') { ?>

                        <a  href="policy.php?policy_status=1&policy_id=<?= $id; ?> " class="label label-danger"> Denied </a>  

                        <?php } else { ?>

                        <a  href="policy.php?policy_status=0&policy_id=<?= $id; ?>" class="label label-success">Approved</a>  
                        <?php } ?>
                  </td>
             
                  <td>
                      <a href="policy_add.php?policy_id=<?php echo $id; ?>"> <span class="label label-warning"> Edit </span> </a> 
                  </td>
                  <td>
                      <a href="policy.php?policy_del_id=<?php echo $id; ?>" onclick="return confirm('Are you sure you want to Remove?');"> <span class="label label-danger"> Delete </span> </a> 
                  </td> 
               </tr>
            <?php $i++;} } ?>
            </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>