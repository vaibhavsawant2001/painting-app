<?php
include_once('header.php');

      
// Change Status
if ( isset($_REQUEST['blog_id']) && isset($_REQUEST['blog_status']) ) {
    $where_array = array( 'id' => strip_tags($_REQUEST['blog_id']) );
    $update_array = array(  'status' => strip_tags($_REQUEST['blog_status']) );    

    if($model->update("blogs", $update_array, $where_array)){
        $succ = 'Status Update';
    }
}
    // delete
elseif (isset($_REQUEST['blog_del_id'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['blog_del_id'])  );
    $stmt_del = $model->select('blogs',$where_array);
    foreach($stmt_del as $delete_image){
      if(!empty($delete_image['image'])){
            $deleteimage = '../'.$delete_image['image'];
            unlink($deleteimage);
      }
    }
    if($model->delete("blogs", $where_array)){
        $succ = 'Delete';
    }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    Blogs
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Blogs</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center"> 
              <a href="blogs_add.php?mid=<?= strip_tags($_REQUEST['mid']) ?>" class="btn btn-primary">Add Blogs</a>                
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
                <thead>
              <tr>
                    <th hidden> ID </th> 
                    <th> Image </th>
                    <th> Title</th>
                    <th> Subject</th>
                    <th> Date </th>      
                    <th> </th>
                    <th> </th>
                    <th> </th>
              </tr>
              </thead>
              <tbody>
             <?php  
             $where_array = array(
               'type' => 'sub',
               'sub_cat_id' => $_REQUEST['mid']
             );
                if($datas = $model->select("blogs",$where_array)){ 
                  $num = count($datas);
                  // echo $num; 
                  $i= '1';
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $image = $data['image'];
                        $pdf = $data['pdf'];
                        $title = $data['title'];
                        $subject = $data['subject'];
                        $content = $data['content'];
                        $date = $data['date'];                    
                        $status = $data['status'];     
                        $ext = pathinfo($pdf, PATHINFO_EXTENSION);               
             ?>
                <tr>
                  <td hidden> <?= $id; ?> </td> 
                  <td> <img src="../<?= $image; ?>" height="100px" width="120px"> </td>  
                  <td> <?= $title; ?> </td> 
                  <td> <?= $subject; ?> </td>  
                  <td ><?php echo $date; ?></td>
                       
                                     
                  <td>     
                        <?php if($status == '0') { ?>

                        <a  href="blogs.php?blog_status=1&blog_id=<?= $id; ?> " class="label label-danger"> Denied </a>  

                        <?php } else { ?>

                        <a  href="blogs.php?blog_status=0&blog_id=<?= $id; ?>" class="label label-success">Approved</a>  
                        <?php } ?>
                  </td>
             
                  <td>
                  <?php if($ext == 'pdf'){ ?>
                      <a href="blogs_pdf_add.php?blog_id=<?php echo $id; ?>"> <span class="label label-warning"> Edit </span> </a> 
                  <?php }else{ ?>
                    <a href="blogs_add.php?blog_id=<?php echo $id; ?>&mid=<?= $_REQUEST['mid']; ?>"> <span class="label label-warning"> Edit </span> </a> 
                  <?php } ?>
                  </td>
                  <td>
                      <a href="blogs.php?blog_del_id=<?php echo $id; ?>&mid=<?= $_REQUEST['mid']; ?>" onclick="return confirm('Are you sure you want to Remove?');"> <span class="label label-danger"> Delete </span> </a> 
                  </td> 
               </tr>
            <?php $i++;} } ?>
            </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  
        <!-- <script type="text/javascript">
        var count= <?php //echo $num; ?>
            for(var i=1 ;i<=count; i++;){
            var longText = $('#discription'+i);
            longText.text(longText.text().substr(0, 500));
            }
        </script>   -->

        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>