

<?php
include_once('header.php');

      
// Change Status
if ( isset($_REQUEST['scat_id']) && isset($_REQUEST['scat_status']) ) {
    $where_array = array( 'id' => strip_tags($_REQUEST['scat_id']) );
    $update_array = array(  'status' => strip_tags($_REQUEST['scat_status']) );    

    if($model->update("gallery_sub", $update_array, $where_array)){
        $succ = 'Status Update';
    }
}
    // delete
elseif (isset($_REQUEST['scat_del_id'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['scat_del_id'])  );
    $stmt_del = $model->select('gallery_sub',$where_array);
    foreach($stmt_del as $delete_image){
      if(!empty($delete_image['image'])){
            $deleteimage = '../'.$delete_image['image'];
            unlink($deleteimage);
      }
    }
    if($model->delete("gallery_sub", $where_array)){
        $succ = 'Delete';
    }
  }

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    Subcategory
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Subcategory</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center"> 
              <a href="gallery_add_sub.php" class="btn btn-primary">Add Subcategory</a>                
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
                <thead>
              <tr>
                  <th hidden> ID </th>                   
                  <th> Image</th> 
                  <th> Title</th>
                  <th> Main Category</th>
                  <th> Status </th>
                  <th> Edit </th>
                  <th> Delete </th>
                  
              </tr>
              </thead>
              <tbody>
              <?php  
             
                if($datas = $model->singleselect("gallery_sub")){ 
               
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $image = $data['image'];
                        $title = $data['title'];
                        // $subject = $data['subject'];
                        $program_cat_id = $data['gallery_id'];
                        // $content = $data['content'];
                         // $file = $data['file'];                    
                        $status = $data['status'];     
                        // $ext = pathinfo($pdf, PATHINFO_EXTENSION);               
             ?>
                <tr>
                  <td hidden> <?= $id; ?> </td> 
                  <td><img style="height:100px"src="../<?php echo $image; ?>"></td> 
                  <td> <?= $title; ?> </td> 

                  <td> 
                    <?php 
                    $where_array= array('id'=>$program_cat_id);
                  if($datas1 = $model->select("gallery",$where_array)){ 
               
                    foreach($datas1 as $data1){ 
                        $mtitle = $data1['title'];
                      }
                    echo $mtitle;
                    }
                    ?>
                   </td>
                    
                  <!-- <td> <?= $subject; ?> </td> -->  
                
                       
                 <!--  <td>
                    <a href="blogs.php?mid=<?php echo $id; ?>"> <span class="label label-warning">Add Subcategory  </span> </a> 
                  </td> -->
                                     
                  <td>     
                        <?php if($status == '0') { ?>

                        <a  href="gallery_sub.php?scat_status=1&scat_id=<?= $id; ?> " class="label label-danger"> Denied </a>  

                        <?php } else { ?>

                        <a  href="gallery_sub.php?scat_status=0&scat_id=<?= $id; ?>" class="label label-success">Approved</a>  
                        <?php } ?>
                  </td>

                  <td>
                    <a href="gallery_add_sub.php?scat_id=<?php echo $id; ?>" class="label label label-warning"> <span class="label label-warning"> Edit </span> </a> 
                  </td>
                  <td>
                      <a href="gallery_sub.php?scat_del_id=<?php echo $id; ?>" onclick="return confirm('Are you sure you want to Remove?');"> <span class="label label-danger"> Delete </span> </a> 
                  </td> 
               </tr>
            <?php } } ?>
            </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  
        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>