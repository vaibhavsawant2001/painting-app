<?php
include_once('header.php');
    

  // delete
if (isset($_REQUEST['ptr_del_id'])) {
  $where_array = array( 'id' => strip_tags($_REQUEST['ptr_del_id']) );
   if($model->delete("fund_partner", $where_array)){
      $succ = 'Delete';
  }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    Business & Corporate Partnership
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">  Business & Corporate Partnership</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center"> 
                <!-- <a href="volunteer_add.php" class="btn btn-primary">Add foster</a> -->
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
                <thead>
              <tr>
                    <th> Name </th>
                    <th> Comapany Name </th>
                    <th> Email </th>
                    <th> Contact No. </th>
                    <th> Sponsor</th>                 
                    <th> </th>
                    <th> </th>
              </tr>
              </thead>
              <tbody>
             <?php  
              $where = array(
                'type' => 'busi_ptnr'
              );
                if($datas = $model->select("fund_partner" ,$where)){ 
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $name = $data['name'];
                        $company_name = $data['company_name'];
                        $email = $data['email'];
                        $phone = $data['phone'];
                        $sponsor = $data['sponsor'];            
                        $sponsor_event = $data['sponsor_event'];            
                        $sponsor_dog = $data['sponsor_dog'];            
                        $sponsor_cat = $data['sponsor_cat'];            
                        $sponsor_spay = $data['sponsor_spay'];            
                        $sponsor_adoption = $data['sponsor_adoption'];            
                        $sponsor_acco = $data['sponsor_acco'];            
                        $sponsor_other = $data['sponsor_other'];            
             ?>
                <tr>
                  <td> <?= $name; ?> </td> 
                  <td> <?= $company_name; ?> </td> 
                  <td> <?= $email; ?> </td> 
                  <td> <?= $phone; ?> </td> 
                  <td> <?= $sponsor; ?> </td>  
                  <td> <a type="button" class="label label-info" data-toggle="modal" data-target="#myModal<?= $id; ?>">More Info.</a> </td>  
                  <td>
                      <a href="partnership.php?ptr_del_id=<?php echo $id; ?>" onclick="return confirm('Are you sure you want to Remove?');"> <span class="label label-danger"> Delete </span> </a> 
                  </td> 
               </tr>
                 <!-- Modal -->
                <div id="myModal<?= $id; ?>" class="modal fade" role="dialog">
                  <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"> Business & Corporate Partnership</h4>
                      </div>
                      <div class="modal-body">
                        <p><b>Name :</b> <?= $name; ?></p>
                        <p><b>Company Name :</b> <?= $company_name; ?></p>
                        <p><b>Email :</b> <?= $email; ?></p>
                        <p><b>Phone No. :</b> <?= $phone; ?></p>
                        <p><b>Sponor :</b> <?= $sponsor; ?></p>
                        <?php if(!empty($sponsor_event)){ ?>
                        <p><b>Sponor Event :</b> <?= $sponsor_event; ?></p>
                        <?php } if(!empty($sponsor_dog)){ ?>
                        <p><b>Sponor Dog :</b> <?= $sponsor_dog; ?></p>
                        <?php } if(!empty($sponsor_cat)){ ?>
                        <p><b>Sponor Cat:</b> <?= $sponsor_cat; ?></p>
                        <?php } if(!empty($sponsor_spay)){ ?>
                        <p><b>Sponor Spay:</b> <?= $sponsor_spay; ?></p>
                        <?php } if(!empty($sponsor_adoption)){ ?>
                        <p><b>Sponor Adoption:</b> <?= $sponsor_adoption; ?></p>
                        <?php } if(!empty($sponsor_acco)){ ?>
                        <p><b>Sponor Accommodation:</b> <?= $sponsor_acco; ?></p>
                        <?php } if(!empty($sponsor_other)){ ?>
                        <p><b>Sponor Other:</b> <?= $sponsor_other; ?></p>
                        <?php } ?>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                      </div>
                    </div>

                  </div>
                </div>
            <?php } } ?>
            </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  
     

        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>