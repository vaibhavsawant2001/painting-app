<?php
include_once('header.php');
    
// Change Status
if ( isset($_REQUEST['service_id']) && isset($_REQUEST['service_status']) ) {
    $where_array = array( 'id' => strip_tags($_REQUEST['service_id']) );
    $update_array = array(  'status' => strip_tags($_REQUEST['service_status']) );    

    if($model->update("services", $update_array, $where_array)){
        $succ = 'Status Update';
    }
}
    // delete
elseif (isset($_REQUEST['service_del_id'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['service_del_id'])  );
    $stmt_del = $model->select('services',$where_array);
    foreach($stmt_del as $delete_image){
      if(!empty($delete_image['image'])){
            $deleteimage = '../'.$delete_image['image'];
            unlink($deleteimage);
      }
    }
  
    if($model->delete("services", $where_array)){
        $succ = 'Delete';
    }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    Events
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Events</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center"> 
                <a href="service_add.php?main_id=<?= $_REQUEST['main_id']; ?>" class="btn btn-primary">Add Events</a>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
                <thead>
              <tr>
                    <th hidden> ID </th> 
                    <th> Image </th>
                    <th> Title</th>
                    <th> Content </th>
                    <th> Date </th>
                    <th> Location </th>                    
                    <th> Status</th>      
                    <th> Edit</th>
                    <th> Delete</th>
              </tr>
              </thead>
              <tbody>
             <?php  
                $where_array = array(
                    'cat_id' => $_REQUEST['main_id'],
                    'type' => 'sub'
                );
                if($datas = $model->select("services", $where_array)){ 
                  $num = count($datas);
                  // echo $num; 
                  $i= '1';
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $image = $data['image'];
                        $title = $data['title'];
                        $subject = $data['content'];
                        $date = $data['date'];
                        $localtion = $data['elocation'];
                        $status = $data['status'];                    
             ?>
                <tr>
                  <td hidden> <?= $id; ?> </td> 
                  <td> <img src="../<?= $image; ?>" height="100px" width="120px"> </td>  
                  <td> <?= $title; ?> </td> 
                  <td> <?= $subject; ?> </td>        
                  <td> <?= $date; ?> </td>
                  <td> <?= $localtion; ?> </td>                
                  <td>     
                        <?php if($status == '0') { ?>

                        <a  href="services.php?main_id=<?= $_REQUEST['main_id']; ?>&service_status=1&service_id=<?= $id; ?> " class="label label-danger"> Denied </a>  

                        <?php } else { ?>

                        <a  href="services.php?main_id=<?= $_REQUEST['main_id']; ?>&service_status=0&service_id=<?= $id; ?>" class="label label-success">Approved</a>  
                        <?php } ?>
                  </td>
             
                  <td>
                      <a href="service_add.php?main_id=<?= $_REQUEST['main_id']; ?>&service_id=<?php echo $id; ?>"> <span class="label label-warning"> Edit </span> </a> 
                  </td>
                  <td>
                      <a href="services.php?main_id=<?= $_REQUEST['main_id']; ?>&service_del_id=<?php echo $id; ?>" onclick="return confirm('Are you sure you want to Remove?');"> <span class="label label-danger"> Delete </span> </a> 
                  </td> 
               </tr>
            <?php $i++;} } ?>
            </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  
        <!-- <script type="text/javascript">
        var count= <?php //echo $num; ?>
            for(var i=1 ;i<=count; i++;){
            var longText = $('#discription'+i);
            longText.text(longText.text().substr(0, 500));
            }
        </script>   -->

        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>