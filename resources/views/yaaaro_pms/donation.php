<?php
include_once('header.php');


    // delete
if (isset($_REQUEST['d_del_id'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['d_del_id']) );
   
    if($model->delete("donation", $where_array)){
        $succ = 'Delete';
    }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Donation
        </h1>
        <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Donation</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- End Main Content -->
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header text-center">
                        <!-- <a href="volunteer_add.php" class="btn btn-primary">Add Volunteer</a> -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table id="datatable" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th> Title</th>
                                    <!-- <th> Header </th> -->
                                    <th> Content </th>
                                    <th> Status</th>
                                    <th> Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  
                                    $sql = "SELECT * FROM `home` WHERE `id`='3'";
                                    $datas = mysqli_query($conn, $sql);
                                  while($data = mysqli_fetch_assoc($datas)){
                                            $id = $data['id'];
                                            $title = $data['title'];
                                            $heading = $data['heading'];
                                            $content = $data['content'];
                                            $status = $data['status'];                    
                                ?>
                                <tr>
                                    <td> <?= $title; ?> </td>
                                    <!-- <td> <?= $heading; ?> </td> -->
                                    <td> <?= substr($content,0,500); ?>... </td>
                                    <td class="action-btn">
                                        <?php if($status == '0') { ?>

                                        <a href="donation.php?home_status=1&home_id=<?= $id; ?> "
                                            class="label label-danger"> Denied </a>

                                        <?php } else { ?>

                                        <a href="donation.php?home_status=0&home_id=<?= $id; ?>"
                                            class="label label-success">Approved</a>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <a href="home_add.php?home_id=<?php echo $id; ?>&url=donation"> <span
                                                class="label label-info"> Edit </span> </a>

                                        <!-- <a href="home.php?home_del_id=<?php echo $id; ?>"> <span class="label label-danger"> Delete </span> </a>  -->
                                    </td>
                                </tr>
                                <?php  } ?>
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <div class="box">
                    <div class="box-header text-center">
                        <!-- <a href="volunteer_add.php" class="btn btn-primary">Add rescue_centers</a> -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table id="client_master" class="table table-bordered table-striped">
                            <thead>
                                <tr>                                  
                                    <th> Name</th>
                                    <th> Email </th>
                                    <th> Phone </th>
                                    <th> Address </th>
                                    <th> Donation Type </th>
                                    <th> Country </th>
                                    <th> Amount </th>
                                    <th> General shelter supplies </th>
                                    <th> Dog & Cat Suppplies</th>
                                    <th> Note </th>
                                    <th> Date </th>
                                    <th> </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php  
                                    if($datas = $model->singleselect("donation")){ 
                                        foreach($datas as $data){ 
                                            $id = $data['id'];
                                            $name = $data['name'];
                                            $email = $data['email'];
                                            $phone = $data['phone'];
                                            $address = $data['address'];
                                            $d_type = $data['d_type'];
                                            $country_curr = $data['country_curr'];
                                            $amount = $data['amount'];
                                            $general_sup = $data['general_sup'];                    
                                            $dc_sup = $data['dc_sup'];                    
                                            $note = $data['note'];                    
                                            $date = $data['date'];                    
                                ?>
                                <tr>
                                    <td> <?= $name; ?> </td>
                                    <td> <?= $email; ?> </td>
                                    <td> <?= $phone; ?> </td>
                                    <td> <?= $address; ?> </td>
                                    <td> <?= $d_type; ?> </td>
                                    <td> <?= $country_curr; ?> </td>
                                    <td> <?= ($country_curr === 'USA') ? "$ $amount":"Rs.$amount"; ?> </td>
                                    <td> <?= $general_sup; ?> </td>
                                    <td> <?= $dc_sup; ?> </td>
                                    <td> <?= $note; ?> </td>
                                    <td> <?= $date; ?> </td>
                                    <td>
                                        <a href="donation.php?d_del_id=<?php echo $id; ?>"  onclick="return confirm('Are you sure you want to Remove?');"> <span class="label label-danger"> Delete </span> </a>
                                    </td>
                                </tr>
                                <?php } } ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>


            <!-- ./col -->
            <!-- ./col -->
            <!-- ./col -->
            <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
</div>


<?php include('footer.php'); ?>