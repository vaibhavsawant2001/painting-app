<?php
include_once('header.php');

      
// Change Status
if ( isset($_REQUEST['run_id']) && isset($_REQUEST['run_status']) ) {
    $where_array = array( 'id' => strip_tags($_REQUEST['run_id']) );
    $update_array = array(  'status' => strip_tags($_REQUEST['run_status']) );    

    if($model->update("runner", $update_array, $where_array)){
        $succ = 'Status Update';
    }
}
    // delete
elseif (isset($_REQUEST['run_del_id'])) {
  
    $where_array = array( 'id' => strip_tags($_REQUEST['run_del_id'])  );
    $stmt_del = $model->select('runner',$where_array);
    foreach($stmt_del as $delete_image){
      if(!empty($delete_image['image'])){
            $deleteimage = '../'.$delete_image['image'];
            unlink($deleteimage);
      }
    }
    if($model->delete("runner", $where_array)){
        $succ = 'Delete';
    }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    RunnerUPS
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">RunnerUPS</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center"> 
              <a href="top_run_add.php" class="btn btn-primary">Add RunnerUPS</a>                
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
                <thead>
              <tr>
                   <th hidden> ID </th> 
                   <th> Title</th>
                    <!-- <th> Main Category</th> -->
                    <th> File </th>
                    <th> Status</th>
                    <th> Edit</th>
                    <th> Delete</th>
                  
              </tr>
              </thead>
              <tbody>
              <?php  
             
                if($datas = $model->singleselect("runner")){ 
               
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $image = $data['image'];
                        $title = $data['title'];

                        // $program_cat_id = $data['program_cat_id'];
                        // $content = $data['content'];
                        // $date = $data['date'];                    
                        $status = $data['status'];     
                        // $ext = pathinfo($pdf, PATHINFO_EXTENSION);               
             ?>
                <tr>
                  <td hidden> <?= $id; ?> </td> 
                  <td> <?= $title; ?> </td> 
                  
                   <td> <img src="../<?= $image; ?>" height="100px" width="120px"> </td>  
                 <td>     
                        <?php if($status == '0') { ?>

                        <a  href="top_run.php?run_status=1&run_id=<?= $id; ?> " class="label label-danger"> Denied </a>  

                        <?php } else { ?>

                        <a  href="top_run.php?run_status=0&run_id=<?= $id; ?>" class="label label-success">Approved</a>  
                        <?php } ?>
                  </td>

             
                  <td>
                    <a href="top_run_add.php?run_id=<?php echo $id; ?>" class="label label label-warning"> <span class="label label-warning"> Edit </span> </a> 
                  </td>
                  <td>
                      <a href="top_run.php?run_del_id=<?php echo $id; ?>" onclick="return confirm('Are you sure you want to Remove?');"> <span class="label label-danger"> Delete </span> </a> 
                  </td> 
               </tr>
            <?php } } ?>
            </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  
       </div>
     </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>