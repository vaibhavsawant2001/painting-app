<?php
include_once('header.php');
// Change Status
if ( isset($_REQUEST['banner_id']) && isset($_REQUEST['banner_status']) ) {
    $where_array = array( 'id' => strip_tags($_REQUEST['banner_id']) );
    $update_array = array(  'status' => strip_tags($_REQUEST['banner_status']) );    

    if($model->update("why_us", $update_array, $where_array)){
        $succ = 'Status Update';
    }
}
    // delete
elseif (isset($_REQUEST['banner_del_id'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['banner_del_id'])  );
    $stmt_del = $model->select('why_us',$where_array);
    foreach($stmt_del as $delete_image){
      if(!empty($delete_image['image'])){
        $deleteimage = '../'.$delete_image['image'];
        unlink($deleteimage);
      }
    }
    if($model->delete("why_us", $where_array)){
        $succ = 'Delete';
    } 
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Why Us
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Why Us</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 


    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center"> 
                <a href="why_us_add.php" class="btn btn-primary">Add Why Us</a>
                     
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
              <thead>
              <tr>

                <th > Sr. No. </th>
                <th> Image</th>    
                <th> Heading</th>      
                <th> Title</th>       
                <!-- <th> Title-3</th> -->
                <th> Status </th>      
                <th> Edit </th>
                <th> Delete </th>
              </tr>
              </thead>
              <tbody>
              <?php 
              $where_array = array(
                'type'=> 'why_us'
              );
                if($datas = $model->select("why_us",$where_array)){ 
                        $i=1;  
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $heading = $data['heading'];
                        $title1 = $data['title'];
                        //$title2 = $data['title2'];
                        //$banner_type = $data['banner_type'];
                        $image = $data['image'];
                        $status = $data['status'];                   
             ?>
                <tr>

                  <td> <?php echo $i; ?> </td> 

                  <td> <?php echo '<img src="../'. $image.'" width="100px" height="100px" >' ; ?>
                  </td>  
                  <td> <?= $heading; ?> </td>  
                   <td> <?= $title1; ?> </td>       
                  <!--<td> <?= $title2; ?> </td>       
                  <td> <?= $title3; ?> </td>  -->
                  <td>     
                    <?php if($status == '0') { ?>

                    <a  href="why_us.php?banner_status=1&banner_id=<?= $id; ?> " class="label label-danger"> Denied </a>  

                    <?php } else { ?>

                    <a  href="why_us.php?banner_status=0&banner_id=<?= $id; ?>" class="label label-success">Approved</a>  
                    <?php } ?>
                  </td>
                  <td> 
                    
                      
                  <a href="why_us_add.php?banner_id=<?= $id; ?>"> <span class="label label-info"> Edit </span> </a>
                    
                  </td>
                  <td>
                    <a href="why_us.php?banner_del_id=<?= $id; ?>" onclick="return confirm('Are you sure you want to delete ?')"> <span class="label label-danger"> Delete </span> </a> 
                  </td> 

                </tr>
            <?php $i++; }} ?>
            </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  


        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>