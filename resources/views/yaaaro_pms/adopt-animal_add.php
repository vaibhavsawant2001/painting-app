<?php

// ob_start();
// error_reporting(E_ALL);

include_once('header.php');
error_reporting(E_ERROR | E_PARSE);

// for edit
if(isset($_REQUEST['animal_id'])){    
  $edit_id = strip_tags($_REQUEST['animal_id']);
  $where = array( 'id' => $edit_id );
  if($others = $model->select('adopt',$where)){
      foreach($others as $data){   
          $id = $data['id'];
          $image = $data['image'];
          $name = $data['name'];
          $age = $data['age'];
          $gender = $data['gender'];
          $color = $data['color'];
          $weight = $data['weight'];
          $price = $data['price'];    
      }
  }
}

if(isset($_POST['animal_edit'])){
  $service_edit1 = 'animal_edit'; 
  $edit_id = $_POST['id'];

  $image1 = strip_tags($_POST['image1']);
  $image2 = $_FILES['image']['name'];
    if (empty($image2)) {
      $file = $image1;
    }
    else{  
      // for image replace
      $stmt_del = $model->select('adopt',$where);
      foreach($stmt_del as $delete_image){
        $deleteimage = '../'.$delete_image['image'];
        unlink($deleteimage);
      }

    $about_file = $_FILES['image']['name'];
    $target_dir1 = '../uploads/adopt/';
    $target_dir = 'uploads/adopt/';
    $newfilename = date('dmYHis').str_replace(" ", "", basename($about_file));
    $file1 = $target_dir1 . basename($newfilename);
    $file = $target_dir . basename($newfilename);
    $uploadOk = 1;
    $temp_file = $_FILES["image"]["tmp_name"];
  }

  $where_other = array( 
    'id' => $edit_id
  );
  $animal =  $_POST['type'];
  $update_array = array(
          'image' => $file,
          'type' => $animal,
          'name' => addslashes(strip_tags(htmlentities($_POST['name']))),
          'age' => addslashes(strip_tags(htmlentities($_POST['age']))),
          'gender' => addslashes(strip_tags(htmlentities($_POST['gender']))),
          'color' => addslashes(strip_tags(htmlentities($_POST['color']))),
          'weight' => addslashes(strip_tags(htmlentities($_POST['weight']))),
          'price' => addslashes(strip_tags(htmlentities($_POST['price']))),
          'date' => $todayDate
  );
  if($model->update("adopt", $update_array, $where_other)){
    move_uploaded_file($temp_file, $file1);
    $model->url('adopt-animal.php?animal='.$animal.'&animal_id='.$edit_id.'&Update');
  }else{
      $model->url('adopt-animal_add.php?animal='.$animal.'&animal_id='.$edit_id.'&fail');
  }
}

// for insert
if(isset($_POST['submit'])){

   
        $about_file = $_FILES['image']['name'];
        if(!empty($about_file)){
        $target_dir1 = '../uploads/adopt/';
        $target_dir = 'uploads/adopt/';
        $newfilename = date('dmYHis').str_replace(" ", "", basename($about_file));
        $file1 = $target_dir1 . basename($newfilename);
        $file = $target_dir . basename($newfilename);
        $uploadOk = 1;
        $temp_file = $_FILES["image"]["tmp_name"];
        }else{
          $file = '';
          $file1 = '';
        }
        $type = $_POST['type'];
        $insert_array = array(
            'image' => $file,
            'type' => $type,
            'name' => addslashes(strip_tags(htmlentities($_POST['name']))),
            'age' => addslashes(strip_tags(htmlentities($_POST['age']))),
            'gender' => addslashes(strip_tags(htmlentities($_POST['gender']))),
            'color' => addslashes(strip_tags(htmlentities($_POST['color']))),
            'weight' => addslashes(strip_tags(htmlentities($_POST['weight']))),
            'price' => addslashes(strip_tags(htmlentities($_POST['price']))),
            'date' => $todayDate,
            'status' => '1'
        );
        if($model->insert("adopt",$insert_array)){
                move_uploaded_file($temp_file, $file1);
                $model->url('adopt-animal.php?animal='.$type.'&succ');
        }
        else
            $msg="faild";
    }  

?>

<script type="text/javascript" src="ckeditor/ckeditor.js">
</script>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add adopt
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="adopt.php">adopt</a></li>
      <li class="active">Add adopt</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">

        <div class="box box-primary">

          <div class="box-header with-border">
            <?php if (isset($_REQUEST['fail'])) {
              echo '<div class="alert alert-danger alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              Something Went Wrong....
              </div>';
            } ?>

          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form enctype="multipart/form-data" method="POST">
            <div class="box-body">
               <?php if(isset($_REQUEST['animal_id'])){ ?>
                    <input type="hidden" class="form-control" name="id" value="<?= $edit_id; ?>" >
                 <?php } ?>
               <?php if(isset($_REQUEST['animal'])){ ?>
                    <input type="hidden" class="form-control" name="type" value="<?= $_REQUEST['animal']; ?>" >
                 <?php } ?>
              
                <div class="form-group col-md-6">
                  <label for="Service Name"> Name : </label>
                  <input type="text" class="form-control" name="name" value="<?php if(isset($_REQUEST['animal_id'])) { echo $name; } elseif(isset($_POST['name'])) { echo $_POST['name']; } else{ echo ''; } ?>">
                </div> 
                <div class="form-group col-md-6">
                  <label for="Service Name"> Age: </label>
                  <input type="text" class="form-control" name="age" value="<?php if(isset($_REQUEST['animal_id'])) { echo $age; } elseif(isset($_POST['age'])) { echo $_POST['age']; } else{ echo ''; } ?>">
                </div> 
                <div class="form-group col-md-6">
                  <label for="Gender Name"> Gender: </label>
                    <select class="form-control" name="gender" required="required">
                        <option value="">Select Gender</option>
                        <option value="Male" <?php if(isset($_REQUEST['animal_id'])) { echo ($gender === 'Male')? 'selected': ''; } elseif(isset($_POST['gender'])){echo ($_POST['gender'] === 'Male')? 'selected': '';} ?>>Male</option>
                        <option value="Female" <?php if(isset($_REQUEST['animal_id'])) { echo ($gender === 'Female')? 'selected': ''; } elseif(isset($_POST['gender'])){echo ($_POST['gender'] === 'Female')? 'selected': '';} ?>>Female</option>
                    </select>
                </div> 
                <div class="form-group col-md-6">
                  <label for="Service Name"> Color : </label>
                  <input type="text" class="form-control" name="color" value="<?php if(isset($_REQUEST['animal_id'])) { echo $color; } elseif(isset($_POST['color'])) { echo $_POST['color']; } else{ echo ''; } ?>">
                </div> 
                <div class="form-group col-md-6">
                  <label for="Service Name"> Weight : </label>
                  <input type="text" class="form-control" name="weight" value="<?php if(isset($_REQUEST['animal_id'])) { echo $weight; } elseif(isset($_POST['weight'])) { echo $_POST['weight']; } else{ echo ''; } ?>">
                </div> 
                <div class="form-group col-md-6">
                  <label for="Service Name"> Price : </label>
                  <input type="text" class="form-control" name="price" value="<?php if(isset($_REQUEST['animal_id'])) { echo $price; } elseif(isset($_POST['price'])) { echo $_POST['price']; } else{ echo ''; } ?>">
                </div> 

                <div class="form-group col-md-6">
                    <label for="exampleInputFile">Image :- </label> <br />
                    
                    <?php if(isset($_REQUEST['animal_id'])){ ?> 
                        <input type="hidden" name="image1" value="<?= $image; ?>">
                        <img src="../<?= $image; ?>"  height="100" width="100px"/> <br /> <br />
                    <?php }?>

                    <input type="file" name="image" size="12"  data-toggle="tooltip"  data-placement="top" title="For Better Result Use Width and Height as Mention Above">   

                </div>  
            </div>


             <!-- /.box-body -->

             <div class="box-footer" align="center">
              <button type="submit" name="<?php if(isset($_REQUEST['animal_id'])) { echo 'animal_edit'; } elseif(isset($service_edit1) == 'animal_edit') { echo 'animal_id'; } else{ echo 'submit'; } ?>" value="submit" class="btn btn-primary ">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>

<?php include('footer.php'); ?>