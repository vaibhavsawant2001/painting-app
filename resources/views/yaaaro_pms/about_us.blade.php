@include('yaaaro_pms/head')
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      About Us
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">About Us</li>
    </ol>
  </section>
  <section class="content">
    <div class="box">
      <div class="box-body table-responsive no-padding">
        <table class="table table-hover" id="about_table">
          <thead>
            <tr>
              <th hidden>ID</th>
              <th>Image For About Us</th>
              <th>Image For Home Page</th>
              <th>Title</th>
              <th>Content</th>
              <th>Status</th>
              <th>Edit</th>
            </tr>
          </thead>
          <tbody>
            @foreach($aboutus as $aboutus)
          <tr>
            <td hidden>{{$aboutus->id}}</td>
            <td><img src="{{ url('public/' . $aboutus->image) }}" height="100" width="100"></td>
            <td><img src="{{ url('public/' . $aboutus->image) }}" height="100" width="100"></td>
            <td>{{$aboutus->title}}</td>
            <td>{{$aboutus->content}}</td>
            <td>
    <form method="POST" action="{{ route('aboutus.status', ['id' => $aboutus->id]) }}">
        @csrf
        @method('POST')

        @if($aboutus->status == 0)
            <button type="submit" class="label label-danger">Denied</button>
        @elseif($aboutus->status == 1)
            <button type="submit" class="label label-success">Approved</button>
        @endif
    </form>
</td>
            <td><a href="{{ route('aboutus.edit', $aboutus->id) }}" class="label label-warning">Edit</a></td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </section>
</div>

@include('yaaaro_pms/footer')
