<?php

// ob_start();
// error_reporting(E_ALL);
// error_reporting(-1);

include_once('header.php');

if(isset($_REQUEST['btype'])){
  $banner_type=strip_tags($_REQUEST['btype']);
}


// for edit
if(isset($_REQUEST['vbid'])){    
  $edit_id = strip_tags($_REQUEST['vbid']);
  $where = array( 'id' => $edit_id );
  if($others = $model->select('banner',$where)){
      foreach($others as $other){   
        $id = $other['id'];
        $title1 = $other['title1'];
        $image = $other['image']; 
      }
  }
}

if(isset($_POST['banner_edit'])){
  $banner_edit1 = 'banner_edit'; 
  $edit_id = $_POST['id'];

  $where_other = array( 
    'id' => $edit_id
  );

  $update_array = array(
      'title1' => strip_tags(htmlentities($_POST['title1'])),
      'image' => strip_tags(htmlentities($_POST['image'])),
      'date' => $todayDate
  );
  if($model->update("banner", $update_array, $where_other)){
    move_uploaded_file($temp_file, $file1);
    $model->url('banner.php?suc&vbid='.$edit_id);
  }else{
      $model->url('vbanner_add.php?msg&vbid='.$edit_id);
  }
}

// for insert
if(isset($_POST['submit'])){
  $vbid=strip_tags(htmlentities($_POST['vbid']));
  $insert_array = array(
      'title1' => strip_tags(htmlentities($_POST['title1'])),
      'image' => strip_tags(htmlentities($_POST['image'])),
      'banner_type' => 'video',
      'type' => addslashes(strip_tags(htmlentities($_POST['banner_type']))),
      'date' => $todayDate,
      'status' => '1'
  );
  if($model->insert("banner", $insert_array)){
        $model->url('banner.php?succ');
  }
  else
      $msg="faild";
}  

?>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Add <?= $banner_title1; ?> Video
    </h1>
    <ol class="breadcrumb">
      <li><a href="iadmin.php"><i class="fa fa-dashboard"></i> Home</a></li>
       <li><a href="banner.php"> Banner</a></li> 
       <li class="active">Add Video Banner</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 


    <div class="row">
      <!-- left column -->
      <div class="col-md-12">

        <div class="box box-primary">

          <div class="box-header with-border">

          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form enctype="multipart/form-data" method="POST">
            <div class="box-body">
              <?php if(isset($_REQUEST['vbid'])){ ?>
                  <input type="hidden" class="form-control" name="id" value="<?= $edit_id; ?>" >
              <?php } ?>
               <?php
              if(isset($_REQUEST['btype'])){
               echo ' <input type="hidden" class="form-control" name="banner_type" value="'.$banner_type.'" >';
              }  
            ?>   
              <div class="row">
                <div class="col-md-12 form-group">
                  <label for="Offer Type"> title1 : </label>
                  <input type="text" class="form-control" name="title1" value="<?php if(isset($_REQUEST['vbid'])) { echo $title1; } elseif(isset($_POST['title1'])) { echo $_POST['title1']; } else{ echo ''; } ?>"  >
                </div>               
              </div>

              <div class="row">
                <div class="col-md-12 form-group">
                  <label for="Offer Type"> Youtube Video Link : </label>
                  <input type="text" class="form-control" name="image" value="<?php if(isset($_REQUEST['vbid'])) { echo $image; } elseif(isset($_POST['image'])) { echo $_POST['image']; } else{ echo ''; } ?>" placeholder="eg.https://www.youtube.com/embed/tgbNymZ7vqY" >
                </div>               
              </div>
             <!-- /.box-body -->

             <div class="box-footer" align="center">
              <button type="submit" name="<?php if(isset($_REQUEST['vbid'])) { echo 'banner_edit'; } elseif(isset($banner_edit1) == 'banner_edit') { echo 'vbid'; } else{ echo 'submit'; } ?>" value="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>

<?php include('footer.php'); ?>