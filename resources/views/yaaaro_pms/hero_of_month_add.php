<?php

// ob_start();
// error_reporting(E_ALL);
// error_reporting(-1);

include_once('header.php');

// for edit
if(isset($_REQUEST['home_id'])){    
  $edit_id = strip_tags($_REQUEST['home_id']);
  $where = array( 'id' => $edit_id );
  if($others = $model->select('month_hero',$where)){
      foreach($others as $other){   
          $id = $other['id'];
          $v_id = $other['v_id'];
          $content = $other['content'];
          $date = $other['date'];
      }
  }
}
if(isset($_POST['home_edit'])){
  $home_edit1 = 'home_edit'; 
  $edit_id = $_POST['id'];
  $content = addslashes(strip_tags(htmlentities($_POST['content'])));

  $where_other = array( 
    'id' => $edit_id
  );
    //  $hero_month = date('M-Y', strtotime($date));
//   echo "<br>";
    // $hero_month = date('M-Y');
//   exit();
  $update_array = array(
    'content' => $content
  );
  if($model->update("month_hero", $update_array, $where_other)){
  
      $model->url('home.php?succ');
  }else{
        $model->url('home.php?fail');
  }
}


//for update hero
if(isset($_POST['submit'])){
    $v_id = $_POST['name'];
    $content = $_POST['content'];

    $insert_array = array(
        'v_id' => $v_id,
        'content' => $content,
        'date' => $todayDate,
        'status' => '1'
    );
    if($datas = $model->insert('month_hero',$insert_array)){
        $model->url('home.php?succ');
    }

}

?>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="top:-50px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Add Hero Of The Month
        </h1>
        <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="home.php">Hero Of The Month</a></li>
            <li class="active">Add Hero Of The Month</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">

                <div class="box box-primary">

                    <div class="box-header with-border">
                        <?php if (isset($_REQUEST['fail'])) {
                        echo '<div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        Something Went Wrong....
                        </div>';
                        } ?>

                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <form enctype="multipart/form-data" method="POST">
                        <div class="box-body">
                            <div class="row">
                                <?php if(isset($_REQUEST['home_id'])){ ?>
                                <input type="hidden" class="form-control" name="id" value="<?= $edit_id; ?>">
                                <?php } ?>
                                <div class="col-md-12 form-group">
                                    <label for="username"> Hero Of The Month </label>
                                    <select name="name" class="form-control" <?php if(isset($_REQUEST['home_id'])){ echo 'disabled'; }?>>
                                        <option value="">Select Hero Of The Month</option>
                                        <?php 
                                            $where = array(
                                                'user_type' => 'Volunteer',
                                                'status' => '1'
                                            );
                                            if($datas = $model->select('volunteer', $where)){
                                                foreach($datas as $data){
                                        ?>
                                        <option value="<?= $data['id'];?>" <?php if(isset($_REQUEST['home_id'])){ if($v_id === $data['id']){
                                            echo "selected";}elseif(isset($_POST['name'])) { echo 'selected'; } else{ echo ''; }} ?>><?= $data['name'];?></option>
                                        <?php }} ?>
                                    </select>
                                </div>
                                <div class="col-md-12 form-group">
                                    <label for="Contant"> Short Description : </label>
                                    <textarea class="form-control" rows="4" name="content"><?php if(isset($_REQUEST['home_id'])) { echo $content; } elseif(isset($_POST['content'])) { echo $_POST['content']; } else{ echo ''; } ?></textarea>
                                </div>
                                <!-- /.box-body -->

                                <div class="box-footer" align="center">
                                        <button type="submit"
                                        name="<?php if(isset($_REQUEST['home_id'])) { echo 'home_edit'; } elseif(isset($home_edit1) == 'home_edit') { echo 'home_id'; } else{ echo 'submit'; } ?>"
                                        value="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

<?php include_once('footer.php'); ?>