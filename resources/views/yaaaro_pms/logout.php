
<?php
    session_start();
    
    if(isset($_REQUEST['sign_out'])){
        session_destroy();
        session_abort();
        unset($_SESSION['user_crm']);
        unset($_SESSION['pass_crm']);
        header('location: index.php');
    }
?>