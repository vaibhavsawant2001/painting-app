<?php

// ob_start();
// error_reporting(E_ALL);
// error_reporting(-1);

include_once('header.php');

// for edit
if(isset($_REQUEST['cat_id'])){    
  $edit_id = strip_tags($_REQUEST['cat_id']);
  $where = array( 'id' => $edit_id );
  if($others = $model->select('program_category',$where)){
      foreach($others as $other){   
          $id = $other['id'];
          $image = $other['image'];
          $file = $other['file'];
          $title = $other['title'];
         $content = $other['content'];
          // $metatag = $other['metatag'];
      }
  }
}

if(isset($_POST['cat_edit'])){
  $cat_edit1 = 'cat_edit'; 
  $edit_id = $_POST['id'];

  $image1 = strip_tags($_POST['image1']);
  $image2 = $_FILES['image']['name'];
    if (empty($image2)) {
      $file = $image1;
    }
    else{  
      // for image replace
      $where = array( 'id' => $edit_id );
      $stmt_del = $model->select('program_category',$where);
      foreach($stmt_del as $delete_image){
        $deleteimage = '../'.$delete_image['image'];
        unlink($deleteimage);
      }

     $target_dir1 = '../uploads/program/';
    $target_dir = 'uploads/program/';
    $newfilename = date('dmYHis').str_replace(" ", "", basename($image2));
    $file1 = $target_dir1 . basename($newfilename);
    $file = $target_dir . basename($newfilename);
    $uploadOk = 1;
    $temp_file = $_FILES["image"]["tmp_name"];
  }


 $fileo = strip_tags($_POST['file1']);
  $filen = $_FILES['file']['name'];
    if (empty($filen)) {
      $file2 = $fileo;
    }
    else{  
      // for image replace
      $where = array( 'id' => $edit_id );
      $stmt_del = $model->select('program_category',$where);
      foreach($stmt_del as $delete_image){
        $deleteimage = '../'.$delete_image['image'];
        unlink($deleteimage);
      }

    $target_dir3 = '../uploads/program/';
    $target_dir2 = 'uploads/program/';
    $catfilename = date('dmYHis').str_replace(" ", "", basename($filen));
    $file3 = $target_dir3 . basename($catfilename);
    $file2 = $target_dir2 . basename($catfilename);
    $uploadOk = 1;
    $temp_filen = $_FILES["file"]["tmp_name"];
  }
  $where_other = array( 
    'id' => $edit_id
  );


    // $image_title = addslashes(strip_tags(htmlentities($_POST['image_title'])));
    $title = addslashes(strip_tags(htmlentities($_POST['title'])));
    // $subject = addslashes(strip_tags(htmlentities($_POST['subject'])));
    $content = addslashes($_POST['content']);
    $date = $todayDate;

  $update_array = array(
    'image' => $file,
    // 'image_title' => $image_title,
    'title' => $title,
    // 'subject' => $subject,
    // 'content' => $content,
    'date' => $todayDate,
  );
  if($model->update("program_category", $update_array, $where_other)){
    move_uploaded_file($temp_file, $file1);
    move_uploaded_file($temp_filen, $file2);
    $model->url('prog_cat.php?succ=Update');
  }else{
      $model->url('prog_cat_add.php?blog_id='.$edit_id);
  }
}
// for insert
if(isset($_POST['submit'])){

    $about_file = $_FILES['image']['name'];
    if(!empty($about_file)){
      $target_dir1 = '../uploads/program/';
      $target_dir = 'uploads/program/';
      $newfilename = date('dmYHis').str_replace(" ", "", basename($about_file));
      $file1 = $target_dir1 . basename($newfilename);
      $file = $target_dir . basename($newfilename);
      $uploadOk = 1;
      $temp_file = $_FILES["image"]["tmp_name"];
    }else{
      $file1 = '';
      $file ='';
    }


      $cat_file = $_FILES['file']['name'];
    if(!empty($cat_file)){
      $target_dir3 = '../uploads/program/';
      $target_dir2 = 'uploads/program/';
      $catfilename = date('dmYHis').str_replace(" ", "", basename($cat_file));
      $file3 = $target_dir3 . basename($catfilename);
      $file2 = $target_dir2 . basename($catfilename);
      $uploadOk = 1;
      $temp_catfile = $_FILES["file"]["tmp_name"];
    }else{
      $file3 = '';
      $file2 ='';
    }



  
    $title = addslashes(strip_tags(htmlentities($_POST['title'])));
    // $subject = addslashes(strip_tags(htmlentities($_POST['subject'])));
    $content = addslashes($_POST['content']);
    $date = $todayDate;
    $status = '1';

  $insert_array = array(
      'image' => $file,
      'file' => $file2,
      'title' => $title,
      // 'subject' => $subject,
      'content' => $content,
      'date' => $todayDate,
      'status' => '1'
  );


  if($model->insert("program_category",$insert_array)){
        move_uploaded_file($temp_file, $file1);
        move_uploaded_file($temp_catfile, $file3);
        $model->url('prog_cat.php?succ');
  }
  else
      $msg="faild";
}   

?>

<script type="text/javascript" src="ckeditor/ckeditor.js">
</script>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add Program Category
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="prog_cat.php">Program Category</a></li>
      <li class="active">Add Program Category</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">

        <div class="box box-primary">

          <div class="box-header with-border">
            <?php if (isset($_REQUEST['fail'])) {
              echo '<div class="alert alert-danger alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              Something Went Wrong....
              </div>';
            } ?>

          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form enctype="multipart/form-data" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">
            <div class="box-body">
               <?php if(isset($_REQUEST['cat_id'])){ ?>
                    <input type="hidden" class="form-control" name="id" value="<?= $edit_id; ?>" >
                 <?php } ?>
              <!-- <div class="form-group">
                <label for="Mata-Tags"> Mata-Tags : </label>
                <input type="text" class="form-control" name="metatag" value="<?php if(isset($_REQUEST['cat_id'])) { echo $metatag; } elseif(isset($_POST['metatag'])) { echo $_POST['metatag']; } else{ echo ''; } ?>">
              </div>  -->
              
                <div class="form-group">
                  <label for="Name">  Name : </label>
                  <input type="text" class="form-control" name="title" required value="<?php if(isset($_REQUEST['cat_id'])) { echo $title; } elseif(isset($_POST['title'])) { echo $_POST['title']; } else{ echo ''; } ?>">
                </div> 

            <!--   <div class="form-group">
                <label for="Subject"> Subject : </label>
                <textarea class="form-control" rows="4" name="subject"><?php if(isset($_REQUEST['blog_id'])) { echo $subject; } elseif(isset($_POST['subject'])) { echo $_POST['subject']; } else{ echo ''; } ?></textarea>
              </div>

              <div class="form-group">
                <label for="Contant"> Content : </label>
                <textarea class="form-control" rows="4" name="content" id="editor"><?php if(isset($_REQUEST['blog_id'])) { echo $content; } elseif(isset($_POST['content'])) { echo $_POST['content']; } else{ echo ''; } ?></textarea>
             </div> -->

             <div class="form-group">
                <label for="Contant"> Content : </label>
                <textarea class="form-control" rows="4" name="content" id="editor"><?php if(isset($_REQUEST['cat_id'])) { echo $content; } elseif(isset($_POST['content'])) { echo $_POST['content']; } else{ echo ''; } ?></textarea>
             </div>
<!-- 
              <div class="form-group">
                <label for="Image Title"> Image Title : </label>
                <input type="text" class="form-control" name="image_title" required value="<?php if(isset($_REQUEST['cat_id'])) { echo $image_title; } elseif(isset($_POST['image_title'])) { echo $_POST['image_title']; } else{ echo ''; } ?>" >
              </div> -->

              <div class="form-group">
                <label for="exampleInputFile">Image :- </label> <br />
                  
               <?php if(isset($_REQUEST['cat_id'])){ ?> 
                  <input type="hidden" name="image1" value="<?= $image; ?>">
                   <img src="../<?= $image; ?>"  height="100" width="100px"/> <br /> <br />
               <?php }?>

                <input type="file" name="image" size="12"  data-toggle="tooltip"  data-placement="top" title="For Better Result Use Width and Height as Mention Above">  
              </div>  





             <!--  <div class="form-group">
                <label for="exampleInputFile">File :- </label> <br />
                 <?php if(isset($_REQUEST['cat_id'])){ 
                  echo '<input type="hidden" name="file1" value="'.$file .'">';
                  echo $file;
                 }?>
                <input type="file" name="file" size="12"  data-toggle="tooltip"  data-placement="top" title="For Better Result Use Width and Height as Mention Above">  
              </div> -->
            </div>


             <!-- /.box-body -->

             <div class="box-footer" align="center">
              <button type="submit" name="<?php if(isset($_REQUEST['cat_id'])) { echo 'cat_edit'; } elseif(isset($cat_edit1) == 'cat_edit') { echo 'cat_id'; } else{ echo 'submit'; } ?>" value="submit" class="btn btn-primary ">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>

<?php include('footer.php'); ?>