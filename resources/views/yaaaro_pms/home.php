<?php
include_once('header.php');
    
//client Change Status
if ( isset($_REQUEST['home_id']) && isset($_REQUEST['home_status']) ) {
    $where_array = array( 'id' => strip_tags($_REQUEST['home_id']) );
    $update_array = array(  'status' => strip_tags($_REQUEST['home_status']) );    

    if($model->update("month_hero", $update_array, $where_array)){
        $succ = 'Status Update';
    }
}
//for delete
if (isset($_REQUEST['home_del_id'])) {
  $where_array = array( 'id' => strip_tags($_REQUEST['home_del_id'])  );
 
  if($model->delete("month_hero", $where_array)){
      $succ = 'Delete';
  }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Home
        </h1>
        <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Home</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- End Main Content -->
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header text-center">
                         <a href="hero_of_month_add.php" class="btn btn-primary">Add Hero Of The Month</a>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table id="client_master" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <!-- <th> ID </th> -->
                                    <th> Image</th>
                                    <th> Name</th>
                                    <th> Short Discription </th>
                                    <th> Month </th>
                                    <th> Status</th>
                                    <th> </th>
                                    <th> </th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php 
                                    if($datas = $model->singleselect('month_hero')){  
                                        foreach($datas as $data){ 
                                            $id = $data['id'];
                                            $v_id = $data['v_id'];
                                            $content = $data['content'];
                                            $date = $data['date'];  
                                            $status = $data['status'];  
                                                
                                      $where_h = array(
                                        'id' => $v_id
                                      );
                                      if($datas = $model->select("volunteer" ,$where_h)){ 
                                        foreach( $datas as $row){
                                          $hero_image = $row['image'];
                                          $hero_name = $row['name'];
                                        }
                                      }
                                ?>
                                <tr>

                                    <td>
                                      <?php    
                                        if(!empty($hero_image)){
                                      ?>
                                        <img src="../<?= $hero_image; ?>" height="100" width="100px" />
                                      <?php } ?>
                                    </td>
                                    <td> <?= $hero_name; ?> </td>
                                  

                                    <td> <?= $content; ?> </td>
                                    <td> <?= date('M-Y', strtotime($date)); ?> </td>
                                    <td class="action-btn">
                                        <?php if($status == '0') { ?>

                                        <a href="home.php?home_status=1&home_id=<?= $id; ?> "
                                            class="label label-danger"> Denied </a>

                                        <?php } else { ?>

                                        <a href="home.php?home_status=0&home_id=<?= $id; ?>"
                                            class="label label-success">Approved</a>
                                        <?php } ?>
                                    </td>
                                    <td>
                                        <a href="hero_of_month_add.php?home_id=<?php echo $id; ?>">
                                            <span class="label label-info"> Edit </span>
                                        </a>
                                    </td>
                                    <td> <a href="home.php?home_del_id=<?= $id; ?>" onclick="return confirm('Are you sure you want to Remove?');"> <span class="label label-danger"> Delete </span> </a> </td> 
                                </tr>
                                <?php }} ?>
                            </tbody>

                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>

            <!-- ./col -->
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
</div>
<?php include('footer.php'); ?>