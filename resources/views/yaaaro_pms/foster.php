<?php
include_once('header.php');
    

// Change Status
if ( isset($_REQUEST['volunteer_id']) && isset($_REQUEST['volunteer_status']) ) {
  $where_array = array( 'id' => strip_tags($_REQUEST['volunteer_id']) );
  $update_array = array(  'status' => strip_tags($_REQUEST['volunteer_status']) );    

  if($model->update("volunteer", $update_array, $where_array)){
    
      // for active / deactive volunteer
      $where_array=array('v_id' => strip_tags($_REQUEST['volunteer_id']));
      if($model->update("user", $update_array, $where_array)){
          $succ = 'Status Update';
      }
      $succ = 'Status Update';
  }
}
  // delete
if (isset($_REQUEST['volunteer_del_id'])) {
  $where_array = array( 'id' => strip_tags($_REQUEST['volunteer_del_id']) );
  $stmt_del = $model->select('volunteer',$where_array);
  foreach($stmt_del as $delete_image){
    if(!empty($delete_image['image'])){
      $deleteimage = '../'.$delete_image['image'];
      unlink($deleteimage);
    }
  }
  // for user delete
  $where = array(
    'v_id' => strip_tags($_REQUEST['volunteer_del_id'])
  );
  $model->delete("user", $where);
  if($model->delete("volunteer", $where_array)){
      $succ = 'Delete';
  }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
        Foster
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Foster</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center"> 
                <!-- <a href="volunteer_add.php" class="btn btn-primary">Add foster</a> -->
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
                <thead>
              <tr>
                    <th> Image </th>
                    <th> Name </th>
                    <th> Email </th>
                    <th> Contact No. </th>
                    <th> City </th>
                    <th> Zip/Postal_code</th>      
                    <th>  </th>
                    <th> Status </th>
                    <th> </th>
              </tr>
              </thead>
              <tbody>
             <?php  
              $where = array(
                'user_type' => 'Foster'
              );
                if($datas = $model->select("volunteer" ,$where)){ 
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $image = $data['image'];
                        $name = $data['name'];
                        $email = $data['email'];
                        $phone = $data['phone'];
                        $city = $data['city'];
                        $zip_code = $data['zip_code'];
                        $status = $data['status'];                    
             ?>
                <tr>
                  <td> <img src="../<?= $image; ?>" height="100px" width="120px"> </td>  
                  <td> <?= $name; ?> </td> 
                  <td> <?= $email; ?> </td> 
                  <td> <?= $phone; ?> </td> 
                  <td> <?= $city; ?> </td>  
                  <td> <?= $zip_code; ?> </td>  
                  <td>  <a  href="foster_detail.php?f_id=<?= $id; ?> " class="label label-warning"> More Info. </a>   </td>  
                       
                                     
                  <td>     
                        <?php if($status == '0') { ?>

                        <a  href="foster.php?volunteer_status=1&volunteer_id=<?= $id; ?> " class="label label-danger"> Denied </a>  

                        <?php } else { ?>

                        <a  href="foster.php?volunteer_status=0&volunteer_id=<?= $id; ?>" class="label label-success">Approved</a>  
                        <?php } ?>
                  </td>
             
                  <!-- <td>
                      <a href="volunteer_add.php?volunteer_id=<?php echo $id; ?>"> <span class="label label-warning"> Edit </span> </a> 
                  </td> -->
                  <td>
                      <a href="foster.php?volunteer_del_id=<?php echo $id; ?>" onclick="return confirm('Are you sure you want to Remove?');"> <span class="label label-danger"> Delete </span> </a> 
                  </td> 
               </tr>
            <?php } } ?>
            </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  
     

        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>