@include('yaaaro_pms/head')
<script src="https://cdn.ckeditor.com/ckeditor5/46.0.1/classic/ckeditor.js"></script>
<div class="content-wrapper">
  <!-- Content Header -->
  <section class="content-header">
    <h1>About Us Edit</h1>
  </section>
  <section class="content">
    <div class="box">
      <form action="{{ route('aboutus.update', [$aboutus->id]) }}" method="POST" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="box-body">
          <div class="form-group">
            <label for="title">Title:</label>
            <input type="hidden" name="id" value="">
            <input type="text" name="title" class="form-control" value="{{$aboutus->title}}" placeholder="Page Title">
          </div>
          <div class="form-group">
            <label for="content">Top Content <i style="color:red">(60 Words Limit)</i></label>
            <textarea name="content" class="form-control" rows="4" id="editor">{!! $aboutus->content !!}</textarea>

          </div>
          <div class="form-group">
            <label for="Contant">Middle Content : </label>
            <textarea class="form-control" rows="4" name="middle_content" id="editor1">{{$aboutus->middle_content}}</textarea>
          </div>
          <div class="form-group">
            <label for="image_h">About Us Image For HomePage (1920 x 642):</label>
            <input type="hidden" name="image" value="">
            <img src="{{ url('public/' . $aboutus->image) }}" height="100" width="100" />
            <br /><br />
            <input type="file" name="image" size="12" value="{{$aboutus->image}}" data-toggle="tooltip" data-placement="top" title="For Better Result Use Width and Height as Mentioned Above">
          </div>
          <div class="box-body">
            <!-- <input type="hidden" class="form-control" name="" value="8"> -->
            <div class="row">
              <div class="form-group col-md-4">
                <label for=" Name"> Title : </label>
                <input type="text" class="form-control" name="header_title" readonly value="ABOUT US">
              </div>
              <div class="form-group col-md-4">
                <label for=" Name"> Page Title : </label>
                <input type="text" class="form-control" name="page_title" required value="{{$aboutus->page_title}}">
              </div>
              <div class="form-group col-md-4">
                <label for=" Name"> URL Extension : </label>
                <input type="text" class="form-control" name="url_extension" required value="{{$aboutus->url_extension}}">
              </div>
              <div class="form-group col-md-12">
                <h4 style="font-weight:bold;">Only For SEO Experts</h4><br>
                <label for="Contant"> Content : [Add meta tag ]</label>
                <textarea class="form-control" rows="4" name="metatag">{{$aboutus->metatag}}</textarea>
              </div>
              <div class="box-footer" align="center">
                <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
              </div>
      </form>
    </div>
  </section>
</div>
<script src="{{url('css/ckeditor/ckeditor.js')}}"></script>
<script>
CKEDITOR.replace('editor1', {
    allowedContent: true
});
</script>
<script>
  ClassicEditor
    .create(document.querySelector('textarea'))
    .catch(error => {
      console.error(error);
    });
</script>
@include('yaaaro_pms/footer')