<?php

// ob_start();
// error_reporting(E_ALL);
// error_reporting(-1);

include_once('header.php');

// for edit
if(isset($_REQUEST['home_id'])){    
  $edit_id = strip_tags($_REQUEST['home_id']);
  $where = array( 'id' => $edit_id );
  if($others = $model->select('home',$where)){
      foreach($others as $other){   
          $id = $other['id'];
          $title = $other['title'];
          $heading = $other['heading'];
          $content = $other['content'];
      }
  }
}
if(isset($_POST['home_edit'])){
  $home_edit1 = 'home_edit'; 
  $edit_id = $_POST['id'];
  $url = $_POST['url'];
  $title = addslashes(strip_tags(htmlentities($_POST['title'])));
  $heading = addslashes(strip_tags(htmlentities($_POST['heading'])));
  $content = $_POST['content'];
  $where_other = array( 
    'id' => $edit_id
  );
  $update_array = array(
          'title' => $title,
          'heading' => $heading,
          'content' => $content
  );
  if($model->update("home", $update_array, $where_other)){
    if(!empty($url)){
      $model->url($url.'.php?succ=Update&home_id='.$edit_id);
    }else{
      $model->url('home.php?succ=Update&home_id='.$edit_id);
    }
  }else{
      $model->url('home_add.php?fail&home_id='.$edit_id);
  }
}

?>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="top:-50px;">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add Home
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li ><a href="home.php">Home</a></li>
      <li class="active">Add Home</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">

        <div class="box box-primary">

          <div class="box-header with-border">
            <?php if (isset($_REQUEST['fail'])) {
              echo '<div class="alert alert-danger alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              Something Went Wrong....
              </div>';
            } ?>

          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form enctype="multipart/form-data" method="POST">
            <div class="box-body">
              <div class="row">
                 <?php if(isset($_REQUEST['home_id'])){ ?>
                    <input type="hidden" class="form-control" name="id" value="<?= $edit_id; ?>" >
                 <?php } ?>
                 <?php if(isset($_REQUEST['url'])){ ?>
                    <input type="hidden" class="form-control" name="url" value="<?= $_REQUEST['url']; ?>" >
                 <?php } ?>
                <div class="col-md-12 form-group">
                  <label for="username">Home Title </label>
                  <input type="text" class="form-control" name="title" value="<?php if(isset($_REQUEST['home_id'])) { echo $title; } elseif(isset($_POST['title'])) { echo $_POST['title']; } else{ echo ''; } ?>">
                </div> 
                <!-- <?php 
                  if($_REQUEST['home_id'] === '3' ){
                ?>
                <div class="col-md-12 form-group">
                  <label for="username">Short Discription </label>
                  <input type="text" class="form-control" name="heading" value="<?php if(isset($_REQUEST['home_id'])) { echo $heading; } elseif(isset($_POST['heading'])) { echo $_POST['heading']; } else{ echo ''; } ?>">
                </div> 
              
            <?php } ?> -->
              <div class="col-md-12 form-group">
                <label for="Contant"> Content : </label>
                <textarea class="form-control" rows="4" name="content" id="editor"><?php if(isset($_REQUEST['home_id'])) { echo $content; } elseif(isset($_POST['content'])) { echo $_POST['content']; } else{ echo ''; } ?></textarea>
                <!-- <script>CKEDITOR.replace( 'content' );</script> -->
              </div>
             <!-- /.box-body -->

             <div class="box-footer" align="center">
              <button type="submit" name="<?php if(isset($_REQUEST['home_id'])) { echo 'home_edit'; } elseif(isset($home_edit1) == 'home_edit') { echo 'home_id'; } else{ echo 'submit'; } ?>" value="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>

<?php include_once('footer.php'); ?>