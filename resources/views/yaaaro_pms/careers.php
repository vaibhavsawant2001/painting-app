<?php
include_once('header.php');

    //client delete
if (isset($_REQUEST['testi_del_id'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['testi_del_id'])  );
    if($model->delete("contact", $where_array)){
        $succ = 'Delete';
    }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    Careers
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Careers</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
                <thead>
              <tr>

                <th> Date </th>
                <th> Name</th>
                <th> Email</th>
                <th> Phone </th> 
                <th> Address </th>   
                <th> Qualification </th>   
                <th> p_area </th>   
                <th> Content </th>   
                <th>  </th>   
              </tr>
              </thead>
                <tbody>
             <?php  
              $where = array(
                  'type' => 'careers'
              );
              if($datas = $model->select("contact", $where)){  
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $name = $data['name'];
                        $email = $data['email'];
                        $phone = $data['phone'];
                        $address = $data['address'];
                        $qualification = $data['qualification'];                    
                        $p_area = $data['p_area'];                    
                        $content = $data['content'];                    
                        $date = $data['date'];                    
             ?>
                <tr>
                  <td> <?= $date; ?> </td>    
                  <td> <?= $name; ?> </td>  
                  <td> <?= $email; ?> </td>  
                  <td> <?= $phone; ?> </td>  
                  <td> <?= $address; ?> </td>          
                  <td> <?= $qualification; ?> </td>          
                  <td> <?= $p_area; ?> </td>          
                  <td> <?= $content; ?> </td>              
                  <td>
                    <a href="careers.php?testi_del_id=<?php echo $id; ?>"> <span class="label label-danger"> Delete </span> </a>
                  </td> 
               </tr>
            <?php } } ?>
            </tbody>

              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  

      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>