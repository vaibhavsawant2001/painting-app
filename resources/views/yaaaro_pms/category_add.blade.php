@include('yaaaro_pms/head')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add Category
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="tag.php"> Category</a></li>
      <li class="active">Add Category</li>
    </ol>
  </section>
  <section class="content"> 
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
          </div>
          <form enctype="multipart/form-data" action="{{route('api.gallerycategory')}}" method="POST">
            <div class="box-body">
             <div class="row">
                <div class="col-md-12 form-group">
                  <label for="Offer Type"> Image Category : </label>
                  <input type="text" class="form-control" name="cat_name" value=""  >
                </div>
              </div>
             <div class="box-footer" align="center">
              <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
<script>
  document.getElementById('gallerycategoryForm').addEventListener('submit', function(e) {
    e.preventDefault();

    const formData = new FormData(e.target);

    axios.post('/api/gallerycategory', formData)
      .then(response => {
        console.log(response.data.token);
        window.location.href = "{{url('yaaaro_pms/category')}}";
      })
      .catch(error => {
        console.error(error.response.data.error);
      });
  });
</script>
@include('yaaaro_pms/footer')
