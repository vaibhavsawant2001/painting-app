<?php

// ob_start();
// error_reporting(E_ALL);
// error_reporting(-1);

include_once('header.php');

// for edit
if(isset($_REQUEST['storie_id'])){    
  $edit_id = strip_tags($_REQUEST['storie_id']);
  $where = array( 'id' => $edit_id );
  if($others = $model->select('stories',$where)){
      foreach($others as $other){   
          $id = $other['id'];
          $image = $other['image'];
          $image_title = $other['image_title'];
          $title = $other['title'];
          $subject = $other['subject'];
          $content = $other['content'];
          // $metatag = $other['metatag'];
      }
  }
}

if(isset($_POST['storie_edit'])){
  $storie_edit1 = 'storie_edit'; 
  $edit_id = $_POST['id'];

  $image1 = strip_tags($_POST['image1']);
  $image2 = $_FILES['image']['name'];
    if (empty($image2)) {
      $file = $image1;
    }
    else{  
      // for image replace
      $where = array( 'id' => $edit_id );
      $stmt_del = $model->select('stories',$where);
      foreach($stmt_del as $delete_image){
        $deleteimage = '../'.$delete_image['image'];
        unlink($deleteimage);
      }

    $about_file = $_FILES['image']['name'];
    $target_dir1 = '../uploads/stories/';
    $target_dir = 'uploads/stories/';
    $newfilename = date('dmYHis').str_replace(" ", "", basename($about_file));
    $file1 = $target_dir1 . basename($newfilename);
    $file = $target_dir . basename($newfilename);
    $uploadOk = 1;
    $temp_file = $_FILES["image"]["tmp_name"];
  }

  $where_other = array( 
    'id' => $edit_id
  );

        $image = $file;
        $image_title = addslashes(strip_tags(htmlentities($_POST['image_title'])));
        $title = addslashes(strip_tags(htmlentities($_POST['title'])));
        $subject = addslashes(strip_tags(htmlentities($_POST['subject'])));
        $content = $_POST['content'];
        $date = $todayDate;

      $update_array = array(
              'image' => $file,
              'image_title' => strip_tags(htmlentities($_POST['image_title'])),
              'title' => strip_tags(htmlentities($_POST['title'])),
              'subject' => strip_tags(htmlentities($_POST['subject'])),
              'content' => $_POST['content'],
              // 'metatag' => $_POST['metatag'],
              'date' => $todayDate
      );
      if($model->update("stories", $update_array, $where_other)){
        move_uploaded_file($temp_file, $file1);
        $model->url('stories.php?succ=Update');
      }else{
          $model->url('stories_add.php?storie_id='.$edit_id);
      }

   }
// for insert
if(isset($_POST['submit'])){

        $about_file = $_FILES['image']['name'];
        if(!empty($about_file)){
          $target_dir1 = '../uploads/stories/';
          $target_dir = 'uploads/stories/';
          $newfilename = date('dmYHis').str_replace(" ", "", basename($about_file));
          $file1 = $target_dir1 . basename($newfilename);
          $file = $target_dir . basename($newfilename);
          $uploadOk = 1;
          $temp_file = $_FILES["image"]["tmp_name"];
        }else{
          $file1 = '';
          $file ='';
        }
      
        $image = $file;
        $image_title = addslashes(strip_tags(htmlentities($_POST['image_title'])));
        $title = addslashes(strip_tags(htmlentities($_POST['title'])));
        $subject = addslashes(strip_tags(htmlentities($_POST['subject'])));
        $content = $_POST['content'];
        $date = $todayDate;
        $status = '1';

      $insert_array = array(
          'image' => $file,
          'image_title' => strip_tags(htmlentities($_POST['image_title'])),
          'title' => strip_tags(htmlentities($_POST['title'])),
          'subject' => strip_tags(htmlentities($_POST['subject'])),
          'content' => $_POST['content'],
          // 'metatag' => $_POST['metatag'],
          'date' => $todayDate,
          'status' => '1'
      );
      if($model->insert("stories",$insert_array)){
            move_uploaded_file($temp_file, $file1);
            $model->url('stories.php?succ');
      }
      else
          $msg="faild";

    }  

?>

<script type="text/javascript" src="ckeditor/ckeditor.js">
</script>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add Stories
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="stories.php">Stories</a></li>
      <li class="active"> Add Stories</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">

        <div class="box box-primary">

          <div class="box-header with-border">
            <?php if (isset($_REQUEST['fail'])) {
              echo '<div class="alert alert-danger alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              Something Went Wrong....
              </div>';
            } ?>

          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form enctype="multipart/form-data" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">
            <div class="box-body">
               <?php if(isset($_REQUEST['storie_id'])){ ?>
                    <input type="hidden" class="form-control" name="id" value="<?= $edit_id; ?>" >
                 <?php } ?>
              <!-- <div class="form-group">
                <label for="Mata-Tags"> Mata-Tags : </label>
                <input type="text" class="form-control" name="metatag" value="<?php if(isset($_REQUEST['storie_id'])) { echo $metatag; } elseif(isset($_POST['metatag'])) { echo $_POST['metatag']; } else{ echo ''; } ?>">
              </div>  -->
              
                <div class="form-group">
                  <label for="storie Name"> storie Name : </label>
                  <input type="text" class="form-control" name="title" value="<?php if(isset($_REQUEST['storie_id'])) { echo $title; } elseif(isset($_POST['title'])) { echo $_POST['title']; } else{ echo ''; } ?>">
                </div> 

              <div class="form-group">
                <label for="Subject"> Subject : </label>
                <textarea class="form-control" rows="4" name="subject"><?php if(isset($_REQUEST['storie_id'])) { echo $subject; } elseif(isset($_POST['subject'])) { echo $_POST['subject']; } else{ echo ''; } ?></textarea>
              </div>

              <div class="form-group">
                <label for="Contant"> Content : </label>
                <textarea class="form-control" rows="4" name="content" id="editor"><?php if(isset($_REQUEST['storie_id'])) { echo $content; } elseif(isset($_POST['content'])) { echo $_POST['content']; } else{ echo ''; } ?></textarea>
                <!-- <script>CKEDITOR.replace( 'content' );</script> -->
              </div>

              <div class="form-group">
                <label for="Image Title"> Image Title : </label>
                <input type="text" class="form-control" name="image_title" value="<?php if(isset($_REQUEST['storie_id'])) { echo $image_title; } elseif(isset($_POST['image_title'])) { echo $_POST['image_title']; } else{ echo ''; } ?>" >
              </div>

              <div class="form-group">
                <label for="exampleInputFile">Image :- </label> <br />
                  
               <?php if(isset($_REQUEST['storie_id'])){ ?> 
                  <input type="hidden" name="image1" value="<?= $image; ?>">
                   <img src="../<?= $image; ?>"  height="100" width="100px"/> <br /> <br />
               <?php }?>

                <input type="file" name="image" size="12"  data-toggle="tooltip"  data-placement="top" title="For Better Result Use Width and Height as Mention Above">  
              </div>  
            </div>


             <!-- /.box-body -->

             <div class="box-footer" align="center">
              <button type="submit" name="<?php if(isset($_REQUEST['storie_id'])) { echo 'storie_edit'; } elseif(isset($storie_edit1) == 'storie_edit') { echo 'storie_id'; } else{ echo 'submit'; } ?>" value="submit" class="btn btn-primary ">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>

<?php include('footer.php'); ?>