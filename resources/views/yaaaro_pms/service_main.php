<?php
include_once('header.php');
    
// Change Status
if ( isset($_REQUEST['service_id']) && isset($_REQUEST['service_status']) ) {
    $where_array = array( 'id' => strip_tags($_REQUEST['service_id']) );
    $update_array = array(  'status' => strip_tags($_REQUEST['service_status']) );    

    if($model->update("services", $update_array, $where_array)){
        $succ = 'Status Update';
    }
}
    // delete
elseif (isset($_REQUEST['service_del_id'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['service_del_id'])  );
    $stmt_del = $model->select('services',$where_array);
    foreach($stmt_del as $delete_image){
      if(!empty($delete_image['image'])){
            $deleteimage = '../'.$delete_image['image'];
            unlink($deleteimage);
      }
    }
    //for sub cate
    $where_array2 = array( 'cat_id' => strip_tags($_REQUEST['service_del_id'])  );
    $stmt_del2 = $model->select('services',$where_array2);
    foreach($stmt_del2 as $delete_image2){
      if(!empty($delete_image2['image'])){
            $deleteimage2 = '../'.$delete_image2['image'];
            unlink($deleteimage2);
      }
    }
    $model->delete("services", $where_array2);

    if($model->delete("services", $where_array)){
        $succ = 'Delete';
    }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    Programs 
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Programs</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center"> 
                <a href="service_main_add.php" class="btn btn-primary">Add Programs Category</a>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
                <thead>
              <tr>
                  <th> Image </th>
                  <th>Category Name</th>
                  <th>Program Date</th>
                  <th>Program Location</th>
                  <th> </th>      
                  <th> </th>      
                  <th> </th>
                  <th> </th>
              </tr>
              </thead>
              <tbody>
             <?php  
                $where_array = array(
                    'type' => 'eventmain'
                );
                if($datas = $model->select("services", $where_array)){ 
                  $num = count($datas);
                  // echo $num; 
                  $i= '1';
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $image = $data['image'];
                        $title = $data['title'];
                        $edate = $data['edate'];
                        $elocation = $data['elocation'];
                        $status = $data['status'];                    
             ?>
                <tr>
                  <td> <img src="../<?= $image; ?>" height="100px" width="120px"> </td>  
                  <td> <?= $title; ?> </td>     
                  <td> <?= $edate; ?> </td>     
                  <td> <?= $elocation; ?> </td>  
                  <td> <a href="services.php?main_id=<?= $id; ?>"> <span class="label label-warning"> View/Add Programs </span> </a> </td>        
                  <td>     
                        <?php if($status == '0') { ?>

                        <a  href="service_main.php?service_status=1&service_id=<?= $id; ?> " class="label label-danger"> Denied </a>  

                        <?php } else { ?>

                        <a  href="service_main.php?service_status=0&service_id=<?= $id; ?>" class="label label-success">Approved</a>  
                        <?php } ?>
                  </td>
             
                  <td>
                      <a href="service_main_add.php?service_id=<?php echo $id; ?>"> <span class="label label-warning"> Edit </span> </a> 
                  </td>
                  <td>
                      <a href="service_main.php?service_del_id=<?php echo $id; ?>" onclick="return confirm('Are you sure you want to Remove?');"> <span class="label label-danger"> Delete </span> </a> 
                  </td> 
               </tr>
            <?php $i++;} } ?>
            </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  
        <!-- <script type="text/javascript">
        var count= <?php //echo $num; ?>
            for(var i=1 ;i<=count; i++;){
            var longText = $('#discription'+i);
            longText.text(longText.text().substr(0, 500));
            }
        </script>   -->

        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>