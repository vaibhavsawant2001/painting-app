<?php
include_once('header.php');

if (isset($_REQUEST['rc_id'])) {
    $where = array(
        'id' => $_REQUEST['rc_id']
    );
    if($datas = $model->select("rescue_centers",$where)){ 
        foreach($datas as $data){ 
            $id = $data['id'];
            $image = $data['image'];
            $name = $data['name'];
            $email = $data['email'];
            $phone = $data['phone'];
            $address = $data['address'];
            $city = $data['city'];
            $state = $data['state'];
            $country = $data['country'];
            $zip_code = $data['zip_code'];
            $founder_name = $data['founder_name'];
            $founder_email = $data['founder_email'];
            $founder_phone = $data['founder_phone'];
            $website_url = $data['website_url'];
            $animal_rescue = $data['animal_rescue']; 
            $stray_prc = $data['stray_prc']; 
            $your_capacity = $data['your_capacity']; 
            $associates = $data['associates']; 
            $volunteers_cnt = $data['volunteers_cnt']; 
            $training = $data['training']; 
            $vehicles = $data['vehicles']; 
            $animals_cnt = $data['animals_cnt']; 
            $exp_plan = $data['exp_plan']; 
            $services = $data['services']; 
            $future_plans = $data['future_plans']; 
            $vets = $data['vets']; 
            $vets_detail = $data['vets_detail']; 
            $vaccination = $data['vaccination']; 
            $vaccination_detail = $data['vaccination_detail']; 
            $own_pet = $data['own_pet']; 
            $backup_detail = $data['backup_detail']; 
            $insurance = $data['insurance']; 
            $nk_policy = $data['nk_policy']; 
            $nk_policy_detail = $data['nk_policy_detail']; 
            $nuetering_policy = $data['nuetering_policy']; 
            $nuetering_policy_detail = $data['nuetering_policy_detail']; 
            $animal_rehome = $data['animal_rehome']; 
            $animal_rehome_detail = $data['animal_rehome_detail']; 
            $work_with = $data['work_with']; 
            $consent = $data['consent']; 
            $consent_detail = $data['consent_detail']; 
            $imp_note = $data['imp_note']; 
            $about = $data['about']; 
            $image = $data['image']; 
            $tc = $data['tc']; 
            $facebook = $data['facebook']; 
            $twitter = $data['twitter']; 
            $instagram = $data['instagram']; 
            $linkedin = $data['linkedin']; 
            $date = $data['date']; 
        }
    }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
             <?= $name; ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="rescue_centers.php"><i class="fa fa-dashboard"></i> Rescue Centers</a></li>
            <li class="active"> <?= $name; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- End Main Content -->
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box" style="padding: 15px;">
                    <!-- /.box-header -->
                    <div class="box-body rc_detail table-responsive no-padding">
                      
                            <div class="col-md-6 col-lg-6 col-xs-12">
                                <img class="rc_detail_img" src="<?= (!empty($image)) ? '../'.$image : '../assests/img/user.png'; ?>" alt="<?= $name; ?>" width="100%" />
                            </div>
                            <div class="col-md-6">
                                <label>Name:</label>
                                <span><?= $name; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>Email Address:</label>
                                <span><?= $email; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>Telephone / Mobile Number: </label>
                                <span><?= $phone; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>Address</label>
                                <span><?= $address; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>City :</label>
                                <span><?= $city; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>State :</label>
                                <span><?= $state; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>Country :</label>
                                <span><?= $country; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>Zip/Postal Code :</label>
                                <span><?= $zip_code; ?></span>
                            </div>
                            <div class="col-md-12">
                                <hr>
                                <h2>Rescue Founder / Manager Information</h2>
                                <hr>
                            </div>
                            <div class="col-md-12">
                                <label>Rescue Founder / Manager Name :</label>
                                <span><?= $founder_name; ?></span>
                            </div>
                            <div class="col-md-12">
                                <label>Rescue Founder / Manager Email :</label>
                                <span><?= $founder_email; ?></span>
                            </div>
                            <div class="col-md-12">
                                <label>Rescue Founder / Manager Contact Number :</label>
                                <span><?= $founder_phone; ?></span>
                            </div>
                            <div class="col-md-3">
                                <label>Rescue website URL </label><br>
                                <span><?= $website_url; ?></span>
                            </div>
                            <div class="col-md-3">
                                <label>Rescue Facebook URL</label><br>
                                <span><?= $facebook; ?></span>
                            </div>
                            <div class="col-md-3">
                                <label>Rescue Twitter URL</label><br>
                                <span><?= $twitter; ?></span>
                            </div>
                            <div class="col-md-3">
                                <label>Rescue Instagram URL</label><br>
                                <span><?= $instagram; ?></span>
                            </div>
                            <div class="col-md-12">
                                <hr>
                                <h2>Other Information</h2>
                                <hr>
                            </div>
                            <?php if(!empty($training)){ ?>
                            <div class="col-md-12">
                                <label>Do you offer first aid /other training to volunteers? </label><br>
                                <span><?= $training; ?></span>
                            </div>
                            <?php } if(!empty($vehicles)){ ?>
                            <div class="col-md-12">
                                <label>Q. Do you have vehicles that is used for rescue animals? </label><br>
                                <span><?= $vehicles; ?></span>
                            </div>
                            <?php } if(!empty($vets)){ ?>
                            <div class="col-md-12">
                                <label>Q. Are there any vets on premises?</label><br>
                                <span><?= $vets; ?></span>,   <span><?= $vets_detail; ?></span>
                            </div>
                            <?php } if(!empty($vaccination)){ ?>
                            <div class="col-md-12">
                                <label>Q. Do you have vaccination packages? </label><br>
                                <span><?= $vaccination; ?></span>, <span><?= $vaccination_detail; ?></span>
                            </div>
                            <?php } if(!empty($animal_rehome)){ ?>
                            <div class="col-md-12">
                                <label>Q. Do you require animals rehomed by yourself to come back to the rescue if an
                                    adoption placement does not work out? </label><br>
                                <span><?= $animal_rehome; ?></span>, <span><?= $animal_rehome_detail; ?></span>
                            </div>
                            <?php } if(!empty($consent)){ ?>
                            <div class="col-md-12">
                                <label>Q. Do you give consent for an Animal team representative to visit your premises at
                                    the rescue address provided above? </label><br>
                                <span><?= $consent; ?></span>, <span><?= $consent_detail; ?></span>
                            </div>
                            <?php } if(!empty($own_pet)){ ?>
                            <div class="col-md-12">
                                <label>Q. Do you currently own other Pets this organisation / rescue offer FULL LIFETIME
                                    Rescue Back Up to its animals? </label><br>
                                <span><?= $own_pet; ?></span>, <span><?= $backup_detail; ?></span>
                            </div>
                            <?php } if(!empty($insurance)){ ?>
                            <div class="col-md-12">
                                <label>Q. Does this rescue have Liability Insurance in place that covers the general public
                                    including volunteers helping with homechecks, assessments and transport relating to
                                    the animals for which it is responsible? </label><br>
                                    <span><?= $insurance; ?></span>
                            </div>
                            <?php } if(!empty($nk_policy)){ ?>
                            <div class="col-md-12">
                                <label>Q. Does this rescue have a "NO KILL" Policy in place?</label><br>
                                <span><?= $nk_policy; ?></span>,  <span><?= $nk_policy_detail; ?></span>
                            </div>
                            <?php } if(!empty($nuetering_policy)){ ?>
                            <div class="col-md-12">
                                <label>Q. Do you spey or neuter your animals before rehoming?</label><br>
                                <span><?= $nuetering_policy; ?></span>, <span><?= $nuetering_policy_detail; ?></span>
                            </div>       
                            <?php } if(!empty($animal_rescue)){ ?>    
                            <div class="col-md-12">
                                <label for="">Q. What animals you rescue? </label><br>
                                <span><?= $animal_rescue; ?></span>
                            </div>
                            <?php } if(!empty($work_with)){ ?>
                            <div class="col-md-12">
                                <label for="">Q. Do you work with ......? </label><br>
                                <span><?= $work_with; ?></span>
                            </div>
                            <?php } if(!empty($stray_prc)){ ?>
                            <div class="col-md-12">
                                <label>Q. What percentage of stray you accomodate?</label><br>
                                <span><?= $stray_prc; ?></span>
                            </div>
                            <?php } if(!empty($your_capacity)){ ?>
                            <div class="col-md-12">
                                <label>Q. What is your capacity?</label>
                                <span><?= $your_capacity; ?></span>
                            </div>
                            <?php } if(!empty($volunteers_cnt)){ ?>
                            <div class="col-md-12">
                                <label>Q. How many volunteers you have currently?</label><br>
                                <span><?= $volunteers_cnt; ?></span>
                            </div>
                            <?php } if(!empty($animals_cnt)){ ?>
                            <div class="col-md-12">
                                <label>Q. How many animals now?</label><br>
                                <span><?= $animals_cnt; ?></span>
                            </div>
                            <?php } if(!empty($associates)){ ?>
                            <div class="col-md-12">
                                <label>Q. What associates you have? What are their capacity? </label><br>
                                <span><?= $associates; ?></span>
                            </div>
                            <?php } if(!empty($exp_plan)){ ?>
                            <div class="col-md-12">
                                <label>Q. What are your future expansion plans?</label><br>
                                <span><?= $exp_plan; ?></span>
                            </div>
                            <?php } if(!empty($services)){ ?>
                            <div class="col-md-12">
                                <label>Q. What services do you offer currently?</label><br>
                                <span><?= $services; ?></span>
                            </div>
                            <?php } if(!empty($future_plans)){ ?>
                            <div class="col-md-12">
                                <label>Q. What are your plans for future? </label><br>
                                <span><?= $future_plans; ?></span>
                            </div>
                            <?php } if(!empty($imp_note)){ ?>
                            <div class="col-md-12">
                                <label>Q. Please provide any other information that you believe is important about Your
                                    rescue or the rescue you represent?</label><br>
                                <span><?= $imp_note; ?></span>
                            </div>
                            <?php } ?>
                   
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.content -->
</div>


<?php include('footer.php'); ?>