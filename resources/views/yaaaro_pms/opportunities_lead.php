<?php
include_once('header.php');
    
    // delete
if (isset($_REQUEST['testi_del_id'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['testi_del_id'])  );
    if($model->delete("contact", $where_array)){
        $succ = 'Delete';
    }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    Contact Us
    <!-- <small> <button style="width:80px;" onclick="window.location = 'seometa.php?seo_page_id=4';" type="button" class="btn btn-block btn-primary btn-sm"> Edit SEO </button></small> -->
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Contact Us</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
                <thead>
              <tr>

                <th hidden> ID </th>
                <th> Name</th>
                <th> Email</th>
                <th> Phone</th>
                <th> Message </th> 
                <th> Date </th>
                <th> file </th>    
                <th> Delete </th>
              </tr>
              </thead>
                <tbody>
             <?php  
             $where_array = array(
               'type' => 'opportunities'
             );
              if($datas = $model->select("opportunities", $where_array)){  
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $fname = $data['fname'];
                        $lname = $data['lname'];
                        $email = $data['email'];
                        $phone = $data['phone'];
                        $file = $data['file'];
                        $description = $data['description'];
                        $date = $data['date'];                  
             ?>
                <tr>
                  <td hidden> <?= $id; ?> </td>   
                  <td> <?= $fname; ?> &nbsp; <?= $lname; ?></td>  

                  <td> <?= $email; ?> </td>  
                  <td> <?= $phone; ?> </td> 
                  <td> <?= $description; ?> </td>
                  <td> <?= $date; ?> </td> 
                  <td> <a class="label label-warning" href="../<?php echo $file; ?>">download</a> </td> 
                         
                  <td>
                    <a href="contact.php?testi_del_id=<?php echo $id; ?>" onclick="return confirm('Are you sure you want to delete ?')"> <span class="label label-danger"> Delete </span> </a>
                  </td> 
               </tr>
            <?php } } ?>
            </tbody>

              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  

      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>