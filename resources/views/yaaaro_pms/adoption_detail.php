<?php
include_once('header.php');

if (isset($_REQUEST['a_id'])) {
    $where = array(
        'id' => $_REQUEST['a_id'],
        'user_type' => 'Adoption'
    );
    if($datas = $model->select("volunteer",$where)){ 
        foreach($datas as $data){ 
            $id = $data['id'];
            $image = $data['image'];
            $name = $data['name'];
            $email = $data['email'];
            $phone = $data['phone'];
            $address = $data['address'];
            $city = $data['city'];
            $state = $data['state'];
            $country = $data['country'];
            $zip_code = $data['zip_code'];
            $dob = $data['dob'];
            $age = $data['age'];

            $health_problem = $data['health_problem'];
            $health_problem_detail = $data['health_problem_detail'];
            $transport = $data['transport'];
            $y_work = $data['y_work']; 
            $y_work_hr = $data['y_work_hr']; 
            $y_job_title = $data['y_job_title']; 
            $p_work = $data['p_work']; 
            $p_work_hr = $data['p_work_hr']; 
            $p_job_title = $data['p_job_title']; 
            $home = $data['home']; 
            $property_type = $data['property_type']; 
            $garden_secure = $data['garden_secure']; 
            $fence_height = $data['fence_height']; 
            $adult_cnt = $data['adult_cnt']; 
            $child_cnt = $data['child_cnt']; 
            $other_rescue = $data['other_rescue']; 
            $other_rescue_name = $data['other_rescue_name']; 
            $animal_issue = $data['animal_issue']; 
            $own_ohter_pet = $data['own_ohter_pet']; 
            $own_pet_detail = $data['own_pet_detail']; 
            $other_pet = $data['other_pet']; 
            $other_pet_detail = $data['other_pet_detail']; 
            $note = $data['nnote']; 
            $image = $data['image'];
            $facebook = $data['facebook']; 
            $twitter = $data['twitter']; 
            $instagram = $data['instagram']; 
            $linkedin = $data['linkedin']; 
            $date = $data['date']; 
        }
    }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
             <?= $name; ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="volunteer.php"><i class="fa fa-dashboard"></i> Volunteer</a></li>
            <li class="active"> <?= $name; ?></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- End Main Content -->
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box" style="padding: 15px;">
                    <!-- /.box-header -->
                    <div class="box-body rc_detail table-responsive no-padding">
                      
                            <div class="col-md-4 col-lg-4 col-xs-12">
                                <img class="rc_detail_img" src="<?= (!empty($image)) ? '../'.$image : '../assests/img/user.png'; ?>" alt="<?= $name; ?>" width="100%" />
                            </div>
                            <div class="col-md-6">
                                <label>Name:</label>
                                <span><?= $name; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>Email Address:</label>
                                <span><?= $email; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>Telephone / Mobile Number: </label>
                                <span><?= $phone; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>Date of Birth:</label>
                                <span><?= $dob; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>Address :</label>
                                <span><?= $address; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>City :</label>
                                <span><?= $city; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>State :</label>
                                <span><?= $state; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>Country :</label>
                                <span><?= $country; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>Zip/Postal Code :</label>
                                <span><?= $zip_code; ?></span>
                            </div>
                            <div class="col-md-12">
                                <hr>
                                <h2>Other Information</h2>
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <label>Are You Over 18 :</label>
                                <span><?= $age; ?></span>
                            </div>   
                            <div class="col-md-12">
                                <label>DO YOU OR ANY FAMILY MEMBER RESIDING AT THE PROPERTY HAVE, OR HAVE SUFFERED, ANY HEALTH OR MENTAL HEALTH PROBLEMS? This will not preclude you from adopting one of our pet but special care will be taken to match you successfully with the appropriate Animal. :</label>
                                <span><?= $health_problem; ?></span>,    <span><?= $health_problem_detail; ?></span>
                            </div>
                            <div class="col-md-12">
                            <hr><h4> Transport </h4><hr>
                            </div>
                            <div class="col-md-6">
                                <label>Do you have your own transport?  :</label>
                                <span><?= $transport; ?></span>
                            </div>
                            <div class="col-md-12">
                            <hr><h4> Your Employment </h4><hr>
                            </div>
                            <div class="col-md-12">
                                <label>Do you work :</label>
                                <span><?= $y_work; ?></span>
                            </div>
                            <div class="col-md-12" id="work_txt">
                                <label>Hours worked per day :</label>
                                <span><?= $y_work_hr; ?></span>
                            </div>                  
                            <div class="col-md-6" id="jwork_txt">
                                <label>Job title :</label>
                                <span><?= $y_job_title; ?></span>
                            </div>
                            <div class="col-md-12">
                                <label>Does your partner work :</label>
                                <span><?= $p_work; ?></span>
                            </div>
                            <div class="col-md-12" id="pwork_txt">
                                <label>Hours worked per day :</label>
                                <span><?= $p_work_hr; ?></span>
                            </div>                  
                            <div class="col-md-6" id="jpwork_txt">
                                <label>Job title :</label>
                                <span><?= $p_job_title; ?></span>
                            </div>
                            <div class="col-md-12">
                            <hr><h4> About your home </h4><hr>
                            </div>
                            <div class="col-md-12">
                                <label>Do you own your home  :</label>
                                <span><?= $home; ?></span>
                            </div>  
                            <div class="col-md-12" id="own_home_note">
                                <span class="required"> If you rent or share your property then we will need to see written evidence from the home-owner to prove you are allowed to keep pets.</span>
                            </div>
                            <div class="col-md-6">
                                <label>Property type :</label>
                                <span><?= $property_type; ?></span>
                            </div>
                            <div class="col-md-12">
                                <label>Is the garden secure :</label>
                                <span><?= $garden_secure; ?></span>
                            </div>
                            <div class="col-md-6">
                                <label>Approx fence height:</label>
                                <span><?= $fence_height; ?></span>
                            </div>
                            <div class="col-md-12">
                            <hr><h3>People living at the property and visitors</h3>
                            <span class="required">Please tell us about ALL people that live at the property and visit on a regular basis.</span><hr>
                            </div>
                            <div class="col-md-12">
                                <label>Adults living at the property :</label>
                                <span><?= $adult_cnt; ?></span>
                            </div> 
                            <div class="col-md-12">
                            <hr><h4>PLEASE NOTE:</h4>
                            <span class="required">We do not rehome to familes with children under the age of 8 years...If your children are between 2yrs and 8yrs then with your permission, we will pass your application on to our Admin” - they re-home pet and puppies to our applicants with younger children.</span><hr>
                            </div>
                            <div class="col-md-12">
                                <label>Children living at the property under 18 years of age  :</label>
                                <span><?= $child_cnt; ?></span>
                            </div>  
                            <div class="col-md-12"> <hr> </div>
                            <div class="col-md-12">
                                <label>Have you applied to any other rescue?  :</label>
                                <span><?= $other_rescue; ?></span>,    <span><?= $other_rescue_name; ?></span>
                            </div>            
                            <div class="col-md-12">
                                <label for="">Would you take a Animal with issues. If so which issues are you prepared to work with :</label><br>
                                <span><?= $animal_issue; ?></span>
                            </div>
                            <div class="col-md-12">
                            <hr><h4>Existing pets and animals</h4>                               
                            <p>Please tell us about the other pets/animals living at your property..if none please put 'None'</p>
                            <hr>
                            </div>  
                            <div class="col-md-12">
                                <label>Do you currently own other pets :</label>
                                <span><?= $own_ohter_pet; ?></span>,    <span><?= $own_pet_detail; ?></span>
                            </div>
                            <div class="col-md-12">
                                <label>Any other pets :</label>
                                <span><?= $other_pet; ?></span>,  <span><?= $other_pet_detail; ?></span>
                            </div>
                            <div class="col-md-12">
                            <hr><h4>Provide Additional Information About Your Experience With Dog/Cat</h4><hr>
                            </div>                            
                            <div class="col-md-12">
                              <span><?= $note; ?></span>
                            </div>
                   
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>

        </div>
    </section>
    <!-- /.content -->
</div>


<?php include('footer.php'); ?>