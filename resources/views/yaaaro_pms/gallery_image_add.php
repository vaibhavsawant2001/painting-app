<?php

// ob_start();
// error_reporting(E_ALL);
// error_reporting(-1);

include_once('header.php');



if(isset($_REQUEST['gallery_id'])){
  $where = array('id' => $_REQUEST['gallery_id']);
  if($datas = $model->select("gallery", $where)){         
      foreach($datas as $data){ 
          $gallery_id = $data['id'];  
          $gallery_title = $data['title'];  
      }
  }   
}

// for edit
if(isset($_REQUEST['gallery_view_id'])){    
  $edit_id = strip_tags($_REQUEST['gallery_view_id']);
  $where = array( 'id' => $edit_id );
  if($others = $model->select('gallery',$where)){
      foreach($others as $other){   
        $id = $other['id'];
        $title = $other['title'];
        $image = $other['image']; 
        $gallery_id = $other['gallery_id']; 
      }
  }
      $where = array('id' => $gallery_id);
      if($datas = $model->select("services", $where)){         
          foreach($datas as $data){ 
              $service_title = $data['title'];  
              $service_id = $data['id'];  
          }
      }
}

if(isset($_POST['gallery_view_edit'])){
  $gallery_view_edit1 = 'gallery_view_edit'; 
  $edit_id = $_POST['id'];

 $image1 = strip_tags($_POST['image1']);
  $image2 = $_FILES['image']['name'];
    if (empty($image2)) {
      $file = $image1;
    }
    else{  
      // for image replace
      $where = array( 'id' => $edit_id );
      $stmt_del = $model->select('gallery',$where);
      foreach($stmt_del as $delete_image){
        $deleteimage = '../'.$delete_image['image'];
        unlink($deleteimage);
      }

    $about_file = $_FILES['image']['name'];
    $target_dir1 = '../uploads/gallery/';
    $target_dir = 'uploads/gallery/';
    $newfilename = date('dmYHis').str_replace(" ", "", basename($about_file));
    $file1 = $target_dir1 . basename($newfilename);
    $file = $target_dir . basename($newfilename);
    $uploadOk = 1;
    $temp_file = $_FILES["image"]["tmp_name"];
  }

  $where_other = array( 
    'id' => $edit_id
  );

  $update_array = array(
      'image' => $file,
      'title' => strip_tags(htmlentities($_POST['title'])),
      'date' => $todayDate
  );

  if($model->update("gallery", $update_array, $where_other)){
    move_uploaded_file($temp_file, $file1);

    $model->url('gallery_view.php?gallery_id='.$gallery_id.'&msg=Update');
  }else{
      $model->url('gallery_view.php?gallery_id='.$gallery_id.'&gallery_view_id='.$edit_id.'&msg=error');
  }
}

// for insert
if(isset($_POST['submit'])){

    // ================ /img ====================
    // s$insertValuesSQL = '';
    $extension=array("jpeg","jpg","png","gif");
   foreach($_FILES["image"]["tmp_name"] as $key=>$tmp_name) {
        $file_name = $_FILES["image"]["name"][$key];
        $file_tmp = $_FILES["image"]["tmp_name"][$key];
      // exit();
        $ext = pathinfo($file_name,PATHINFO_EXTENSION);

        if(in_array($ext,$extension)) {
          $target_dir1 = '../uploads/gallery/';
          $target_dir = 'uploads/gallery/';
          $newfilename = date('dmYHis').str_replace(" ", "", basename($file_name));
          $file1 = $target_dir1 . basename($newfilename);
          $file = $target_dir . basename($newfilename);
          $temp_file = $_FILES["image"]["tmp_name"][$key];
          
          // $insertValuesSQL .= "('".$target_dir."', NOW()),"; 
   

  $insert_array = array(
      'gallery_id' => strip_tags(htmlentities($_POST['gallery_id'])),
      'image' => $file,
      'title' => strip_tags(htmlentities($_POST['title'])),  
      'gallery_type' => 'image', 
      'type' => 'sub', 
      'date' => $todayDate,
      'status' => '1'
  );

    if($model->insert("gallery",$insert_array)){
           move_uploaded_file($temp_file, $file1);  
    }
    else
        $msg="faild";
    }   
      }
   
 
          $model->url('gallery_view.php?gallery_id='.$_POST['gallery_id'].'&succ');
  
  
}
 
?>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Add <?= $gallery_title; ?> Images
    </h1>
    <ol class="breadcrumb">
      <li><a href="iadmin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="gallery.php">Gallery</a></li>
      <li><a href="gallery_view.php?gallery_id=<?= $gallery_id; ?>"><?= $gallery_title; ?></a></li>
      <li class="active">Add Images</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 


    <div class="row">
      <!-- left column -->
      <div class="col-md-12">

        <div class="box box-primary">

          <div class="box-header with-border">

          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form enctype="multipart/form-data" method="POST">
            <div class="box-body">
                <?php if(isset($_REQUEST['gallery_view_id'])){ ?>
                  <input type="hidden" class="form-control" name="id" value="<?= $edit_id; ?>" >
                <?php } ?>
                <?php if(isset($_REQUEST['gallery_id'])){ ?>
                  <input type="hidden" class="form-control" name="gallery_id" value="<?= $gallery_id; ?>" >
                <?php } ?>

                <div class="row">
                  <div class="col-md-6 form-group">
                    <label for="Offer Type"> Title : </label>
                    <input type="text" class="form-control" name="title" value="<?php if(isset($_REQUEST['gallery_view_id'])) { echo $title; } elseif(isset($_POST['title'])) { echo $_POST['title']; } else{ echo ''; } ?>"  >
                  </div>                 
                </div>

                <div class="form-group">
                  <label for="exampleInputFile">Image :- <span  style="color: #ea3232">(Width) 600px × (Height) 800px </span> </label> <br />
                  <?php if(isset($_REQUEST['gallery_view_id'])){ ?>
                    <input type="hidden" name="image1" value="<?= $image; ?>">
                    <img src="../<?= $image; ?>"  height="100" width="100px"/> <br /> <br />
                  <?php } ?>
                     <?php if(!isset($_REQUEST['gallery_view_id'])){ ?>
                  <input type="file" name="image[]" size="12" multiple  data-toggle="tooltip"  data-placement="top" title="For Better Result Use Width and Height as Mention Above">  
                <?php }else{ ?>
                     <input type="file" name="image" size="12"  data-toggle="tooltip"  data-placement="top" title="For Better Result Use Width and Height as Mention Above">  
                   <?php } ?>
                </div>  
              <!-- /.box-body -->

              <div class="box-footer" align="center">
                <button type="submit" name="<?php if(isset($_REQUEST['gallery_view_id'])) { echo 'gallery_view_edit'; } elseif(isset($gallery_view_edit1) == 'gallery_view_edit') { echo 'gallery_view_id'; } else{ echo 'submit'; } ?>" value="submit" class="btn btn-primary">Submit</button>
              </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>

<?php include('footer.php'); ?>