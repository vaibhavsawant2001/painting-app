<?php
    include_once('header.php');

    // Change Status
  if ( isset($_REQUEST['regist_id']) && isset($_REQUEST['regist_status']) ) {
    $where_array = array( 'id' => strip_tags($_REQUEST['regist_id']) );
    $update_array = array(  'status' => strip_tags($_REQUEST['regist_status']) );    

    if($model->update("regist", $update_array, $where_array)){
        $succ = 'Status Update';
    }
  }

?>
  <!-- ====================== Body  ========================= -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <h1>
         Register Details
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Register Details</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
              <tr>
                <th hidden> ID </th>
                <th> Logo </th> 
                <th> Heading1 </th>  
                <th> Heading2 </th>  
                <th> Heading3 </th>  
                <th> rule1 </th>              
                <th> rule2 </th>              
                <th> rule3 </th>              
                <th> rule4 </th>              
                <th> rule5 </th>              
                <th> rule6 </th>              
                <th> rule7 </th>              
                <th> Status </th>            
                <th> Edit </th>            
              </tr>
              <?php
                  $where_array = array(
                    'id' => '1'
                  );
                  if($datas = $model->select("regist", $where_array)){  
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $logo = $data['logo'];
                        $head1 = $data['head1'];
                        $head2 = $data['head2'];
                        $head3 = $data['head3'];
                        $rule1 = $data['rule1'];
                        $rule2 = $data['rule2'];
                        $rule3 = $data['rule3'];
                        $rule4 = $data['rule4'];
                        $rule5 = $data['rule5'];
                        $rule6 = $data['rule6'];
                        $rule7 = $data['rule7'];
                        $status = $data['status'];
              ?>
              <tr style="vertical-align: center;">

                <td hidden> <?= $id; ?> </td>
                <td>
                  <?php 
                    if(!empty($logo)){
                  ?>
                  <img src="../<?= $logo; ?>"  height="100" width="100px"/>
                    <?php }?>
                </td>
                <td><?= $head1; ?></td>
                <td><?= $head2; ?></td>
                <td><?= $head3; ?></td>
                <td> <?= $rule1; ?></td>
                <td> <?= $rule2; ?></td>
                <td> <?= $rule3; ?></td>
                <td> <?= $rule4; ?></td>
                <td> <?= $rule5; ?></td>
                <td> <?= $rule6; ?></td>
                <td> <?= $rule7; ?></td>
                <td>     
                        <?php if($status == '0') { ?>

                        <a  href="regist.php?regist_status=1&regist_id=<?= $id; ?> " class="label label-danger"> Denied </a>  

                        <?php } else { ?>

                        <a  href="regist.php?regist_status=0&regist_id=<?= $id; ?>" class="label label-success">Approved</a>  
                        <?php } ?>
                  </td>
                <td> 
                    <a href="regist_edit.php?regist_edit=<?= $id;?>" class="label label-warning">Edit</a> 
                </td> 
              </tr>
        <?php }} ?>
            </table>
          </div>
        </div>      
    </section>
    <section class="content">
        <div class="box">
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover">
              <tr>
                <th hidden> ID </th>
                <th> PLAYER FULL NAME </th> 
                <th> PLAYER FATHER NAME </th>      
                <th> PLAYER MOTHER NAME </th>                  
                <th> SCHOOL/COLLEGE NAME </th>              
                <th> RESIDENCIAL ADDRESS </th> 
                <th> PRIMARY MOBILE </th>      
                <th> SECONDARY MOBILE </th>                  
                <th> PREVIOUS INJURY </th>
                <th> GENDER </th> 
                <th> DATE OF BIRTH </th>      
                <th> PLAYER AGE </th>                  
                <th> RACE EVENT (UNDER 16) </th>              
                <th> RACE EVENT (UNDER 18) </th> 
                <th> CATEGORY </th>      
                <th> NOMADIC TRIBES </th>                  
                <th> DECLARATION </th>                         
                <th> PHOTO </th>                         
                <th> SIGNATURE </th>                         
              </tr>
              <?php
              $where = array(
                'agree' => 'ACCEPTED'
              );
                  if($datas = $model->select("register",$where)){  
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $pfname = $data['pfname'];
                        $pmname = $data['pmname'];
                        $plname = $data['plname'];
                        $ffname = $data['ffname'];
                        $fmname = $data['fmname'];
                        $flname = $data['flname'];
                        $mfname = $data['mfname'];
                        $mmname = $data['mmname'];
                        $mlname = $data['mlname'];
                        $sname = $data['sname'];
                        $rname = $data['rname'];
                        $mobile = $data['mobile'];
                        $mobile2 = $data['mobile2'];
                        $Injury = $data['Injury'];
                        $radio = $data['radio'];
                        $gender = $data['gender'];
                        $dob = $data['dob'];
                        $age = $data['age'];
                        $agecat = $data['agecat'];
                        $under16 = $data['under16'];
                        $under18 = $data['under18'];
                        $category = $data['category'];
                        $tribes = $data['tribes'];
                        $decleration = $data['decleration'];
                        $agree = $data['agree'];
                        $photo = $data['photo'];
                        $sign = $data['sign'];
                        
              ?>
              <tr>

                <td hidden> <?= $id; ?> </td>
                <td><?= $pfname; ?></td>
                <td><?= $pmname; ?></td>
                <td><?= $plname; ?></td>
                <td><?= $ffname; ?></td>
                <td><?= $fmname; ?></td>
                <td><?= $flname; ?></td>
                <td><?= $mfname; ?></td>
                <td><?= $mmname; ?></td>
                <td><?= $mlname; ?></td>
                <td><?= $sname; ?></td>
                <td><?= $rname; ?></td>
                <td><?= $mobile; ?></td>
                <td><?= $mobile2; ?></td>
                <td><?= $Injury; ?></td>
                <td><?= $radio; ?></td>
                <td><?= $gender; ?></td>
                <td><?= $dob; ?></td>
                <td><?= $age; ?></td>
                <td><?= $agecat; ?></td>
                <td><?= $under16; ?></td>
                <td><?= $under18; ?></td>
                <td><?= $category; ?></td>
                <td><?= $tribes; ?></td>
                <td><?= $decleration; ?></td>
                <td><img src="../<?= $photo; ?>" style="height: 100px; width: 100px;" /></td>
                <td><img src="../<?= $sign; ?>" style="height: 100px; width: 100px;" /></td>
               
              </tr>
        <?php }} ?>
            </table>
          </div>
        </div>      
    </section>
    <!-- mission -->
  
  </div>
  <!-- /.content-wrapper -->

  
 <!-- ========================== footer ===================== -->

 <?php include('footer.php');?>
