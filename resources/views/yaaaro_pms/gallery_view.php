<?php
include_once('header.php');
if($_REQUEST['gallery_id']){
  $gallery_id = strip_tags($_REQUEST['gallery_id']);
  $where = array(
    'id' => $gallery_id
  );
  if($datas = $model->select("gallery", $where)){         
      foreach($datas as $data){ 
          $gallery_title = $data['title'];  
          $gallery_id = $data['id'];  
      }
  }
}

if ( isset($_REQUEST['gallery_view_id1']) && isset($_REQUEST['gallery_view_status']) ) {
    $where_array = array( 'id' => strip_tags($_REQUEST['gallery_view_id1']) );
    $update_array = array( 'status' => strip_tags($_REQUEST['gallery_view_status']));    

    if($model->update("gallery", $update_array, $where_array)){
        $model->url('gallery_view.php?gallery_id='.$_REQUEST['gallery_id'].'&succ=Status_Update');
    }
}
    // delete
elseif (isset($_REQUEST['gallery_del_id'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['gallery_del_id'])  );
   
    $stmt_del = $model->select('gallery',$where_array);
    foreach($stmt_del as $delete_image){
      if(!empty($delete_image['image'])){
      $deleteimage = '../'.$delete_image['image'];
      unlink($deleteimage);
      }
    }
    if($model->delete("gallery", $where_array)){
      $succ = 'Delete';
    }
}

    
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1><?= $gallery_title; ?> </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <?php if(isset($_REQUEST['gallery_id'])){?>
      <li><a href="gallery.php">Gallery</a></li>
      <?php }?>
      <li class="active"><?= $gallery_title; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 


    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center"> 
                <a href="gallery_image_add.php?gallery_id=<?= $gallery_id; ?>" class="btn btn-primary">Add Images</a>
                <a href="vgallery_add.php?gallery_id=<?= $gallery_id; ?>" class="btn btn-primary">Add Video</a>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
                <thead>
              <tr>

                <th> Sr. No. </th>
                <th> Image</th>    
                <th> title </th>         
                <th> Status </th>      
                <th>  </th>
                <th>  </th>
              </tr>
              </thead>
              <tbody>
              <?php 
                $where = array(
                  'gallery_id'=>$gallery_id,
                  'type' => 'sub'
                );
              if($datas = $model->select("gallery",$where)){  
                    $i=1;  
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $title = $data['title'];
                        $image = $data['image'];
                        $gallery_type = $data['gallery_type'];                   
                        $status = $data['status'];                   
             ?>
                <tr>

                  <td> <?php echo $i; ?> </td> 
                  <td> <?php if($gallery_type=='image'){
                           echo '<img src="../'. $image.'" width="100px" height="100px" >' ; 
                  }if($gallery_type=='video'){
                     $url = str_replace("watch?v=",'embed/',$image);    
                    echo '<iframe width="320"  allowfullscreen="allowfullscreen"  mozallowfullscreen="mozallowfullscreen"   msallowfullscreen="msallowfullscreen"   oallowfullscreen="oallowfullscreen"  webkitallowfullscreen="webkitallowfullscreen" src="'. $url .'"></iframe>';
                  }
                  ?>

                    
                  </td>    
                  <td> <?= $title; ?> </td>       
                  <td>     
                        <?php if($status == '0') { ?>

                        <a  href="gallery_view.php?gallery_view_status=1&gallery_view_id1=<?= $id; ?>&gallery_id=<?= $gallery_id; ?>" class="label label-danger"> Denied </a>  

                        <?php } else { ?>

                        <a  href="gallery_view.php?gallery_view_status=0&gallery_view_id1=<?= $id; ?>&gallery_id=<?= $gallery_id; ?>" class="label label-success">Approved</a>  
                        <?php } ?>
                  </td>
                  <td> 
                     <?php if($gallery_type=='image'){ ?>
                    <a href="gallery_image_add.php?gallery_id=<?= $gallery_id; ?>&gallery_view_id=<?= $id; ?>"> <span class="label label-info"> Edit </span> </a> 
                    <?php } if($gallery_type=='video'){ ?>
                       <a href="vgallery_add.php?gallery_id=<?= $gallery_id; ?>&vgid=<?= $id; ?>"> <span class="label label-info"> Edit </span> </a> 
                    <?php } ?>
                  </td> 
                  <td> <a href="gallery_view.php?gallery_id=<?= $gallery_id; ?>&gallery_del_id=<?= $id; ?>"> <span class="label label-danger"> Delete </span> </a> </td> 

                </tr>
            <?php  $i++; }}  ?>
            </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  


        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>