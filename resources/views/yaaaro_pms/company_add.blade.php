@include('yaaaro_pms/head')
<script src="https://cdn.ckeditor.com/ckeditor5/46.0.1/classic/ckeditor.js"></script>
<script type="text/javascript" src="{{url('ckeditor/ckeditor.js')}}"></script>
<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Add Company
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="tag.php"> Company</a></li>
      <li class="active">Add Company</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
          </div>
          <form enctype="multipart/form-data" action="{{route('company.store')}}" method="POST">
            <div class="box-body">
              <div class="row">
                <div class="col-md-6 form-group">
                  <label for="Offer Type"> Company Title : </label>
                  <input type="text" class="form-control" name="company_title" value="">
                </div>
                <div class="col-md-6 form-group">
                  <label for="Offer Type"> Company Logo : </label>
                  <input type="file" class="form-control" name="company_logo" value="">
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-12">
                  <label for="Content"> Company Short Description : </label>
                  <textarea class="form-control" rows="4" id="editor" name="company_short_description"></textarea>
                </div>
                <div class="form-group col-md-12">
                  <label for="Content"> Company Long Description : </label>
                  <textarea class="form-control" rows="4" id="editor1" name="company_long_description"></textarea>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 form-group">
                  <label for="Offer Type"> Main Image : </label>
                  <input type="file" class="form-control" name="image1" value="">
                </div>
                <div class="col-md-6 form-group">
                  <label for="Offer Type"> Image 2 : </label>
                  <input type="file" class="form-control" name="image2" value="">
                </div>
              </div>
              <div class="row">
                <div class="col-md-6 form-group">
                  <label for="Offer Type"> Image 3 : </label>
                  <input type="file" class="form-control" name="image3" value="">
                </div>
                <div class="col-md-6 form-group">
                  <label for="Offer Type"> Image 4 : </label>
                  <input type="file" class="form-control" name="image4" value="">
                </div>
              </div>
              <h1>
                SEO
              </h1>
              <form enctype="multipart/form-data" action="" method="POST">
                <div class="box-body">
                  <input type="hidden" class="form-control" name="id" value="">
                  <div class="row">
                    <div class="form-group col-md-4">
                      <label for=" Name"> Title : </label>
                      <input type="text" class="form-control" name="header_title" readonly value="COMPANY">
                    </div>
                    <div class="form-group col-md-4">
                      <label for=" Name"> Page Title : </label>
                      <input type="text" class="form-control" name="page_title" value="">
                    </div>
                    <div class="form-group col-md-4">
                      <label for=" Name"> #URL : </label>
                      <input type="text" class="form-control" name="url_extension" value="">
                    </div>
                    <div class="form-group col-md-12">
                      <h4 style="font-weight:bold;">Only For SEO Experts</h4><br>
                      <label for="Contant"> Content : [Add meta tag ]</label>
                      <textarea class="form-control" rows="4" name="metatag"></textarea>
                    </div>
                  </div>
                </div>
              </form>
              <div class="box-footer" align="center">
                <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>
<script>
CKEDITOR.replace('editor1', {
    allowedContent: true
});
</script>
<script>
  ClassicEditor
    .create(document.querySelector('textarea'))
    .catch(error => {
      console.error(error);
    });
</script>
@include('yaaaro_pms/footer')