<?php
include_once('header.php');
    //short page data Change Status
if ( isset($_REQUEST['home_id']) && isset($_REQUEST['home_status']) ) {
  $where_array = array( 'id' => strip_tags($_REQUEST['home_id']) );
  $update_array = array(  'status' => strip_tags($_REQUEST['home_status']) );    

  if($model->update("home", $update_array, $where_array)){
      $succ = 'Status Update';
  }
}
// Change Status
if ( isset($_REQUEST['volunteer_id']) && isset($_REQUEST['volunteer_status']) ) {
    $where_array = array( 'id' => strip_tags($_REQUEST['volunteer_id']) );
    $update_array = array(  'status' => strip_tags($_REQUEST['volunteer_status']) );    

    if($model->update("volunteer", $update_array, $where_array)){
        $succ = 'Status Update';
    }
}
    // delete
if (isset($_REQUEST['volunteer_del_id'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['volunteer_del_id']) );
    $stmt_del = $model->select('volunteer',$where_array);
    foreach($stmt_del as $delete_image){
      if(!empty($delete_image['image'])){
        $deleteimage = '../'.$delete_image['image'];
        unlink($deleteimage);
      }
    }
    if($model->delete("volunteer", $where_array)){
        $succ = 'Delete';
    }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
        Adoption
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"> Adoption</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center"> 
                <!-- <a href="volunteer_add.php" class="btn btn-primary">Add Volunteer</a> -->
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
                <thead>
              <tr>
                <th> Title</th> 
                <th> Content </th>     
                <th> Status</th>
                <th> Edit</th>
              </tr>
              </thead>
                <tbody>
             <?php  
                $sql = "SELECT * FROM `home` WHERE `id`='4'";
                $datas = mysqli_query($conn, $sql);
               while($data = mysqli_fetch_assoc($datas)){
                        $id = $data['id'];
                        $title = $data['title'];
                        $heading = $data['heading'];
                        $content = $data['content'];
                        $status = $data['status'];                    
             ?>
                <tr>
                  <td> <?= $title; ?> </td>               
                  <td> <?= $content; ?> </td>                
                  <td class="action-btn">     
                    <?php if($status == '0') { ?>

                    <a  href="adoption.php?home_status=1&home_id=<?= $id; ?> " class="label label-danger"> Denied </a>  

                    <?php } else { ?>

                    <a  href="adoption.php?home_status=0&home_id=<?= $id; ?>" class="label label-success">Approved</a>  
                    <?php } ?>
                  </td>
                  <td>                    
                    <a href="home_add.php?home_id=<?php echo $id; ?>&url=adoption"> <span class="label label-info"> Edit </span> </a> 
                    
                    <!-- <a href="home.php?home_del_id=<?php echo $id; ?>"> <span class="label label-danger"> Delete </span> </a>  -->
                  </td> 
               </tr>
            <?php  } ?>
            </tbody>

              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <div class="box">
            <table id="datatable" class="table table-bordered table-striped">
                <thead>
              <tr>
                    <!-- <th> Image </th> -->
                    <th> Name </th>
                    <th> Email </th>
                    <th> Contact No. </th>
                    <th> Pet Name </th>
                    <th> Pet Gender </th>
                    <th> Pet Price </th>
                    <th> City </th>
                    <th> Zip/Postal_code </th>      
                    <th> More Info. </th>
                    <th> </th>
              </tr>
              </thead>
              <tbody>
             <?php  
              $where = array(
                'user_type' => 'Adoption'
              );
                if($datas = $model->select("volunteer" ,$where)){ 
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $image = $data['image'];
                        $name = $data['name'];
                        $email = $data['email'];
                        $phone = $data['phone'];
                        $pet_name = $data['pet_name'];
                        $pet_sex = $data['pet_sex'];
                        $pet_size = $data['pet_size'];
                        $city = $data['city'];
                        $zip_code = $data['zip_code'];
                        $status = $data['status'];                    
             ?>
                <tr>
                  <!-- <td> <img src="../<?= $image; ?>" height="100px" width="120px"> </td>   -->
                  <td> <?= $name; ?> </td> 
                  <td> <?= $email; ?> </td> 
                  <td> <?= $phone; ?> </td> 
                  <td> <?= $pet_name; ?> </td> 
                  <td> <?= $pet_sex; ?> </td> 
                  <td> <?= $pet_size; ?> </td> 
                  <td> <?= $city; ?> </td>  
                  <td> <?= $zip_code; ?> </td>  
                  <td> <a  href="adoption_detail.php?a_id=<?= $id; ?> " class="label label-warning"> More Info. </a> </td>  
                       
                                     
                  <!-- <td>     
                        <?php if($status == '0') { ?>

                        <a  href="volunteer.php?volunteer_status=1&volunteer_id=<?= $id; ?> " class="label label-danger"> Denied </a>  

                        <?php } else { ?>

                        <a  href="volunteer.php?volunteer_status=0&volunteer_id=<?= $id; ?>" class="label label-success">Approved</a>  
                        <?php } ?>
                  </td> -->
             
                  <!-- <td>
                      <a href="volunteer_add.php?volunteer_id=<?php echo $id; ?>"> <span class="label label-warning"> Edit </span> </a> 
                  </td> -->
                  <td>
                      <a href="adoption.php?volunteer_del_id=<?php echo $id; ?>" onclick="return confirm('Are you sure you want to Remove?');"> <span class="label label-danger"> Delete </span> </a> 
                  </td> 
               </tr>
            <?php } } ?>
            </tbody>
              </table>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  
     

        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>