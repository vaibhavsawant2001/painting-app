<?php
include_once('header.php');
    

  // delete
if (isset($_REQUEST['fundr_del_id'])) {
  $where_array = array( 'id' => strip_tags($_REQUEST['fundr_del_id']) );
   if($model->delete("fund_partner", $where_array)){
      $succ = 'Delete';
  }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Fund Raising 
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"> Fund Raising </li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center"> 
                <!-- <a href="volunteer_add.php" class="btn btn-primary">Add foster</a> -->
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
                <thead>
              <tr>
                    <th> Name </th>
                    <th> Comapany Name </th>
                    <th> Email </th>
                    <th> Contact No. </th>
                    <th>  Fund resign event you want to hold/sponsor</th>      
                    <th> Do you have funds/approval to arrange for fund raising</th>
                  
                    <th> </th>
              </tr>
              </thead>
              <tbody>
             <?php  
              $where = array(
                'type' => 'fund_raising'
              );
                if($datas = $model->select("fund_partner" ,$where)){ 
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $name = $data['name'];
                        $company_name = $data['company_name'];
                        $email = $data['email'];
                        $phone = $data['phone'];
                        $event_sponsor = $data['event_sponsor'];
                        $funds = $data['funds'];                  
             ?>
                <tr>
                  <td> <?= $name; ?> </td> 
                  <td> <?= $company_name; ?> </td> 
                  <td> <?= $email; ?> </td> 
                  <td> <?= $phone; ?> </td> 
                  <td> <?= $event_sponsor; ?> </td>  
                  <td> <?= $funds; ?> </td>  
                  <td>
                      <a href="fundraising.php?fundr_del_id=<?php echo $id; ?>" onclick="return confirm('Are you sure you want to Remove?');"> <span class="label label-danger"> Delete </span> </a> 
                  </td> 
               </tr>
            <?php } } ?>
            </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  
     

        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>