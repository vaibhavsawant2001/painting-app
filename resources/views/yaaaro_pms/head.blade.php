<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link rel="shortcut icon" href="{{ asset('css/dist/img/management.png') }}">
  <title>Yaaaro PMS </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ asset('css/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ asset('css/bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('css/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="{{ asset('css/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('css/bower_components/select2/dist/css/select2.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('css/dist/css/AdminLTE.min.css') }}">
  <link rel="stylesheet" href="{{ asset('css/dist/css/mystyle.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ asset('css/dist/css/skins/_all-skins.min.css') }}">
  <script src="{{ url('css/ckeditor/ckeditor.js') }}"></script>
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="{{ url('yaaaro_pms/admin') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini">
          <img src="{{ asset('css/dist/img/management.png') }}" class="logo-img" alt="User Image" style="width: 210%; margin-left: -55%; margin-top: 20%;">
        </span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg">
          <img src="{{ asset('css/dist/img/yaaaro.png') }}" class="logo-img" alt="User Image" style="height:35px">
        </span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="{{ asset('css/dist/img/management.png') }}" class="user-image" alt="User Image">
                <span class="hidden-xs">Yaaaro PMS</span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="{{ asset('css/dist/img/yaaaro.png') }}" class="" alt="User Image">
                  <p>
                    Yaaaro Management System
                  </p>
                  <p>
                    <a style="color: white;" href="http://bluesuninfo.com/" target="_blank">bluesuninfo.com</a>
                  </p>
                  <p>
                    Contact No. : <a style="color: white;" href="tel:91-22-66154433">91-22-66154433</a>
                  </p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <!-- <a href="change_password.php" class="btn btn-default btn-flat">Change Password</a> -->
                  </div>
                  <div class="pull-right">
                    <form action="{{ route('admin.logout') }}" method="POST">
                      @csrf
                      <button type="submit" class="btn btn-default btn-flat">Sign out</button>
                    </form>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <aside class="main-sidebar">
      <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
          <li class="">
            <a href="{{url('yaaaro_pms/admin')}}">
              <i class="fa fa-dashboard"></i><span>Dashboard</span>
            </a>
          </li>
          <li class="">
            <a href="{{url('yaaaro_pms/about_us')}}">
              <i class="fa fa-dashboard"></i><span>About Us</span>
            </a>
          </li>
          <li class="">
            <a href="{{url('yaaaro_pms/banner_list')}}">
              <i class="fa fa-dashboard"></i><span>Banner</span>
            </a>
          </li>
          <li class="treeview">
            <a href="">
              <i class="fa fa-dashboard"></i><span>Gallery</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="{{url('yaaaro_pms/category')}}"><i class="fa fa-circle-o"></i>Add Image Category</a></li>
              <li><a href="{{url('yaaaro_pms/image_list')}}"><i class="fa fa-circle-o"></i>Add Image</a></li>
            </ul>
          </li>
          <li class="">
            <a href="{{url('yaaaro_pms/blog_list')}}">
              <i class="fa fa-dashboard"></i><span>Blogs</span>
            </a>
          </li>
          <li class="">
            <a href="{{url('yaaaro_pms/seo')}}">
              <i class="fa fa-dashboard"></i><span>SEO</span>
            </a>
          </li>
          <li class="">
            <a href="{{url('yaaaro_pms/everything_else')}}">
              <i class="fa fa-dashboard"></i><span>Everything Else</span>
            </a>
          </li>
          <li class="">
            <a href="{{url('yaaaro_pms/our_work_list')}}">
              <i class="fa fa-dashboard"></i><span>Our Works</span>
            </a>
          </li>
          <li class="">
            <a href="{{url('yaaaro_pms/company_list')}}">
              <i class="fa fa-dashboard"></i><span>Company</span>
            </a>
          </li>
          <li class="">
            <a href="{{url('yaaaro_pms/contact')}}">
              <i class="fa fa-dashboard"></i><span>Contact</span>
            </a>
          </li>
        </ul>
      </section>
    </aside>