
<?php

// ob_start();
// error_reporting(E_ALL);
// error_reporting(-1);

include_once('header.php');


// for edit
if(isset($_REQUEST['banner_id'])){    
  $edit_id = strip_tags($_REQUEST['banner_id']);
  $where = array( 'id' => $edit_id );
  if($others = $model->select('why_us',$where)){
      foreach($others as $other){   
        $id = $other['id'];
        $title1 = $other['heading'];
        $title2 = $other['title'];
        // $title3 = $other['title3'];
        $image = $other['image']; 
        //$banner_type =$other['type'];
      }
  }
}

if(isset($_POST['banner_edit'])){
  $banner_edit1 = 'banner_edit'; 
  $edit_id = $_POST['id'];

  $image1 = strip_tags($_POST['image1']);
  $image2 = $_FILES['image']['name'];
    if (empty($image2)) {
      $file = $image1;
    }
    else{  
      // for image replace
      $stmt_del = $model->select('why_us',$where);
      foreach($stmt_del as $delete_image){
        $deleteimage = '../'.$delete_image['image'];
        unlink($deleteimage);
      }

    $about_file = $_FILES['image']['name'];
    $target_dir1 = '../uploads/why_us/';
    $target_dir = 'uploads/why_us/';
    $newfilename = date('dmYHis').str_replace(" ", "", basename($about_file));
    $file1 = $target_dir1 . basename($newfilename);
    $file = $target_dir . basename($newfilename);
    $uploadOk = 1;
    $temp_file = $_FILES["image"]["tmp_name"];
  }

  $where_other = array( 
    'id' => $edit_id
  );
 //$banner_type=addslashes(strip_tags(htmlentities($_POST['banner_type'])));
  $update_array = array(
      'image' => $file,
      'heading' => addslashes(strip_tags(htmlentities($_POST['title1']))),
      'title' => addslashes(strip_tags(htmlentities($_POST['title2']))),
      // 'title3' => addslashes(strip_tags(htmlentities($_POST['title3']))),
      //'banner_type' => 'image',
      'date' => $todayDate
  );
  if($model->update("why_us", $update_array, $where_other)){
    move_uploaded_file($temp_file, $file1);
    
          $model->url('why_us.php?succ');
       
  }else{
      $model->url('why_us_add.php?msg&banner_id='.$edit_id);
  }
}

// for insert
if(isset($_POST['submit'])){

    $about_file = $_FILES['image']['name'];
    if(!empty($about_file)){
    $target_dir1 = '../uploads/why_us/';
    $target_dir = 'uploads/why_us/';
    $newfilename = date('dmYHis').str_replace(" ", "", basename($about_file));
    $file1 = $target_dir1 . basename($newfilename);
    $file = $target_dir . basename($newfilename);
    $uploadOk = 1;
    $temp_file = $_FILES["image"]["tmp_name"];
    }else{
      $file1 = '';
      $file = '';
    }
  //$banner_type=addslashes(strip_tags(htmlentities($_POST['banner_type'])));
  $insert_array = array(
      'image' => $file,
      'heading' => addslashes(strip_tags(htmlentities($_POST['title1']))),
      'title' => addslashes(strip_tags(htmlentities($_POST['title2']))),
      // 'title3' => addslashes(strip_tags(htmlentities($_POST['title3']))),
      'date' => $todayDate,
      'type' => 'why_us',
      //'banner_type' => 'image',
      'status' => '1'
  );
  if($model->insert("why_us",$insert_array)){
        move_uploaded_file($temp_file, $file1);
        
          $model->url('why_us.php?succ');
        
  }
  else
      $msg="faild";
}  

?>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Why Us
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="banner.php"> Why Us</a></li>
      <li class="active">Add Why Us Point</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <div class="row">
      <!-- left column -->
      <div class="col-md-12">

        <div class="box box-primary">

          <div class="box-header with-border">

          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form enctype="multipart/form-data" method="POST">
            <div class="box-body">
               <input type="hidden" class="form-control" name="banner_type" value="<?= @$banner_type; ?>" >
                <?php if(isset($_REQUEST['banner_id'])){ ?>
                    <input type="hidden" class="form-control" name="id" value="<?= $edit_id; ?>" >
                 <?php } ?>
              <div class="row">
                <div class="col-md-6 form-group">
                  <label for="Offer Type"> Heading : </label>
                  <input type="text" class="form-control" name="title1" value="<?php if(isset($_REQUEST['banner_id'])) { echo $title1; } elseif(isset($_POST['title1'])) { echo $_POST['title1']; } else{ echo ''; } ?>"  >
                </div>
                <div class="col-md-6 form-group">
                  <label for="Offer Price"> Title : </label>
                  <input type="text" class="form-control" name="title2" value="<?php if(isset($_REQUEST['banner_id'])) { echo $title2; } elseif(isset($_POST['title2'])) { echo $_POST['title2']; } else{ echo ''; } ?>"  >
                </div>   
                <!-- <div class="col-md-6 form-group">
                  <label for="Offer Price"> Add Link : </label>
                  <input type="text" class="form-control" name="title3" value="<?php if(isset($_REQUEST['banner_id'])) { echo $title3; } elseif(isset($_POST['title3'])) { echo $_POST['title3']; } else{ echo ''; } ?>" placeholder="http://www.xyz.com/xyz..." >
                </div>  -->
              </div>
                
              <div class="form-group">
                <label for="exampleInputFile">Image For Banner :- <span  style="color: #ea3232">(Width) 71px × (Height) 65px </span> </label> <br />
                  
                <?php if(isset($_REQUEST['banner_id'])){  ?>
                  <input type="hidden" name="image1" value="<?= $image; ?>">
                  <img src="../<?= $image; ?>"  height="100" width="100px"/> <br /> <br />
                <?php }?>
                <input type="file" name="image" size="12"  data-toggle="tooltip"  data-placement="top" title="For Better Result Use Width and Height as Mention Above">  
              </div>  
             <!-- /.box-body -->

             <div class="box-footer" align="center">
              <button type="submit" name="<?php if(isset($_REQUEST['banner_id'])) { echo 'banner_edit'; } elseif(isset($banner_edit1) == 'banner_edit') { echo 'banner_id'; } else{ echo 'submit'; } ?>" value="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>

<?php include('footer.php'); ?>