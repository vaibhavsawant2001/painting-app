<?php
include_once('header.php');

      
// Change Status
if ( isset($_REQUEST['prrun_id']) && isset($_REQUEST['prrun_status']) ) {
    $where_array = array( 'id' => strip_tags($_REQUEST['prrun_id']) );
    $update_array = array('status' => strip_tags($_REQUEST['prrun_status']) );    

    if($model->update("prrunner_gallery", $update_array, $where_array)){
        $succ = 'Status Update';
    }
}
    // delete
elseif (isset($_REQUEST['prrun_del_id'])) {
  
    $where_array = array( 'id' => strip_tags($_REQUEST['prrun_del_id'])  );
    $stmt_del = $model->select('prrunner_gallery',$where_array);
    foreach($stmt_del as $delete_image){
      if(!empty($delete_image['image'])){
            $deleteimage = '../'.$delete_image['image'];
            unlink($deleteimage);
      }
    }
    if($model->delete("prrunner_gallery", $where_array)){
        $succ = 'Delete';
    }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    RunnerUPS
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">RunnerUPS</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center"> 
              <a href="pr_run_add.php" class="btn btn-primary">Add RunnerUPSGallery</a>                
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
                <thead>
              <tr>
                   <th hidden> ID </th> 
                   <th> Title</th>
                    <th> Main Category</th>
                    <th> Sub Category</th>
                    <th> Image </th>
                    <th> Status </th>
                    <th> Edit </th>
                    <th> Delete </th>
                  
              </tr>
              </thead>
              <tbody>
              <?php  
             
                if($datas = $model->singleselect("prrunner_gallery")){ 
               
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $image = $data['image'];
                        $title = $data['title'];
                        $program_cat_id = $data['program_cat_id'];
                        $program_sub_id = $data['program_sub_id'];
                        // $content = $data['content'];
                        // $date = $data['date'];                    
                        $status = $data['status'];     
                        // $ext = pathinfo($pdf, PATHINFO_EXTENSION);               
             ?>
                <tr>
                  <td hidden> <?= $id; ?> </td> 
                  <td> <?= $title; ?> </td> 
                  <td> 
                    <?php 
                    $where_array= array('id'=> $program_cat_id);
                  if($datas1 = $model->select("program_category",$where_array)){ 
               
                    foreach($datas1 as $data1){ 
                        $mtitle = $data1['title'];
                      }
                    echo $mtitle;
                    }
                    ?>
                   </td>

                    <td> 
                    <?php 
                    $where_array= array('id'=> $program_sub_id);
                  if($datas2 = $model->select("program_subcategory",$where_array)){ 
               
                    foreach($datas2 as $data2){ 
                        $stitle = $data2['title'];
                      }
                    echo $stitle;
                    }
                    ?>
                   </td>
                   <td> <img src="../<?= $image; ?>" height="100px" width="120px"> </td>  
                 <td>     
                        <?php if($status == '0') { ?>

                        <a  href="pr_run.php?prrun_status=1&prrun_id=<?= $id; ?> " class="label label-danger"> Denied </a>  

                        <?php } else { ?>

                        <a  href="pr_run.php?prrun_status=0&prrun_id=<?= $id; ?>" class="label label-success">Approved</a>  
                        <?php } ?>
                  </td>

             
                  <td>
                    <a href="pr_run_add.php?prrun_id=<?php echo $id; ?>" class="label label label-warning"> <span class="label label-warning"> Edit </span> </a> 
                  </td>
                  <td>
                      <a href="pr_run.php?prrun_del_id=<?php echo $id; ?>" onclick="return confirm('Are you sure you want to Remove?');"> <span class="label label-danger"> Delete </span> </a> 
                  </td> 
               </tr>
            <?php } } ?>
            </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  
       </div>
     </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>