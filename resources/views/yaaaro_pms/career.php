<?php
include_once('header.php');
    

  // delete
if (isset($_REQUEST['ptr_del_id'])) {
  $where_array = array( 'id' => strip_tags($_REQUEST['ptr_del_id']) );
   if($model->delete("fund_partner", $where_array)){
      $succ = 'Delete';
  }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    Careers
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Careers</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center"> 
                <!-- <a href="volunteer_add.php" class="btn btn-primary">Add foster</a> -->
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
                <thead>
              <tr>
                    <th> Name </th>
                    <th> Email </th>
                    <th> Contact No. </th>            
                    <th> Address</th>   
                    <th> Key Skill</th>                   
                    <th> Experience </th>                 
                    <th> Designation</th>                 
                    <th> CTC</th>                 
                    <th> CV </th>
                    <th> </th>
              </tr>
              </thead>
              <tbody>
             <?php  
                if($datas = $model->singleselect("career")){ 
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $name = $data['name'];
                        $email = $data['email'];
                        $phone = $data['phone'];
                        $address = $data['address'];            
                        $key_skills = $data['key_skills'];            
                        $experience = $data['experience'];            
                        $designation = $data['designation'];            
                        $cur_ctc = $data['cur_ctc'];            
                        $exp_ctc = $data['exp_ctc'];            
                        $image = $data['image'];                       
             ?>
                <tr>
                  <td> <?= $name; ?> </td> 
                  <td> <?= $email; ?> </td> 
                  <td> <?= $phone; ?> </td> 
                  <td> <?= $address; ?> </td>                   
                  <td> <?= $key_skills; ?> </td>                   
                  <td> <?= $experience; ?> </td>               
                  <td> <?= $designation; ?> </td>                    
                  <td> Current CTC:<?= $cur_ctc; ?> <br>Expected CTC: <?= $exp_ctc; ?> </td>                   
                  <td> <a href='download.php?name=<?= $image; ?>' class='label label-warning'>Download</a> </td>                       
                  <td>
                      <a href="partnership.php?ptr_del_id=<?php echo $id; ?>" onclick="return confirm('Are you sure you want to Remove?');"> <span class="label label-danger"> Delete </span> </a> 
                  </td> 
               </tr>
            <?php } } ?>
            </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  
     

        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>