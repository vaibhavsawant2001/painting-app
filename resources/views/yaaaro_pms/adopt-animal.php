<?php
include_once('header.php');
    
if(isset($_REQUEST['animal'])){
    $animal = $_REQUEST['animal'];
}
// Change Status
if ( isset($_REQUEST['animal_id']) && isset($_REQUEST['animal_status']) ) {
    $where_array = array( 'id' => strip_tags($_REQUEST['animal_id']) );
    $update_array = array(  'status' => strip_tags($_REQUEST['animal_status']) );    

    if($model->update("adopt", $update_array, $where_array)){
        $succ = 'Status Update';
    }
}
    // delete
elseif (isset($_REQUEST['animal_del_id'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['animal_del_id'])  );
    $stmt_del = $model->select('adopt',$where_array);
    foreach($stmt_del as $delete_image){
      if(!empty($delete_image['image'])){
            $deleteimage = '../'.$delete_image['image'];
            unlink($deleteimage);
      }
    }
  
    if($model->delete("adopt", $where_array)){
        $succ = 'Delete';
    }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    Adopt <?= $animal; ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="adopt.php"><i class="fa fa-dashboard"></i> Adopt</a></li>
      <li class="active">Adopt <?= $animal; ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center"> 
                <a href="adopt-animal_add.php?animal=<?= $animal; ?>" class="btn btn-primary">Add <?= $animal; ?></a>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
                <thead>
              <tr>
                    <th> Image </th>
                    <th> Name</th>
                    <th> Age</th>
                    <th> Gender </th>
                    <th> Color</th>
                    <th> Weight</th>
                    <th> Price</th>
                    <th> </th>      
                    <th> </th>
                    <th> </th>
              </tr>
              </thead>
              <tbody>
             <?php  
                $where_array = array(
                    'type' => $animal
                );
                if($datas = $model->select("adopt", $where_array)){ 
                  $num = count($datas);
                  // echo $num; 
                  $i= '1';
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $image = $data['image'];
                        $name = $data['name'];
                        $age = $data['age'];
                        $gender = $data['gender'];
                        $color = $data['color'];
                        $weight = $data['weight'];
                        $price = $data['price'];                    
                        $status = $data['status'];                    
             ?>
                <tr>
                  <td> <img src="../<?= $image; ?>" height="100px" width="120px"> </td>  
                  <td> <?= $name; ?> </td> 
                  <td> <?= $age; ?> </td>        
                  <td> <?= $gender; ?> </td>        
                  <td> <?= $color; ?> </td>        
                  <td> <?= $weight; ?> </td>        
                  <td> <?= $price; ?> </td>        
                  <td>     
                        <?php if($status == '0') { ?>

                        <a  href="adopt-animal.php?animal=<?= $animal; ?>&animal_status=1&animal_id=<?= $id; ?> " class="label label-danger"> Denied </a>  

                        <?php } else { ?>

                        <a  href="adopt-animal.php?animal=<?= $animal; ?>&animal_status=0&animal_id=<?= $id; ?>" class="label label-success">Approved</a>  
                        <?php } ?>
                  </td>
             
                  <td>
                      <a href="adopt-animal_add.php?animal=<?= $animal; ?>&animal_id=<?php echo $id; ?>"> <span class="label label-warning"> Edit </span> </a> 
                  </td>
                  <td>
                      <a href="adopt-animal.php?animal=<?= $animal; ?>&animal_del_id=<?php echo $id; ?>" onclick="return confirm('Are you sure you want to Remove?');"> <span class="label label-danger"> Delete </span> </a> 
                  </td> 
               </tr>
            <?php $i++;} } ?>
            </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  
        <!-- <script type="text/javascript">
        var count= <?php //echo $num; ?>
            for(var i=1 ;i<=count; i++;){
            var longText = $('#discription'+i);
            longText.text(longText.text().substr(0, 500));
            }
        </script>   -->

        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>