<?php

// ob_start();
// error_reporting(E_ALL);

include_once('header.php');
error_reporting(E_ERROR | E_PARSE);

// for edit
if(isset($_REQUEST['founders_id'])){    
  $edit_id = strip_tags($_REQUEST['founders_id']);
  $where = array( 'id' => $edit_id );
  if($others = $model->select('founders',$where)){
      foreach($others as $other){   
          $id = $other['id'];
          $image = $other['image'];
          $name = $other['name'];
          $degree = $other['degree'];
          $heading = $other['heading'];
          $content = $other['content'];
          $address = $other['address'];
          $phone = $other['phone'];
          $email = $other['email'];
          $fb = $other['fb'];
          $twitter = $other['twitter'];
          $insta = $other['insta'];
          $linkedin = $other['linkedin'];
      }
  }
}

if(isset($_POST['founders_edit'])){
  $founders_edit1 = 'founders_edit'; 
  $edit_id = $_POST['id'];
  $page = $_POST['page'];
  

  $image1 = strip_tags($_POST['image1']);
  $image2 = $_FILES['image']['name'];
    if (empty($image2)) {
      $file = $image1;
    }
    else{  
      // for image replace
      $stmt_del = $model->select('founders',$where);
      foreach($stmt_del as $delete_image){
        $deleteimage = '../'.$delete_image['image'];
        unlink($deleteimage);
      }

    $about_file = $_FILES['image']['name'];
    $target_dir1 = '../uploads/founders/';
    $target_dir = 'uploads/founders/';
    $newfilename = date('dmYHis').str_replace(" ", "", basename($about_file));
    $file1 = $target_dir1 . basename($newfilename);
    $file = $target_dir . basename($newfilename);
    $uploadOk = 1;
    $temp_file = $_FILES["image"]["tmp_name"];
  }

  $where_other = array( 
    'id' => $edit_id
  );

  $update_array = array(
          'image' => $file,
          'name' => addslashes(strip_tags(htmlentities($_POST['name']))),
          'degree' => addslashes(strip_tags(htmlentities($_POST['degree']))),
          'phone' => addslashes(strip_tags(htmlentities($_POST['phone']))),
          'email' => addslashes(strip_tags(htmlentities($_POST['email']))),
          'heading' => addslashes(strip_tags(htmlentities($_POST['heading']))),
          'content' => $_POST['content'],
          'fb' => addslashes(strip_tags(htmlentities($_POST['fb']))),
          'twitter' => addslashes(strip_tags(htmlentities($_POST['twitter']))),
          'insta' => addslashes(strip_tags(htmlentities($_POST['insta']))),
          'linkedin' => addslashes(strip_tags(htmlentities($_POST['linkedin']))),
        //   'date' => $todayDate
  );
  if($model->update("founders", $update_array, $where_other)){
    move_uploaded_file($temp_file, $file1);
    if(!empty($page)){
     $model->url($page.'.php?succ=Update&founders_id='.$edit_id);
    }else{
      $model->url('founders.php?succ=Update&founders_id='.$edit_id);
    }
  }else{
    if(!empty($page)){
      $model->url($page.'.php?msg&founders_id='.$edit_id);
    }else{
      $model->url('founders_add.php?msg&founders_id='.$edit_id);
    }
  }
}

// for insert
if(isset($_POST['submit'])){

   
      $about_file = $_FILES['image']['name'];
        if(!empty($about_file)){
        $target_dir1 = '../uploads/founders/';
        $target_dir = 'uploads/founders/';
        $newfilename = date('dmYHis').str_replace(" ", "", basename($about_file));
        $file1 = $target_dir1 . basename($newfilename);
        $file = $target_dir . basename($newfilename);
        $uploadOk = 1;
        $temp_file = $_FILES["image"]["tmp_name"];
      }else{
        $file1 = '';
        $file = '';
      }    
   
      $insert_array = array(
        'image' => $file,
        'name' => addslashes(strip_tags(htmlentities($_POST['name']))),
        'degree' => addslashes(strip_tags(htmlentities($_POST['degree']))),
        'phone' => addslashes(strip_tags(htmlentities($_POST['phone']))),
        'email' => addslashes(strip_tags(htmlentities($_POST['email']))),
        'heading' => addslashes(strip_tags(htmlentities($_POST['heading']))),
        'content' => $_POST['content'],
        'fb' => addslashes(strip_tags(htmlentities($_POST['fb']))),
        'twitter' => addslashes(strip_tags(htmlentities($_POST['twitter']))),
        'insta' => addslashes(strip_tags(htmlentities($_POST['insta']))),
        'linkedin' => addslashes(strip_tags(htmlentities($_POST['linkedin']))),
        'date' => $todayDate,
        'status' => '1'
      );
      if($model->insert("founders",$insert_array)){
            move_uploaded_file($temp_file, $file1);
            $model->url('founders.php?succ');
      }
      else
          $msg="faild";
    }  

?>

<script type="text/javascript" src="ckeditor/ckeditor.js">
</script>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add Founders
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="founders.php">Founders</a></li>
      <li class="active">Add Founders</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">

        <div class="box box-primary">

          <div class="box-header with-border">
            <?php if (isset($_REQUEST['fail'])) {
              echo '<div class="alert alert-danger alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              Something Went Wrong....
              </div>';
            } ?>

          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form enctype="multipart/form-data" method="POST">
            <div class="box-body">
               <!-- <?php if(isset($_REQUEST['page'])){ ?>
                <input type="hidden" class="form-control" name="page" value="<?= $_REQUEST['page']; ?>" >
               <?php } ?> -->
               <?php if(isset($_REQUEST['founders_id'])){ ?>
                    <input type="hidden" class="form-control" name="id" value="<?= $edit_id; ?>" >
                 <?php } ?>

              <div class="form-group">
                <label for="Mata-Tags"> Name : </label>
                <input type="text" class="form-control" name="name" value="<?php if(isset($_REQUEST['founders_id'])) { echo $name; } elseif(isset($_POST['name'])) { echo $_POST['name']; } else{ echo ''; } ?>">
              </div> 
            
              <div class="form-group">
                <label for="Service Name"> Degree : </label>
                <input type="text" class="form-control" name="degree" value="<?php if(isset($_REQUEST['founders_id'])) { echo $degree; } elseif(isset($_POST['degree'])) { echo $_POST['degree']; } else{ echo ''; } ?>">
              </div> 
              <div class="form-group">
                <label for="Mata-Tags"> Conact No. : </label>
                <input type="number" class="form-control" name="phone" value="<?php if(isset($_REQUEST['founders_id'])) { echo $phone; } elseif(isset($_POST['phone'])) { echo $_POST['phone']; } else{ echo ''; } ?>">
              </div> 
              <div class="form-group">
                <label for="Mata-Tags"> email ID : </label>
                <input type="email" class="form-control" name="email" value="<?php if(isset($_REQUEST['founders_id'])) { echo $email; } elseif(isset($_POST['email'])) { echo $_POST['email']; } else{ echo ''; } ?>">
              </div> 

              <div class="form-group">
                <label for="Subject"> Short Discription : </label>
                <textarea class="form-control" rows="4" name="heading" id="desc1"><?php if(isset($_REQUEST['founders_id'])) { echo $heading; } elseif(isset($_POST['heading'])) { echo $_POST['heading']; } else{ echo ''; } ?></textarea>
              </div>
              <div class="form-group">
                <label for="Contant"> Content : </label>
                <textarea class="form-control" rows="4" name="content" id="editor"><?php if(isset($_REQUEST['founders_id'])) { echo $content; } elseif(isset($_POST['content'])) { echo $_POST['content']; } else{ echo ''; } ?></textarea>
                <!-- <script>CKEDITOR.replace( 'content' );</script> -->
              </div>
              <div class="form-group">
                <label for="exampleInputFile">Image :- <span  style="color: #ea3232">(Width) 270px × (Height) 276px </span> </label> <br />
                  
               <?php if(isset($_REQUEST['founders_id'])){ ?> 
                  <input type="hidden" name="image1" value="<?= $image; ?>">
                   <img src="../<?= $image; ?>"  height="100" width="100px"/> <br /> <br />
               <?php }?>

                <input type="file" name="image" size="12"  data-toggle="tooltip"  data-placement="top" title="For Better Result Use Width and Height as Mention Above">  
              </div>
              <div class="form-group">
              <hr><h2 class="text-center">Social Media</h2><hr>
              </div>
              <div class="form-group col-md-3 col-sm-12">
                <label for="Mata-Tags"> Facebook : </label>
                <input type="text" class="form-control" name="fb" value="<?php if(isset($_REQUEST['founders_id'])) { echo $fb; } elseif(isset($_POST['fb'])) { echo $_POST['fb']; } else{ echo ''; } ?>" placeholder="https://www.facebook.com/xyz">
              </div> 
              <div class="form-group col-md-3 col-sm-12">
                <label for="Mata-Tags"> Twitter : </label>
                <input type="text" class="form-control" name="twitter" value="<?php if(isset($_REQUEST['founders_id'])) { echo $twitter; } elseif(isset($_POST['twitter'])) { echo $_POST['twitter']; } else{ echo ''; } ?>" placeholder="https://www.twitter.com/xyz">
              </div> 
              <div class="form-group col-md-3 col-sm-12">
                <label for="Mata-Tags"> Instagram : </label>
                <input type="text" class="form-control" name="insta" value="<?php if(isset($_REQUEST['founders_id'])) { echo $insta; } elseif(isset($_POST['insta'])) { echo $_POST['insta']; } else{ echo ''; } ?>" placeholder="https://www.instagram.com/xyz">
              </div> 
              <div class="form-group col-md-3 col-sm-12">
                <label for="Mata-Tags"> Linkedin : </label>
                <input type="text" class="form-control" name="linkedin" value="<?php if(isset($_REQUEST['founders_id'])) { echo $linkedin; } elseif(isset($_POST['linkedin'])) { echo $_POST['linkedin']; } else{ echo ''; } ?>" placeholder="https://www.linkedin.com/xyz">
              </div> 

            </div>


             <!-- /.box-body -->

             <div class="box-footer" align="center">
              <button type="submit" name="<?php if(isset($_REQUEST['founders_id'])) { echo 'founders_edit'; } elseif(isset($founders_edit1) == 'founders_edit') { echo 'founders_id'; } else{ echo 'submit'; } ?>" value="submit" class="btn btn-primary ">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>

<?php include('footer.php'); ?>