<?php
include_once('header.php');
    
// Change Status
if ( isset($_REQUEST['event_id']) && isset($_REQUEST['event_status']) ) {
    $where_array = array( 'id' => strip_tags($_REQUEST['event_id']) );
    $update_array = array(  'status' => strip_tags($_REQUEST['event_status']) );    

    if($model->update("events", $update_array, $where_array)){
        $succ = 'Status Update';
    }
}
    // delete
elseif (isset($_REQUEST['event_del_id'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['event_del_id'])  );
    $stmt_del = $model->select('events',$where_array);
    foreach($stmt_del as $delete_image){
      if(!empty($delete_image['image'])){
            $deleteimage = '../'.$delete_image['image'];
            unlink($deleteimage);
      }
    }
  
    if($model->delete("events", $where_array)){
        $succ = 'Delete';
    }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    events
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">events</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center"> 
                <a href="event_add.php" class="btn btn-primary">Add Event</a>
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
                <thead>
              <tr>
                    <th hidden> ID </th> 
                    <th> Image </th>
                    <th> Name</th>
                    <th> Date</th>
                    <th> Time</th>
                    <th> Location</th>
                    <th> Content</th>
                    <th> </th>      
                    <th> </th>
                    <th> </th>
              </tr>
              </thead>
              <tbody>
             <?php  
                if($datas = $model->singleselect("events")){ 
                  $num = count($datas);
                  // echo $num; 
                  $i= '1';
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $image = $data['image'];
                        $title = $data['title'];
                        $edate = $data['edate'];
                        $etime = $data['etime'];
                        $elocation = $data['elocation'];
                        $description = $data['description'];
                        $status = $data['status'];                    
             ?>
                <tr>
                  <td hidden> <?= $id; ?> </td> 
                  <td> <img src="../<?= $image; ?>" height="100px" width="120px"> </td>  
                  <td> <?= $title; ?> </td> 
                  <td> <?= $edate; ?> </td> 
                  <td> <?= date('h:i A', strtotime($etime)); ?> </td> 
                  <td> <?= $elocation; ?> </td> 
                  <td> <?= $description; ?> </td>        
                  <td>     
                        <?php if($status == '0') { ?>

                        <a  href="events.php?event_status=1&event_id=<?= $id; ?> " class="label label-danger"> Denied </a>  

                        <?php } else { ?>

                        <a  href="events.php?event_status=0&event_id=<?= $id; ?>" class="label label-success">Approved</a>  
                        <?php } ?>
                  </td>
             
                  <td>
                      <a href="event_add.php?event_id=<?php echo $id; ?>"> <span class="label label-warning"> Edit </span> </a> 
                  </td>
                  <td>
                      <a href="events.php?event_del_id=<?php echo $id; ?>" onclick="return confirm('Are you sure you want to Remove?');"> <span class="label label-danger"> Delete </span> </a> 
                  </td> 
               </tr>
            <?php $i++;} } ?>
            </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  
        <!-- <script type="text/javascript">
        var count= <?php //echo $num; ?>
            for(var i=1 ;i<=count; i++;){
            var longText = $('#discription'+i);
            longText.text(longText.text().substr(0, 500));
            }
        </script>   -->

        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>