<?php
include_once('header.php');

      
// Change Status
if ( isset($_REQUEST['cat_id']) && isset($_REQUEST['cat_status']) ) {
    $where_array = array( 'id' => strip_tags($_REQUEST['cat_id']) );
    $update_array = array(  'status' => strip_tags($_REQUEST['cat_status']) );    

    if($model->update("program_category", $update_array, $where_array)){
        $succ = 'Status Update';
    }
}
    // delete
elseif (isset($_REQUEST['cat_del_id'])) {
    //del sub
    // $where_sub = array( 'sub_cat_id' => strip_tags($_REQUEST['blog_del_id'])  );
    // $stmt_del11 = $model->select('blogs',$where_sub);
    // foreach($stmt_del11 as $delete_image1){
    //   if(!empty($delete_image1['image'])){
    //         $deleteimage1 = '../'.$delete_image1['image'];
    //         unlink($deleteimage1);
    //   }
    // }
    // $model->delete("blogs", $where_sub);
   //main
    $where_array = array( 'id' => strip_tags($_REQUEST['cat_del_id']));
    $stmt_del = $model->select('program_category',$where_array);
    foreach($stmt_del as $delete_image){
      if(!empty($delete_image['image'])){
            $deleteimage = '../'.$delete_image['image'];
             $deletefile = '../'.$delete_image['file'];
            unlink($deleteimage);
            unlink($deletefile);
      }
     }
    if($model->delete("program_category", $where_array)){
        $succ = 'Delete';
    }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    Program Category
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">  Program Category</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center"> 
              <a href="prog_cat_add.php" class="btn btn-primary">Add Program Category</a>                
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
                <thead>
              <tr>
                    <th hidden> ID </th> 
                    <th> Image </th>
                    <th> Title</th>
                    <!-- <th> Subject</th> -->
                    <th> Status </th>
                    <th> Edit </th>
                    <th> Delete </th>
            </tr>
              </thead>
              <tbody>
             <?php  
            if($datas = $model->singleselect("program_category")){ 
                  $num = count($datas);
                  // echo $num; 
                  $i= '1';
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $image = $data['image'];
                        $title = $data['title'];
                        // $subject = $data['subject'];
                        // $content = $data['content'];
                        // $date = $data['date'];                    
                        $status = $data['status'];     
                        $ext = pathinfo($pdf, PATHINFO_EXTENSION);               
             ?>
                <tr>
                  <td hidden> <?= $id; ?> </td> 
                  <td> <img src="../<?= $image; ?>" height="100px" width="120px"> </td> 
                  <td> <?= $title; ?> </td> 
                <!--   <td> <?= $subject; ?> </td>  --> 
               <!--   <td>
                    <a href="prog_cat.php?cat=<?php echo $id; ?>"> <span class="label label-warning">Add Category  </span> </a> 
                  </td> -->
                                     
                  <td>     
                        <?php if($status == '0') { ?>

                        <a  href="prog_cat.php?cat_status=1&cat_id=<?= $id; ?> " class="label label-danger"> Denied </a>  

                        <?php } else { ?>

                        <a  href="prog_cat.php?cat_status=0&cat_id=<?= $id; ?>" class="label label-success">Approved</a>  
                        <?php } ?>
                  </td>

             
                  <td>
                    <a href="prog_cat_add.php?cat_id=<?php echo $id; ?>"> <span class="label label-warning"> Edit </span> </a> 
                  </td>
                  <td>
                      <a href="prog_cat.php?cat_del_id=<?php echo $id; ?>" onclick="return confirm('Are you sure you want to Remove?');"> <span class="label label-danger"> Delete </span> </a> 
                  </td> 
               </tr>
            <?php $i++;} } ?>
            </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  
        <!-- <script type="text/javascript">
        var count= <?php //echo $num; ?>
            for(var i=1 ;i<=count; i++;){
            var longText = $('#discription'+i);
            longText.text(longText.text().substr(0, 500));
            }
        </script>   -->

        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>