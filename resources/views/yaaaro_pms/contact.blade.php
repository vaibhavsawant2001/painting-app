<!-- resources/views/your-blade-file.blade.php -->
@include('yaaaro_pms/head')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Subscribe Users
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"> Subscribe Users</li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-body table-responsive no-padding">
            <table id="client_master" class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th hidden> ID </th>
                  <th> Name </th>
                  <th> Email Id </th>
                  <th> Message </th>
                  <th> Date </th>
                  <th> Operation </th>
                </tr>
              </thead>
              <tbody>
                @foreach($contact as $contact)
                <tr>
                  <td hidden>{{$contact->id}}</td>
                  <td>{{$contact->name}} </td>
                  <td>{{$contact->email}} </td>
                  <td>{{$contact->message}}</td>
                  <td>{{$contact->created_at}}</td>
                  <td>
                  <td>
                    <a href="{{$contact->id}}" class="delete-contact" data-contact-id="{{$contact->id}}">
                      <span class="btn btn-danger">Delete Contact</span>
                    </a>
                  </td>

                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
@include('yaaaro_pms/footer')
<script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
<script>
  $(document).ready(function() {
    $('.delete-contact').on('click', function(e) {
      e.preventDefault();

      var contactId = $(this).data('contact-id');
      var deleteUrl = '/api/contact/' + contactId;

      $.ajax({
        type: 'DELETE',
        url: deleteUrl,
        data: {
          "_token": "{{ csrf_token() }}"
        },
        success: function(data) {
          // Handle success, e.g., remove the row from the table
          alert('Contact deleted successfully!');
          location.reload(); // Reload the page or update the table as needed
        },
        error: function(xhr, status, error) {
          console.log('Error:', xhr.responseText);
          alert('Failed to delete contact.');
        }
      });
    });
  });
</script>