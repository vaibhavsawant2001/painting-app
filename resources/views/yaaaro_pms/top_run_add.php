<?php

// ob_start();
// error_reporting(E_ALL);
// error_reporting(-1);

include_once('header.php');


// for edit
if(isset($_REQUEST['run_id'])){    
  $edit_id = strip_tags($_REQUEST['run_id']);
  $where = array( 'id' => $edit_id );
  if($others = $model->select('runner',$where)){
      foreach($others as $other){   
          $id = $other['id'];
          $image = $other['image'];
          $title = $other['title'];
          // $subject = $other['subject'];
          // $content = $other['content'];
          $program_cat_id = $other['program_cat_id'];
      }
  }
}

if(isset($_POST['run_edit'])){
  $run_edit1 = 'run_edit'; 
  $edit_id = $_POST['id'];

  $image1 = strip_tags($_POST['image1']);
  $image2 = $_FILES['image']['name'];
    if (empty($image2)) {
      $file = $image1;
    }
    else{  
      // for image replace
      $where = array( 'id' => $edit_id );
      $stmt_del = $model->select('runner',$where);
      foreach($stmt_del as $delete_image){
        $deleteimage = '../'.$delete_image['image'];
        unlink($deleteimage);
      }

    $about_file = $_FILES['image']['name'];
    $target_dir1 = '../uploads/program/';
    $target_dir = 'uploads/program/';
    $newfilename = date('dmYHis').str_replace(" ", "", basename($about_file));
    $file1 = $target_dir1 . basename($newfilename);
    $file = $target_dir . basename($newfilename);
    $uploadOk = 1;
    $temp_file = $_FILES["image"]["tmp_name"];
  }

  $where_other = array( 
    'id' => $edit_id
  );

    $image = $file;
    $title = addslashes(strip_tags(htmlentities($_POST['title'])));
    $program_cat_id = addslashes(strip_tags(htmlentities($_POST['program_cat_id'])));
    // $subject = addslashes(strip_tags(htmlentities($_POST['subject'])));
    // $content = addslashes($_POST['content']);
    $date = $todayDate;

  $update_array = array(
    'image' => $file,
    'title' => $title,
    // 'program_cat_id' => $program_cat_id,
    // 'content' => $content,
    'date' => $todayDate,
  );
  if($model->update("runner", $update_array, $where_other)){
    move_uploaded_file($temp_file, $file1);
    $model->url('top_run.php?succ=Update');
  }else{
      $model->url('top_run_add.php?scat_id='.$edit_id);
  }
}
// for insert
if(isset($_POST['submit'])){

    $about_file = $_FILES['image']['name'];
    if(!empty($about_file)){
      $target_dir1 = '../uploads/program/';
      $target_dir = 'uploads/program/';
      $newfilename = date('dmYHis').str_replace(" ", "", basename($about_file));
      $file1 = $target_dir1 . basename($newfilename);
      $file = $target_dir . basename($newfilename);
      $uploadOk = 1;
      $temp_file = $_FILES["image"]["tmp_name"];
    }else{
      $file1 = '';
      $file ='';
    }
  
    $image = $file;
    $title = addslashes(strip_tags(htmlentities($_POST['title'])));
    $program_cat_id = addslashes(strip_tags(htmlentities($_POST['program_cat_id'])));
    $date = $todayDate;
    $status = '1';

  $insert_array = array(
      'image' => $file,
      // 'image_title' => $image_title,
      'title' => $title,
      // 'program_cat_id' => $program_cat_id,
      'date' => $todayDate,
      'status' => '1'
  );
  if($model->insert("runner",$insert_array)){
        move_uploaded_file($temp_file, $file1);
        $model->url('top_run.php?succ');
  }
  else
      $msg="faild";
}  

?>

<script type="text/javascript" src="ckeditor/ckeditor.js">
</script>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add Subcategory
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="top_run.php">Toprunner</a></li>
      <li class="active">Add Toprunner</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">

        <div class="box box-primary">

          <div class="box-header with-border">
            <?php if (isset($_REQUEST['fail'])) {
              echo '<div class="alert alert-danger alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              Something Went Wrong....
              </div>';
            } ?>

          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form enctype="multipart/form-data" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">
            <div class="box-body">
               <?php if(isset($_REQUEST['run_id'])){ ?>
                    <input type="hidden" class="form-control" name="id" value="<?= $edit_id; ?>" >
                 <?php } ?>
              
               <div class="row">
                <div class="form-group col-md-6">
                  <label for=" Name"> Name : </label>
                  <input type="text" class="form-control" name="title" required value="<?php if(isset($_REQUEST['run_id'])) { echo $title; } elseif(isset($_POST['title'])) { echo $_POST['title']; } else{ echo ''; } ?>">
                </div>


             <!--  <div class="form-group col-md-12">
                <label for="Contant"> Content : </label>
                <textarea class="form-control" rows="4" name="content" id="editor"><?php if(isset($_REQUEST['run_id'])) { echo $content; } elseif(isset($_POST['content'])) { echo $_POST['content']; } else{ echo ''; } ?></textarea>
             </div> --> 

              <div class="form-group col-md-12">
                <label for="exampleInputFile">File :- </label> <br />
                  
               <?php if(isset($_REQUEST['run_id'])){ ?> 
                  <input type="hidden" name="image1" value="<?= $image; ?>">
                  
               <?php echo $image; }?>

                <input type="file" name="image" size="12"  data-toggle="tooltip"  data-placement="top" title="For Better Result Use Width and Height as Mention Above">  
              </div>  
            </div>


             <!-- /.box-body -->

             <div class="box-footer" align="center">
              <button type="submit" name="<?php if(isset($_REQUEST['run_id'])) { echo 'run_edit'; } elseif(isset($run_edit1) == 'run_edit') { echo 'run_id'; } else{ echo 'submit'; } ?>" value="submit" class="btn btn-primary ">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>

<?php include('footer.php'); ?>