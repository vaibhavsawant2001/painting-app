<?php include('header.php'); 

  // for update

  if(isset($_REQUEST['regist_edit'])){

    
    $edit_id = strip_tags($_REQUEST['regist_edit']);
    $where = array( 'id' => $edit_id );
    if($others = $model->select('regist',$where)){
        foreach($others as $other){   
          $logo = $other['logo'];
            $head1 = $other['head1'];
            $head2 = $other['head2'];
            $head3 = $other['head3'];
          $rule1 = $other['rule1'];
          $rule2 = $other['rule2'];
          $rule3 = $other['rule3'];
          $rule4 = $other['rule4'];
          $rule5 = $other['rule5'];
          $rule6 = $other['rule6'];
          $rule7 = $other['rule7'];
        }
    }
  }

  if(isset($_POST['submit'])){
    $where = array( 'id' => $_POST['id']);

    $logo1 = strip_tags($_POST['logo1']);
    $logo2 = $_FILES['logo']['name'];
    if (empty($logo2)) {
      $file = $logo1;
    }
    else{

      $stmt_del = $model->select('regist',$where);
      foreach($stmt_del as $delete_image){
        $deleteimage = '../'.$delete_image['logo'];
        unlink($deleteimage);
      }
      $about_file =   $_FILES['logo']['name'];
      $target_dir1 = '../uploads/';
      $target_dir = 'uploads/';
      $newfilename = date('dmYHis').str_replace(" ", "", basename($about_file));
      $file1 = $target_dir1 . basename($newfilename);
      $file = $target_dir . basename($newfilename);
      $uploadOk = 1;
      $temp_file = $_FILES["logo"]["tmp_name"];
    }
    

    $where = array( 'id' => $_POST['id']);
    $update_array = array(
        'logo' => $file,
        'head1' => addslashes(strip_tags(htmlentities($_POST['head1']))),
        'head2' => addslashes(strip_tags(htmlentities($_POST['head2']))),
        'head3' => addslashes(strip_tags(htmlentities($_POST['head3']))),
        'rule1' => addslashes(strip_tags(htmlentities($_POST['rule1']))),
        'rule2' => addslashes(strip_tags(htmlentities($_POST['rule2']))),
        'rule3' => addslashes(strip_tags(htmlentities($_POST['rule3']))),
        'rule4' => addslashes(strip_tags(htmlentities($_POST['rule4']))),
        'rule5' => addslashes(strip_tags(htmlentities($_POST['rule5']))),
        'rule6' => addslashes(strip_tags(htmlentities($_POST['rule6']))),
        'rule7' => addslashes(strip_tags(htmlentities($_POST['rule7'])))
    );
    if($model->update("regist",$update_array,$where)){
          move_uploaded_file($temp_file, $file1);
          $model->url('regist.php?succ');
    }
    else
       $msg="faild";
  }
 ?>
<style type="text/css">
  .company-detail{
    padding: 2% 5%;
    font-size: 16px;
  }
  .company-detail .image{
    margin-top: 10%;
  }
  .company-detail .image img{
    width:50%;
  }
  .company-detail .time{
    margin-top: 5%;
  }
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       
    <div class="box">
        <div class="col-md-12">
        <center> <h2>Welcome to Yaaaro Management</h2></center> 
        </div>
        <form action="regist_edit.php" method="post" enctype="multipart/form-data">
        <div class="company-detail">
            <div class="row">
                <div class="col-md-6">
                <input type="hidden" name="id" value= <?= $edit_id; ?> >
                <div class="image">
                <input type="file" name="logo" style="margin-bottom: 4%">
                <input type="hidden" name="logo1" value="<?= $logo; ?>">
                <img src="../<?= $logo; ?>" alt="Company logo" class="img-responsive">
                </div>
                <div class="row">
                    <h3 style="padding-left: 20px;">Register Heading <font size="2">(insert like www.facebook.com?fhshdfk)</font></h3>
                    <div class="col-md-6">
                    <label for="Facebook">Head 1</label>
                    <input type="text" name="head1" class="form-control" value="<?= $head1; ?>">
                    </div>
                    <div class="col-md-6">
                    <label for="Twitter">Head 2</label>
                    <input type="text" name="head2" class="form-control" value="<?= $head2; ?>">
                    </div>
                    <div class="col-md-6">
                    <label for="Instagram">Head 3</label>
                    <input type="text" name="head3" class="form-control" value="<?= $head3; ?>" >
                    </div>
                </div>
                </div>
                <div class="col-md-6">
                <h3>RULES AND REGULATION</h3>
                <p>
                    <label for="Company Name">Rule 1 : </label> 
                    <input type="text" name="rule1" class="form-control" value="<?= $rule1; ?>" >
                </p>
                <p>
                    <label for="Company CEO">Rule 2 : </label> 
                    <input type="text" name="rule2" class="form-control" value="<?= $rule2; ?>" >
                </p>
                <p>
                    <label for="Company Address">Rule 3 : </label> 
                    <input type="text" name="rule3" class="form-control" value="<?= $rule3; ?>"  >
                </p>
                <p>
                    <label for="Company Address">Rule 4 : </label> 
                    <input type="text" name="rule4" class="form-control" value="<?= $rule4; ?>"  >
                </p>
                <p>
                    <label for="Company Address">Rule 5 : </label> 
                    <input type="text" name="rule5" class="form-control" value="<?= $rule5; ?>"  >
                </p>
                <p>
                    <label for="Company Address">Rule 6 : </label> 
                    <input type="text" name="rule6" class="form-control" value="<?= $rule6; ?>"  >
                </p>
                <p>
                    <label for="Company Address">Rule 7 : </label> 
                    <input type="text" name="rule7" class="form-control" value="<?= $rule7; ?>"  >
                </p>
                
                <!-- <p>
                    <label for="Company Address">Company Address (Andheri) : </label> 
                    <input type="text" name="address2" class="form-control" value="<?= $address2; ?>"  >
                </p> -->
                
                </div>
                <div class="col-md-12 " style="text-align: right;">
                <button type="submit" name="submit" class="btn btn-success">Update Detail</button>
                </div>
            </div>
              </form>
       </div>    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  
 <!-- ========================== footer ===================== -->

 <?php
    include_once('footer.php');
?>
