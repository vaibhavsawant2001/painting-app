<?php

// ob_start();
// error_reporting(E_ALL);
// error_reporting(-1);

include_once('header.php');


if(isset($_REQUEST['gallery_id'])){
  $where = array('id' => $_REQUEST['gallery_id']);
  if($datas = $model->select("gallery", $where)){         
      foreach($datas as $data){ 
          $gallery_id = $data['id'];  
          $gallery_title = $data['title'];  
      }
  }   
}

// for edit
if(isset($_REQUEST['vgid'])){    
  $edit_id = strip_tags($_REQUEST['vgid']);
  $where = array( 'id' => $edit_id );
  if($others = $model->select('gallery',$where)){
      foreach($others as $other){   
        $id = $other['id'];
        $title = $other['title'];
        $image = $other['image']; 
      }
  }
}

if(isset($_POST['gallery_edit'])){
  $gallery_edit1 = 'gallery_edit'; 
  $edit_id = $_POST['id'];

  $where_other = array( 
    'id' => $edit_id
  );

  $update_array = array(
      'title' => strip_tags(htmlentities($_POST['title'])),
      'image' => strip_tags(htmlentities($_POST['image'])),
      'date' => $todayDate
  );
  if($model->update("gallery", $update_array, $where_other)){
    move_uploaded_file($temp_file, $file1);
    $model->url('gallery_view.php?succ=Update&gallery_id='.$gallery_id);
  }else{
      $model->url('vgallery_add.php?msg&gallery_id='.$gallery_id);
  }
}

// for insert
if(isset($_POST['submit'])){
  $gallery_id=strip_tags(htmlentities($_POST['gallery_id']));
  $insert_array = array(
      'title' => strip_tags(htmlentities($_POST['title'])),
      'image' => strip_tags(htmlentities($_POST['image'])),
      'gallery_id' => $gallery_id,
      'gallery_type' => 'video',
      'type' => 'sub',
      'date' => $todayDate,
      'status' => '1'
  );
  if($model->insert("gallery", $insert_array)){
        $model->url('gallery_view.php?succ&gallery_id='.$gallery_id);
  }
  else
      $msg="faild";
}  

?>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Add <?= $gallery_title; ?> Video
    </h1>
    <ol class="breadcrumb">
      <li><a href="iadmin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="gallery.php">Gallery</a></li>
      <li><a href="gallery_view.php?gallery_id=<?= $gallery_id; ?>"><?= $gallery_title; ?></a></li>
      <li class="active">Add Images</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 


    <div class="row">
      <!-- left column -->
      <div class="col-md-12">

        <div class="box box-primary">

          <div class="box-header with-border">

          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form enctype="multipart/form-data" method="POST">
            <div class="box-body">
              <?php if(isset($_REQUEST['vgid'])){ ?>
                  <input type="hidden" class="form-control" name="id" value="<?= $edit_id; ?>" >
              <?php } ?>
              <?php if(isset($_REQUEST['gallery_id'])){ ?>
                  <input type="hidden" class="form-control" name="gallery_id" value="<?= $gallery_id; ?>" >
              <?php } ?>
              <div class="row">
                <div class="col-md-12 form-group">
                  <label for="Offer Type"> Title : </label>
                  <input type="text" class="form-control" name="title" value="<?php if(isset($_REQUEST['vgid'])) { echo $title; } elseif(isset($_POST['title'])) { echo $_POST['title']; } else{ echo ''; } ?>"  >
                </div>               
              </div>

              <div class="row">
                <div class="col-md-12 form-group">
                  <label for="Offer Type"> Youtube Video Link : </label>
                  <input type="text" class="form-control" name="image" value="<?php if(isset($_REQUEST['vgid'])) { echo $image; } elseif(isset($_POST['image'])) { echo $_POST['image']; } else{ echo ''; } ?>" placeholder="eg.https://www.youtube.com/embed/tgbNymZ7vqY" >
                </div>               
              </div>
             <!-- /.box-body -->

             <div class="box-footer" align="center">
              <button type="submit" name="<?php if(isset($_REQUEST['vgid'])) { echo 'gallery_edit'; } elseif(isset($gallery_edit1) == 'gallery_edit') { echo 'gallery_id'; } else{ echo 'submit'; } ?>" value="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>

<?php include('footer.php'); ?>