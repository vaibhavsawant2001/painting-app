<?php
include_once('header.php');

      
// Change Status
if ( isset($_REQUEST['storie_id']) && isset($_REQUEST['storie_status']) ) {
    $where_array = array( 'id' => strip_tags($_REQUEST['storie_id']) );
    $update_array = array(  'status' => strip_tags($_REQUEST['storie_status']) );    

    if($model->update("stories", $update_array, $where_array)){
        $succ = 'Status Update';
    }
}
    // delete
elseif (isset($_REQUEST['storie_del_id'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['storie_del_id'])  );
    $stmt_del = $model->select('stories',$where_array);
    foreach($stmt_del as $delete_image){
      if(!empty($delete_image['image'])){
            $deleteimage = '../'.$delete_image['image'];
            unlink($deleteimage);
      }
    }
    if($model->delete("stories", $where_array)){
        $succ = 'Delete';
    }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
    Adoption Stories
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Adoption Stories</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header text-center"> 
              <a href="storie_add.php" class="btn btn-primary">Add stories</a>                
          </div>
          <!-- /.box-header -->
          <div class="box-body table-responsive no-padding">
          <table id="client_master" class="table table-bordered table-striped">
                <thead>
              <tr>
                    <th hidden> ID </th> 
                    <th> Image </th>
                    <th> Title</th>
                    <th> Subject</th>
                    <th> Date </th>      
                    <th> </th>
                    <th> </th>
                    <th> </th>
              </tr>
              </thead>
              <tbody>
             <?php  
            //  $where_array = array(
            //    'status' => '1'
            //  );
                if($datas = $model->singleselect("stories")){ 
                  $num = count($datas);
                  // echo $num; 
                  $i= '1';
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $image = $data['image'];
                        $pdf = $data['pdf'];
                        $title = $data['title'];
                        $subject = $data['subject'];
                        $content = $data['content'];
                        $date = $data['date'];                    
                        $status = $data['status'];                 
             ?>
                <tr>
                  <td hidden> <?= $id; ?> </td> 
                  <td> <img src="../<?= $image; ?>" height="100px" width="120px"> </td>  
                  <td> <?= $title; ?> </td> 
                  <td> <?= $subject; ?> </td>  
                  <td ><?php echo $date; ?></td>
                       
                                     
                  <td>     
                        <?php if($status == '0') { ?>

                        <a  href="stories.php?storie_status=1&storie_id=<?= $id; ?> " class="label label-danger"> Denied </a>  

                        <?php } else { ?>

                        <a  href="stories.php?storie_status=0&storie_id=<?= $id; ?>" class="label label-success">Approved</a>  
                        <?php } ?>
                  </td>
             
                  <td>
                    <a href="storie_add.php?storie_id=<?php echo $id; ?>"> <span class="label label-warning"> Edit </span> </a> 
                 
                  </td>
                  <td>
                      <a href="stories.php?storie_del_id=<?php echo $id; ?>" onclick="return confirm('Are you sure you want to Remove?');"> <span class="label label-danger"> Delete </span> </a> 
                  </td> 
               </tr>
            <?php $i++;} } ?>
            </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  
        <!-- <script type="text/javascript">
        var count= <?php //echo $num; ?>
            for(var i=1 ;i<=count; i++;){
            var longText = $('#discription'+i);
            longText.text(longText.text().substr(0, 500));
            }
        </script>   -->

        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>