<?php

// ob_start();
// error_reporting(E_ALL);
// error_reporting(-1);

include_once('header.php');

// for edit
if(isset($_REQUEST['ach_id'])){    
  $edit_id = strip_tags($_REQUEST['ach_id']);
  $where = array( 'id' => $edit_id );
  if($others = $model->select('achivements',$where)){
      foreach($others as $other){   
          $id = $other['id'];
          $image = $other['image'];
          $name = $other['name'];
          $content = $other['content'];
          // $metatag = $other['metatag'];
      }
  }
}

if(isset($_POST['ach_edit'])){
  $blog_edit1 = 'ach_edit'; 
  $edit_id = $_POST['id'];

  $image1 = strip_tags($_POST['image1']);
  $image2 = $_FILES['image']['name'];
    if (empty($image2)) {
      $file = $image1;
    }
    else{  
      // for image replace
      $where = array( 'id' => $edit_id );
      $stmt_del = $model->select('achivements',$where);
      foreach($stmt_del as $delete_image){
        $deleteimage = '../'.$delete_image['image'];
        unlink($deleteimage);
      }

    $about_file = $_FILES['image']['name'];
    $target_dir1 = '../uploads/achivements/';
    $target_dir = 'uploads/achivements/';
    $newfilename = date('dmYHis').str_replace(" ", "", basename($about_file));
    $file1 = $target_dir1 . basename($newfilename);
    $file = $target_dir . basename($newfilename);
    $uploadOk = 1;
    $temp_file = $_FILES["image"]["tmp_name"];
  }

   $name = addslashes(strip_tags(htmlentities($_POST['name'])));
   $content = addslashes($_POST['content']);

  $where_other = array( 
    'id' => $edit_id
  );
  $update_array = array(
    'image' => $file,
    'name' => $name,
    'content' => $content,
    'date' => $todayDate,
   
  );
  if($model->update("achivements", $update_array, $where_other)){
    move_uploaded_file($temp_file, $file1);
    $model->url('achivements.php?succ');
  }else{
      $model->url('achivements_add.php?ach_id='.$edit_id);
  }
}
// for insert
if(isset($_POST['submit'])){

    $about_file = $_FILES['image']['name'];
    if(!empty($about_file)){
      $target_dir1 = '../uploads/achivements/';
      $target_dir = 'uploads/achivements/';
      $newfilename = date('dmYHis').str_replace(" ", "", basename($about_file));
      $file1 = $target_dir1 . basename($newfilename);
      $file = $target_dir . basename($newfilename);
      $uploadOk = 1;
      $temp_file = $_FILES["image"]["tmp_name"];
    }else{
      $file1 = '';
      $file ='';
    }
  
    $image = $file;
    $name = addslashes(strip_tags(htmlentities($_POST['name'])));
    $content = addslashes($_POST['content']);
    $date = $todayDate;
    $status = '1';

  $insert_array = array(
      'image' => $file,
       'name' => $name,
       'content' => $content,
      'date' => $todayDate,
      'status' => '1',
      
  );
  if($model->insert("achivements",$insert_array)){
        move_uploaded_file($temp_file, $file1);
        $model->url('achivements.php?succ');
  }
  else
      $msg="faild";
}  

?>

<script type="text/javascript" src="ckeditor/ckeditor.js">
</script>


<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Add achivements
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="achivements.php">Achivements</a></li>
      <li class="active">Add Achivements</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 
    <div class="row">
      <!-- left column -->
      <div class="col-md-12">

        <div class="box box-primary">

          <div class="box-header with-border">
            <?php if (isset($_REQUEST['fail'])) {
              echo '<div class="alert alert-danger alert-dismissible">
              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
              Something Went Wrong....
              </div>';
            } ?>

          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form enctype="multipart/form-data" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">
            <div class="box-body">
               <?php if(isset($_REQUEST['ach_id'])){ ?>
                    <input type="hidden" class="form-control" name="id" value="<?= $edit_id; ?>" >
                 <?php } ?>
           
              
                <div class="form-group">
                  <label for="Name">  Name : </label>
                  <input type="text" class="form-control" name="name" required value="<?php if(isset($_REQUEST['ach_id'])) { echo $name; } elseif(isset($_POST['name'])) { echo $_POST['name']; } else{ echo ''; } ?>">
                </div>
              </div>
            
                <div class="form-group">
                  <label for="Contant">Short Content : </label>
                  <textarea class="form-control" rows="4" name="content"><?php if(isset($_REQUEST['ach_id'])) { echo $content; } elseif(isset($_POST['content'])) { echo $_POST['content']; } else{ echo ''; } ?></textarea>
                </div>

              <div class="form-group">
                <label for="exampleInputFile">Image :- </label> <br />
                  
               <?php if(isset($_REQUEST['ach_id'])){ ?> 
                  <input type="hidden" name="image1" value="<?= $image; ?>">
                   <img src="../<?= $image; ?>"  height="100" width="100px"/> <br /> <br />
               <?php }?>

                <input type="file" name="image" size="12"  data-toggle="tooltip"  data-placement="top" title="For Better Result Use Width and Height as Mention Above">  
              </div>  
            </div>


             <!-- /.box-body -->

             <div class="box-footer" align="center">
              <button type="submit" name="<?php if(isset($_REQUEST['ach_id'])) { echo 'ach_edit'; } elseif(isset($ach_edit1) == 'ach_edit') { echo 'ach_id'; } else{ echo 'submit'; } ?>" value="submit" class="btn btn-primary ">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>

<?php include('footer.php'); ?>