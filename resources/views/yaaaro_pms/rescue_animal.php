<?php
include_once('header.php');
    //short page data Change Status
if ( isset($_REQUEST['home_id']) && isset($_REQUEST['home_status']) ) {
  $where_array = array( 'id' => strip_tags($_REQUEST['home_id']) );
  $update_array = array(  'status' => strip_tags($_REQUEST['home_status']) );    

  if($model->update("home", $update_array, $where_array)){
      $succ = 'Status Update';
  }
}
// Change Status
if ( isset($_REQUEST['volunteer_id']) && isset($_REQUEST['volunteer_status']) ) {
    $where_array = array( 'id' => strip_tags($_REQUEST['volunteer_id']) );
    $update_array = array(  'status' => strip_tags($_REQUEST['volunteer_status']) );    

    if($model->update("rescue_animal", $update_array, $where_array)){
        $succ = 'Status Update';
    }
}
    // delete
if (isset($_REQUEST['volunteer_del_id'])) {
    $where_array = array( 'id' => strip_tags($_REQUEST['volunteer_del_id']) );
    // $stmt_del = $model->select('rescue_animal',$where_array);
    // foreach($stmt_del as $delete_image){
    //   if(!empty($delete_image['image'])){
    //     $deleteimage = '../'.$delete_image['image'];
    //     unlink($deleteimage);
    //   }
    // }
    if($model->delete("rescue_animal", $where_array)){
        $succ = 'Delete';
    }
}

?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
        Rescue Animal
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">  Rescue Animal </li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <!-- End Main Content -->
    <!-- Small boxes (Stat box) -->
    <div class="row">
      <div class="col-xs-12">
          <div class="box">
            <table id="datatable" class="table table-bordered table-striped">
                <thead>
                    <tr>
                            <!-- <th> Image </th> -->
                            <th> Name </th>
                            <th> Email </th>
                            <th> Contact No. </th>
                            <th> Animal Name </th>
                            <th> Animal Age </th>
                            <th> Animal Gender </th>
                            <th> Animal Breed </th>
                            <th> Address </th> 
                            <th> Volunteer Names </th>
                            <th> Date </th>
                            <!-- <th> Status </th> -->
                            <th> </th>
                    </tr>
              </thead>
              <tbody>
             <?php  
                if($datas = $model->singleselect("rescue_animal")){ 
                    foreach($datas as $data){ 
                        $id = $data['id'];
                        $v_id = $data['v_id'];
                        $image = $data['image'];
                        $name = $data['name'];
                        $email = $data['email'];
                        $phone = $data['phone'];
                        $animal_type = $data['animal_type'];
                        $animal_name = $data['animal_name'];
                        $animal_age = $data['animal_age'];
                        $animal_gender = $data['animal_gender'];
                        $animal_breed = $data['animal_breed'];
                        $address = $data['address'];
                        $city = $data['city'];
                        $state = $data['state'];
                        $country = $data['country'];
                        $zip_code = $data['zip_code'];
                        $date = $data['date'];                    
                        $status = $data['status'];                    
             ?>
                <tr>
                  <!-- <td> <img src="../<?= $image; ?>" height="100px" width="120px"> </td>   -->
                  <td> <?= $name; ?> </td> 
                  <td> <?= $email; ?> </td> 
                  <td> <?= $phone; ?> </td> 
                  <td> <?= ($animal_type === 'Other')? $animal_name : $animal_type; ?> </td> 
                  <td> <?= $animal_age; ?> </td> 
                  <td> <?= $animal_gender; ?> </td> 
                  <td> <?= $animal_breed; ?> </td> 
                  <td> <?= $address; ?>,<?= $city; ?>,<?= $state; ?>,<?= $country; ?>,<?= $zip_code; ?> </td>  
                  <td> 
                    <?php
                    $v_ids = explode(',', $v_id);
                    $count = count($v_ids);
                    // $vname = '';
                    for($i=0 ; $i<$count; $i++){
                      // echo $v_ids[$i];
                        $vsql ="SELECT `id`,`name`,`phone` FROM `volunteer` WHERE `id` = '$v_ids[$i]'" ;
                        $query = mysqli_query($conn, $vsql);
                        $vros = mysqli_fetch_array($query);
                       echo $vname = ($i+1)."-".$vros['name']."-".$vros['phone']."<br>--------------<br>";
                    }    
                    // echo expload(,)
                    ?> 
                  </td>             
                  <td> <?= $date; ?> </td>             
                                     
                  <!-- <td>     
                        <?php if($status == '0') { ?>

                        <a  href="volunteer.php?volunteer_status=1&volunteer_id=<?= $id; ?> " class="label label-danger"> Denied </a>  

                        <?php } else { ?>

                        <a  href="volunteer.php?volunteer_status=0&volunteer_id=<?= $id; ?>" class="label label-success">Approved</a>  
                        <?php } ?>
                  </td> -->
             
                  <!-- <td>
                      <a href="volunteer_add.php?volunteer_id=<?php echo $id; ?>"> <span class="label label-warning"> Edit </span> </a> 
                  </td> -->
                  <td>
                      <a href="rescue_animal.php?volunteer_del_id=<?php echo $id; ?>" onclick="return confirm('Are you sure you want to Remove?');"> <span class="label label-danger"> Delete </span> </a> 
                  </td> 
               </tr>
            <?php } } ?>
            </tbody>
              </table>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>  
     

        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <!-- /.row (main row) -->
    </section>
    <!-- /.content -->
  </div>


  <?php include('footer.php'); ?>