<!-- Breadcrumb Start -->
<div class="container-fluid mt-4">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-decoration-none text-dark" href="<?= base_url() ?>">Home</a>
				<span class="breadcrumb-item active">Register</span>
			</nav>
		</div>
	</div>
</div>
<!-- Breadcrumb End -->

<!-- Contact Start -->
<div class="container-fluid" style="margin-top:15px ;">
	<h5 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span class="bg-secondary pr-3">Contact
			Us</span></h5>
	<div class="row px-xl-5">
		<div class="col-lg-7 mb-5">
			<div class="contact-form bg-light p-30">
				<?php if ($this->session->flashdata('message_succ') !== null) { ?>
					<div class="col-md-12">
						<div class="alert alert-success">
							<strong>
								<?= $this->session->flashdata('message_succ'); ?>
							</strong>
						</div>
					</div>
				<?php } ?>
				<?php if ($this->session->flashdata('message_fail') !== null) { ?>
					<div class="col-md-12">
						<div class="alert alert-danger">
							<strong>
								<?= $this->session->flashdata('message_fail'); ?>
							</strong>
						</div>
					</div>
				<?php } ?>
				<form name="sentMessage" action="<?= base_url('Site/contactdb'); ?>" method="post" id="contactForm" >
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
						value="<?php echo $this->security->get_csrf_hash(); ?>">
					<div class="control-group mb-3">
						<input type="text" class="form-control" name="name" id="name" placeholder="Your Name" required="required"
							placeholder="Please enter your name" />
					</div>
					<div class="control-group mb-3">
						<input type="email" name="email" class="form-control"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" id="email" placeholder="Your Email" required="required"
							placeholder="Please enter your email" />
					</div>
					<div class="control-group mb-3">
						<textarea class="form-control" name="message" rows="8" id="message" placeholder="Message" required="required"
							placeholder="Please enter your message"></textarea>
					</div>
					<div>
						<button class="btn btn-primary py-2 px-4" type="submit" id="sendMessageButton">Send
							Message</button>
					</div>
				</form>
			</div>
		</div>
		<div class="col-lg-5 mb-5">

			<div class="bg-light p-30 mb-3" style="margin-top:0px;">

				<p class="mb-2"><i class="fa fa-envelope text-primary mr-3"></i>anytimemoney@gmail.com</p>
				<p class="mb-2"><i class="fa fa-phone-alt text-primary mr-3"></i>+91 987654321</p>
			</div>
		</div>
	</div>
</div>
<!-- Contact End -->