<!-- Shop Start -->
<?php 
$user_id = (!empty($this->session->userdata('register_id'))) ? $this->session->userdata('register_id')  : '';
$my_product = $this->db->get_where('my_product', ['product_id'=> $product->id,'user_id'=> $user_id,'status' => '1'], 3)->result();
$my_product_count = count($my_product);
$my_fav_list = $this->db->get_where('fav_list', ['car_id' => $product->id, 'login_id' => $user_id, 'status' => '1'], 3)->result();
$my_fav_list_count = count($my_fav_list);
?>
<!-- Breadcrumb Start -->
<div class="container-fluid mt-4">
    <div class="row px-xl-5">
        <div class="col-12">
            <nav class="breadcrumb bg-light mb-30">
                <a class="breadcrumb-item text-decoration-none text-dark" href="<?= base_url() ?>">Home</a>
                <a class="breadcrumb-item text-decoration-none text-dark" href="<?= base_url('shop') ?>">Shop</a>
                <span class="breadcrumb-item active">Shop Details</span>
            </nav>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->
<!-- Shop Detail Start -->
<div class="container-fluid pb-5">
    <div class="row px-xl-5">
        <div class="col-lg-3 mb-30">
            <div id="product-carousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner bg-light">
                    <div class="carousel-item active">
                        <img class="w-100 h-50" src="<?= base_url(); ?>uploads/img/<?= $product->img??'' ?>"
                            alt="Image">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-9 h-auto mb-30">
            <div class="h-100 bg-light p-30">
                <?php if ($this->session->flashdata('error') !== null) { ?>
                <div class="alert alert-danger">
                    <strong>
                        <?= $this->session->flashdata('error'); ?>
                    </strong>
                </div>
                <?php } ?>

                <?php if ($this->session->flashdata('success') !== null) { ?>
                <div class="alert alert-primary">
                    <strong>
                        <?= $this->session->flashdata('success'); ?>
                    </strong>
                </div>
                <?php } ?>
                <h3>
                    <?= $product->product_name ?? '' ?>
                </h3>
                <h3 class="font-weight-semi-bold mb-4">
                    <i class="fas fa-rupee-sign ml-1"></i>
                    <?= $product->amount??'' ?>
                    <i class="fas fa-thin fa-gem ml-1 text-primary"></i>
                    <?= $product->jems ?? '' ?>
                </h3>
                <?php if (!empty($product->short_desc)) { ?>
                <p class="mb-4"><?=$product->short_desc?></p>
                <?php } ?>
                <?php if (!empty($this->session->userdata('register_id'))) { ?>

                <div class="d-flex align-items-center mb-4 pt-2">
                    <a class="text-decoration-none" href="<?= base_url('checkout/') ?><?= $product->id ?? '' ?>"><button
                            class="btn  btn-warning px-3"><?= ($my_product_count > 0)?'Redeem Now':'Buy Now'?></button></a>
                    <?php if($my_fav_list_count > 0){?>
                    <form action="<?= base_url('Site/fav_delete'); ?>" method="post">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                            value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <input type="hidden" readonly name="product_id" value="<?= $product->id ?? '' ?>">
                        <button class="btn btn-danger  ml-3"><i class="far fa-heart"></i></button>
                    </form>

                    <?php }else {?>
                    <form action="<?=base_url('Site/fav_add');?>" method="post">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                            value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <input type="hidden" readonly name="product_id" value="<?= $product->id??''?>">
                        <button type="submit" class="btn btn-outline border-primary ml-3"><i
                                class="far fa-heart text-primary"></i></button>
                    </form>
                    <?php } ?>
                </div>
                <?php }else{ ?>
                <div class="d-flex align-items-center mb-4 pt-2">
                    <button class="btn btn-primary px-3 disabled" data-toggle="modal" data-target="#myModal">Buy
                        Now</button>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php if (!empty($product->long_desc)) { ?>
    <div class="row px-xl-5">
        <div class="col">
            <div class="bg-light p-30">
                <div class="nav nav-tabs mb-4">
                    <a class="nav-item nav-link text-dark active" data-toggle="tab" href="#tab-pane-1">Description</a>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="tab-pane-1">
                        <h4 class="mb-3">Product Description</h4>
                        <?= $product->long_desc?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
<!-- Shop Detail End -->

<!-- Products Start -->
<div class="container-fluid py-5">
    <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span class="bg-secondary pr-3">You May Also
            Like</span></h2>
    <div class="row px-xl-5">
        <div class="col">
            <div class="owl-carousel related-carousel">
                <?php 
                    foreach($similar_products as $product){
                    $my_product = $this->db->get_where('my_product', ['product_id' => $product->id, 'user_id' => $user_id, 'status' => '1'], 3)->result();
                    $my_product_count = count($my_product);
                ?>
                <div class="product-item bg-light mb-4" style="height:610px">
                    <div class="product-img position-relative overflow-hidden" style="height:452px">
                        <img class="img-fluid w-100" src="<?= base_url() ?>uploads/img/<?= $product->img?>" alt="">
                    </div>
                    <div class="text-center py-4">
                        <a class="h6 text-decoration-none text-truncate"
                            href="<?= base_url('shop-detail/') ?><?= $product->id ?? '' ?>"><?= $product->product_name??'' ?></a>
                        <div class="d-flex align-items-center justify-content-center mt-2">
                            <h5><?= $product->amount ?? '' ?><i class="fas fa-rupee-sign ml-1"></i></h5>
                            <h6 class="text-muted ml-2"><?= $product->jems ?? '' ?><i
                                    class="fas fa-thin fa-gem ml-1 text-primary"></i></h6>
                        </div>
                        <?php if (!empty($this->session->userdata('register_id'))) { ?>
                        <a href="<?= base_url('checkout/') ?><?= $product->id ?? '' ?>">
                            <div class="mb-4 pt-2">
                                <button
                                    class="btn btn-primary px-3"><?= ($my_product_count > 0) ? 'Redeem Now' : 'Buy Now' ?></button>
                            </div>
                        </a>
                        <?php } else { ?>
                        <div class=" mb-4 pt-2">
                            <button class="btn btn-primary px-3 disabled" data-toggle="modal" data-target="#myModal">Buy
                                Now</button>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<!-- Products End -->