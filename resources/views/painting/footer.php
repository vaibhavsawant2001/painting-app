<!-- Modal Login-->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h6>Please Login for futher process</h6>
            </div>
            <div class="modal-footer">
                <a href="<?= base_url('login') ?>"><button type="button" class="btn btn-warning">Login</button></a>
            </div>
        </div>

    </div>
</div>

<!-- Modal Confirmation-->
<div class="modal fade" id="myModalPrice" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <h6>Are you sure, you want to claim the price</h6>
            </div>
            <div class="modal-footer">
                <form method="post" action="<?= base_url('Site/price_add') ?>">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                        value="<?php echo $this->security->get_csrf_hash(); ?>">
                    <input type="hidden" name="game_id" id="product_pop_id" readonly>
                    <button type="submit" class="btn btn-outline-success">Yes</button>
                </form>
                <button type="button" data-dismiss="modal" class="btn btn-outline-danger">No</button>
            </div>
        </div>
    </div>
</div>
<!-- Footer Start -->
<div class="container-fluid bg-dark mb-0 position-sticky" style="top:100vh" >
    <center><img src="<?= base_url() ?>uploads/img/logo_W.png" alt=""
            style="width: 30%; margin-top: 15px; margin-bottom: 15px "></center>
</div>
<!-- Footer End -->

<!-- Back to Top -->
<a href="#" class="btn btn-primary back-to-top"><i class="fa fa-angle-double-up"></i></a>

<!-- JavaScript Libraries -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url() ?>lib/easing/easing.min.js"></script>
<script src="<?= base_url() ?>lib/owlcarousel/owl.carousel.min.js"></script>

<!-- Contact Javascript File -->
<!-- <script src="<? //= base_url() ?>mail/jqBootstrapValidation.min.js"></script> -->
<!-- <script src="<? //= base_url() ?>mail/contact.js"></script> -->

<!-- Template Javascript -->
<script src="<?= base_url() ?>js/main.js"></script>
<script>
    function pop_up_price(product_id) {
        $("#product_pop_id").val(product_id);
    }
</script>
</body>

</html>