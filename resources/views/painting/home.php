<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <title>Home | Any Time Money</title>
    <meta name="description" content="yaaaro" />
    <meta name="keywords" content="website desin and development company in mumbai, web development, web design, seo" />
    <!-- Favicon -->
    <link href="http://127.0.0.1/painting-app/public/uploads/img/favicon.ico" rel="icon">
    <!-- Google Web Fonts -->
    <!-- <link rel="preconnect" href=""> -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <!-- Libraries Stylesheet -->
    <link href="http://127.0.0.1/painting-app/public/lib/animate/animate.min.css" rel="stylesheet">
    <link href="http://127.0.0.1/painting-app/public/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <!-- Customized Bootstrap Stylesheet -->
    <link href="{{ asset('css/painting/css/style.css') }}" rel="stylesheet">

    <style>
        html,
        body {
            height: 100%;
        }
    </style>
</head>

<body>
    <!-- Topbar Start  -->
    <div class="col-lg-0 text-center text-lg-right b-block d-md-none" style="background-color: black; color: white;">
        <div class="d-inline-flex align-items-right">

            <!-- <a href="http://127.0.0.1/painting-app/public/" class="btn px-0" style="color: white;">Home</a>
                <a href="http://127.0.0.1/painting-app/public/" class="btn px-0" style="color: white;">Register</a>
                <a href="http://127.0.0.1/painting-app/public/" class="btn px-0" style="color: white;">Login</a> -->
        </div>
    </div>

    <!-- Topbar End -->

    <!-- Navbar Start -->
    <div class="container-fluid bg-dark mb-0">
        <div class="row px-xl-0">
            <div class="col-lg-3 d-none d-lg-block">
                <img src="http://127.0.0.1/painting-app/public/uploads/img/logo_W.png" alt=""
                    style="width: 200px;margin-top: 5px;">
            </div>
            <div class="col-lg-9">
                <nav class="navbar navbar-expand-lg bg-dark navbar-dark py-2 py-lg-0 px-0">
                    <a href="" class="text-decoration-none d-block d-lg-none">
                        <img src="http://127.0.0.1/painting-app/public/uploads/img/logo_W.png" alt="" style="width: 200px;">

                    </a>
                    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                        <div class="navbar-nav mr-auto py-0">
                            <a href="http://127.0.0.1/painting-app/public/" class="nav-item nav-link text-warning">Home</a>
                            <a href="http://127.0.0.1/painting-app/public/shop" class="nav-item nav-link ">Shop</a>
                            <a href="http://127.0.0.1/painting-app/public/games" class="nav-item nav-link ">Game</a>
                            <a href="http://127.0.0.1/painting-app/public/contact-us" class="nav-item nav-link ">Contact</a>
                            <a href="http://127.0.0.1/painting-app/public/register" class="nav-item nav-link "
                                style="color: white;">Register</a>
                            <a href="http://127.0.0.1/painting-app/public/login" class="nav-item nav-link "
                                style="color: white;">Login</a>
                        </div>



                        <!-- <div class="navbar-nav ml-auto  py-2 d-none d-lg-block">

                            <a href="http://127.0.0.1/painting-app/public/" class="btn text-warning px-2"
                                style="color: white;">Home</a>
                            <a href="http://127.0.0.1/painting-app/public/shop" class="btn  px-2"
                                style="color: white;">Shop</a>
                            <a href="http://127.0.0.1/painting-app/public/games" class="btn  px-2"
                                style="color: white;">Game</a>
                            <a href="http://127.0.0.1/painting-app/public/contact-us" class="btn   px-2"
                                style="color: white;">Contact</a>
                            <a href="http://127.0.0.1/painting-app/public/register" class="btn  px-2"
                                style="color: white;">Register</a>
                            <a href="http://127.0.0.1/painting-app/public/login" class="btn  px-2"
                                style="color: white;">Login</a>
                        </div> -->
                    </div>
                </nav>
            </div>
        </div>
    </div>
    <!-- Navbar End -->
    <!-- Carousel Start -->
    <div class=" mb-1 ">
        <div class="row_container px-xl-12" style="background-color:#3D464D">
            <div class="col-lg-12">
                <div id="header-carousel" class="carousel slide carousel-fade mb-0 mb-lg-0" data-ride="carousel">
                    <ol class="carousel-indicators">
                        <li data-target="#header-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#header-carousel" data-slide-to="1"></li>
                        <li data-target="#header-carousel" data-slide-to="2"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item position-relative active" style="height: 430px;">
                            <img class="position-absolute w-100 h-100"
                                src="{{url('css/painting/uploads/banner/1690075543_carousel-3.jpg')}}"
                                style="object-fit: cover;">
                            <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">

                            </div>
                        </div>
                        <div class="carousel-item position-relative " style="height: 430px;">
                            <img class="position-absolute w-100 h-100"
                                src="{{url('css/painting/uploads/banner/1690075518_carousel-2.jpg')}}"
                                style="object-fit: cover;">
                            <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">

                            </div>
                        </div>
                        <div class="carousel-item position-relative " style="height: 430px;">
                            <img class="position-absolute w-100 h-100"
                                src="{{url('css/painting/uploads/banner/1690084592_carousel-1.jpg')}}"
                                style="object-fit: cover;">
                            <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <ul class="nav nav-tabs bg-dark" id="myTab" role="tablist">
                <li class="nav-item" onclick="selectCategory('1')">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                        aria-controls="category11" aria-selected="true">category1</a>
                </li>
                <li class="nav-item" onclick="selectCategory('2')">
                    <a class="nav-link " id="home-tab" data-toggle="tab" href="#home" role="tab"
                        aria-controls="category2" aria-selected="true">category2</a>
                </li>
                <li class="nav-item" onclick="selectCategory('3')">
                    <a class="nav-link " id="home-tab" data-toggle="tab" href="#home" role="tab"
                        aria-controls="category3" aria-selected="true">category3</a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="category_1" role="tabpanel" aria-labelledby="home-tab">
                    <!-- Start -->
                    <div class="container-fluid pt-5 pb-3">
                        <div class="row px-xl-5">
                            <input type="hidden" readonly id="categorys_1" value="6">
                            <div class="col-lg-2 col-md-4 col-sm-6 pb-1">
                                <div class="product-item bg-light mb-4" style="height:480px">
                                    <div class="product-img position-relative overflow-hidden" style="height:302px">
                                        <img class="img-fluid w-100"
                                            src="{{url('css/painting/uploads/img/game-6.jpg')}}" alt="">
                                    </div>
                                    <div class="text-center py-4">
                                        <a class="h6 text-decoration-none text-truncate"
                                            href="http://127.0.0.1/painting-app/public/shop-detail/7">Product 6</a>
                                        <div class="d-flex align-items-center justify-content-center mt-2">
                                            <h5><i class="fas fa-rupee-sign ml-1"></i>
                                                100000 </h5>
                                            <h6 class="text-muted ml-2"><i
                                                    class="fas fa-thin fa-gem ml-1 text-primary"></i>
                                                10 </h6>
                                        </div>
                                        <div class=" mb-4 pt-2">
                                            <button class="btn btn-primary px-3 disabled" data-toggle="modal"
                                                data-target="#myModal">Buy
                                                Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-6 pb-1">
                                <div class="product-item bg-light mb-4" style="height:480px">
                                    <div class="product-img position-relative overflow-hidden" style="height:302px">
                                        <img class="img-fluid w-100"
                                            src="{{url('css/painting/uploads/img/product-1.jpg')}}" alt="">
                                    </div>
                                    <div class="text-center py-4">
                                        <a class="h6 text-decoration-none text-truncate"
                                            href="http://127.0.0.1/painting-app/public/shop-detail/6">Product 5</a>
                                        <div class="d-flex align-items-center justify-content-center mt-2">
                                            <h5><i class="fas fa-rupee-sign ml-1"></i>
                                                100000 </h5>
                                            <h6 class="text-muted ml-2"><i
                                                    class="fas fa-thin fa-gem ml-1 text-primary"></i>
                                                30 </h6>
                                        </div>
                                        <div class=" mb-4 pt-2">
                                            <button class="btn btn-primary px-3 disabled" data-toggle="modal"
                                                data-target="#myModal">Buy
                                                Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-6 pb-1">
                                <div class="product-item bg-light mb-4" style="height:480px">
                                    <div class="product-img position-relative overflow-hidden" style="height:302px">
                                        <img class="img-fluid w-100"
                                            src="{{url('css/painting/uploads/img/game-5.jpg')}}" alt="">
                                    </div>
                                    <div class="text-center py-4">
                                        <a class="h6 text-decoration-none text-truncate"
                                            href="http://127.0.0.1/painting-app/public/shop-detail/5">Product 4</a>
                                        <div class="d-flex align-items-center justify-content-center mt-2">
                                            <h5><i class="fas fa-rupee-sign ml-1"></i>
                                                100000 </h5>
                                            <h6 class="text-muted ml-2"><i
                                                    class="fas fa-thin fa-gem ml-1 text-primary"></i>
                                                60 </h6>
                                        </div>
                                        <div class=" mb-4 pt-2">
                                            <button class="btn btn-primary px-3 disabled" data-toggle="modal"
                                                data-target="#myModal">Buy
                                                Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-6 pb-1">
                                <div class="product-item bg-light mb-4" style="height:480px">
                                    <div class="product-img position-relative overflow-hidden" style="height:302px">
                                        <img class="img-fluid w-100"
                                            src="{{url('css/painting/uploads/img/game-3.jpg')}}" alt="">
                                    </div>
                                    <div class="text-center py-4">
                                        <a class="h6 text-decoration-none text-truncate"
                                            href="http://127.0.0.1/painting-app/public/shop-detail/3">Product 3</a>
                                        <div class="d-flex align-items-center justify-content-center mt-2">
                                            <h5><i class="fas fa-rupee-sign ml-1"></i>
                                                10000 </h5>
                                            <h6 class="text-muted ml-2"><i
                                                    class="fas fa-thin fa-gem ml-1 text-primary"></i>
                                                40 </h6>
                                        </div>
                                        <div class=" mb-4 pt-2">
                                            <button class="btn btn-primary px-3 disabled" data-toggle="modal"
                                                data-target="#myModal">Buy
                                                Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-6 pb-1">
                                <div class="product-item bg-light mb-4" style="height:480px">
                                    <div class="product-img position-relative overflow-hidden" style="height:302px">
                                        <img class="img-fluid w-100"
                                            src="{{url('css/painting/uploads/img/game-2.jpg')}}" alt="">
                                    </div>
                                    <div class="text-center py-4">
                                        <a class="h6 text-decoration-none text-truncate"
                                            href="http://127.0.0.1/painting-app/public/shop-detail/2">Product 2</a>
                                        <div class="d-flex align-items-center justify-content-center mt-2">
                                            <h5><i class="fas fa-rupee-sign ml-1"></i>
                                                10000 </h5>
                                            <h6 class="text-muted ml-2"><i
                                                    class="fas fa-thin fa-gem ml-1 text-primary"></i>
                                                100 </h6>
                                        </div>
                                        <div class=" mb-4 pt-2">
                                            <button class="btn btn-primary px-3 disabled" data-toggle="modal"
                                                data-target="#myModal">Buy
                                                Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-6 pb-1">
                                <div class="product-item bg-light mb-4" style="height:480px">
                                    <div class="product-img position-relative overflow-hidden" style="height:302px">
                                        <img class="img-fluid w-100"
                                            src="{{url('css/painting/uploads/img/game-1.jpg')}}" alt="">
                                    </div>
                                    <div class="text-center py-4">
                                        <a class="h6 text-decoration-none text-truncate"
                                            href="http://127.0.0.1/painting-app/public/shop-detail/1">Product 1</a>
                                        <div class="d-flex align-items-center justify-content-center mt-2">
                                            <h5><i class="fas fa-rupee-sign ml-1"></i>
                                                500 </h5>
                                            <h6 class="text-muted ml-2"><i
                                                    class="fas fa-thin fa-gem ml-1 text-primary"></i>
                                                10 </h6>
                                        </div>
                                        <div class=" mb-4 pt-2">
                                            <button class="btn btn-primary px-3 disabled" data-toggle="modal"
                                                data-target="#myModal">Buy
                                                Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form action="http://127.0.0.1/painting-app/public/shop" id="form_category" method="get">
                        <input type="hidden" id="category_filter" readonly value="1" name="category_id[]">
                        <center><button type="submit" class="btn btn-primary"
                                style="margin-bottom: 15px;">More</button></a>
                        </center>
                    </form>
                </div>
                <!-- end -->
                <div class="tab-pane fade" id="category_2" role="tabpanel" aria-labelledby="profile-tab">
                    <!-- Start -->
                    <div class="container-fluid pt-5 pb-3">
                        <div class="row px-xl-5">
                            <input type="hidden" readonly id="categorys_2" value="2">
                            <div class="col-lg-2 col-md-4 col-sm-6 pb-1">
                                <div class="product-item bg-light mb-4" style="height:480px">
                                    <div class="product-img position-relative overflow-hidden" style="height:302px">
                                        <img class="img-fluid w-100"
                                            src="{{url('css/painting/uploads/img/game-5.jpg')}}" alt="">
                                    </div>
                                    <div class="text-center py-4">
                                        <a class="h6 text-decoration-none text-truncate"
                                            href="http://127.0.0.1/painting-app/public/shop-detail/9">Product 8</a>
                                        <div class="d-flex align-items-center justify-content-center mt-2">
                                            <h5>
                                                100000<i class="fas fa-rupee-sign ml-1"></i>
                                            </h5>
                                            <h6 class="text-muted ml-2">
                                                2<i class="fas fa-thin fa-gem ml-1 text-primary"></i>
                                            </h6>
                                        </div>
                                        <div class=" mb-4 pt-2">
                                            <button class="btn btn-primary px-3 disabled" data-toggle="modal"
                                                data-target="#myModal">Buy
                                                Now</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-4 col-sm-6 pb-1">
                                <div class="product-item bg-light mb-4" style="height:480px">
                                    <div class="product-img position-relative overflow-hidden" style="height:302px">
                                        <img class="img-fluid w-100"
                                            src="{{url('css/painting/uploads/img/game-6.jpg')}}" alt="">
                                    </div>
                                    <div class="text-center py-4">
                                        <a class="h6 text-decoration-none text-truncate"
                                            href="http://127.0.0.1/painting-app/public/shop-detail/8">Product 7</a>
                                        <div class="d-flex align-items-center justify-content-center mt-2">