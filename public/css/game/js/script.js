// {/* <script> */}
let selectedValues = [];

function openModal(number) {
    var modal = document.getElementById("myModal");
    var modalContent = document.getElementById("modalContent");
    modal.style.display = "flex";
    // Clear the selected values array
    selectedValues = [];
    updateButtonStyles();
}

function closeModal() {
    var modal = document.getElementById("myModal");
    modal.style.display = "none";
}

function submitValues() {
    if (selectedValues.length > 0) {
        // Store the selected values before closing the modal
        const selectedValuesCopy = [...selectedValues];
        closeModal();

        // Highlight the selected buttons after closing the modal
        selectedValuesCopy.forEach(value => {
            const betButton = document.querySelector('.bet-button[data-value="' + value + '"]');
            if (betButton) {
                betButton.style.border = "2px solid red"; // You can customize the border style
            }
        });

        const chatMessagesContainer = document.getElementById('ChatBox');
        const message = "₹" + selectedValuesCopy.map(value => value.replace('button', '')).join(', ') +
            " - guest292585";
        const pElement = document.createElement('p');
        pElement.textContent = message;
        chatMessagesContainer.appendChild(pElement);
    } else {
        alert("Please select values first");
    }
}


function updateButtonStyles() {
    // Remove the 'selected' class from all buttons
    document.querySelectorAll('.modal-button').forEach(button => button.classList.remove('selected'));
}

// </script>



// <script>
// Function to toggle the chat input field
function toggleChatInput() {
    const chatInputContainer = document.getElementById('chatInputContainer');
    chatInputContainer.style.display = chatInputContainer.style.display === 'none' ? 'flex' : 'none';
}

// Function to send a message
function sendMessage() {
    const chatInput = document.getElementById('chatInput');
    const message = chatInput.value.trim();

    if (message !== '') {
        const chatMessagesContainer = document.getElementById('ChatBox');
        const pElement = document.createElement('p');
        pElement.textContent = message;
        chatMessagesContainer.appendChild(pElement);

        // Clear the input field
        chatInput.value = '';
    }

    // Hide the chat input field
    toggleChatInput();
}


{/* <script> */ }
function updateRangeValue() {
    var rangeInput = document.getElementById("betRange");
    var rangeValue = document.getElementById("rangeValue");

    // Update the displayed range value with the currency symbol
    rangeValue.textContent = "₹ " + rangeInput.value;
}

function selectValue(buttonId) {
    // Toggle the selection of the clicked button
    const index = selectedValues.indexOf(buttonId);

    // Clear the selectedValues array
    selectedValues = [];

    // Add the buttonId to the selectedValues array
    selectedValues.push(buttonId);

    // Remove the 'selected' class from all buttons
    document.querySelectorAll('.modal-button').forEach(button => button.classList.remove('selected'));

    // Toggle the 'selected' class for the clicked button
    const button = document.getElementById(buttonId);
    if (button) {
        button.classList.add('selected');
        // Update the range value based on the clicked button
        var rangeInput = document.getElementById("betRange");
        rangeInput.value = parseInt(button.textContent);
        updateRangeValue();
    }
}

//This is  a function ,when i click on placebet button then chatbox will open automatically.
// JavaScript code
document.addEventListener('DOMContentLoaded', function () {
    document.querySelector('.placebet-button').addEventListener('click', function () {
        document.getElementById('ChatBox').style.display = 'block';
    });

    document.getElementById('closeChatBtn').addEventListener('click', function () {
        document.getElementById('ChatBox').style.display = 'none';
    });
});



