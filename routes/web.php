<?php

use App\Http\Controllers\AdminLoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/', function () {
    return view('index');
});

Route::get('/game', function () {
    return view('game/rollingdice');
});
Route::get('/index', function () {
    return view('game/index');

});
Route::get('shop', function () {
    return view('shop');
});
Route::get('games', function () {
    return view('games');
});
Route::get('contact-us', function () {
    return view('contact-us');
});
Route::get('register', function () {
    return view('register');
});

Route::get('shop_detail', function () {
    return view('shop_detail');
});
Route::get('my_shopping', function () {
    return view('my_shopping');
});
Route::get('favorite', function () {
    return view('favorite');
});
Route::get('game_detail', function () {
    return view('game_detail');
});

Route::get('painting/home', function () {
    return view('painting/home');
});

Route::group(['prefix'=> 'admin'], function () {
    Route::group(['middleware'=> 'admin.guest'], function () {
        Route::get('/login',[AdminLoginController::class,'index'])->name('admin.login');
    });

    Route::group(['middleware'=> 'admin.auth'], function () {

    });
});
